// Flutter imports:
import 'package:flutter/material.dart';

class TextFieldController {
  static TextEditingController urlTextFieldController = TextEditingController();

  static TextEditingController userNameFieldController =
      TextEditingController();

  static TextEditingController notesFieldController = TextEditingController();

  static TextEditingController passwordNameFieldController =
      TextEditingController();

  static TextEditingController passwordFieldController =
      TextEditingController();

  static TextEditingController masterPasswordFieldController =
      TextEditingController();

  static TextEditingController clientPasswordFieldController =
      TextEditingController();

  static TextEditingController passwordController = TextEditingController();

  static TextEditingController refreshRateController = TextEditingController();

  static TextEditingController searchUserTextController =
      TextEditingController();

  static TextEditingController tagTextController = TextEditingController();

  static TextEditingController folderNameController = TextEditingController();

  static TextEditingController searchController = TextEditingController();

  static TextEditingController urlEditingTextFieldController =
      TextEditingController();

  static TextEditingController userNameEditingFieldController =
      TextEditingController();

  static TextEditingController notesEditingFieldController =
      TextEditingController();

  static TextEditingController passwordNameEditingFieldController =
      TextEditingController();

  static TextEditingController passwordEditingFieldController =
      TextEditingController();
}
