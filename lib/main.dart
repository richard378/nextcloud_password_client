// Dart imports:
import 'dart:async';

// Flutter imports:
import 'package:bitsdojo_window_v3/bitsdojo_window_v3.dart';
import 'package:flutter/material.dart';

// Package imports:
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_sodium/flutter_sodium.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:locale_switcher/locale_switcher.dart';
import 'package:nextcloud_password_client/models/user_model.dart';
import 'package:nextcloud_password_client/utils/logger.dart';
import 'package:nextcloud_password_client/view_models/user_list_view_model.dart';
import 'package:provider/provider.dart';

// Project imports:
import 'package:nextcloud_password_client/constants/page_config_constants.dart';
import 'package:nextcloud_password_client/constants/persistence_constants.dart';
import 'package:nextcloud_password_client/constants/widget_constants.dart';
import 'package:nextcloud_password_client/enums/nodes.dart';
import 'package:nextcloud_password_client/enums/security.dart';
import 'package:nextcloud_password_client/enums/themes.dart';
import 'package:nextcloud_password_client/models/config_model.dart';
import 'package:nextcloud_password_client/models/credentials_model.dart';
import 'package:nextcloud_password_client/models/folder_model.dart';
import 'package:nextcloud_password_client/models/password_model.dart';
import 'package:nextcloud_password_client/models/password_share_model.dart';
import 'package:nextcloud_password_client/models/server_config_model.dart';
import 'package:nextcloud_password_client/models/tag_model.dart';
import 'package:nextcloud_password_client/router/password_router_parser.dart';
import 'package:nextcloud_password_client/router/router_delegate.dart';
import 'package:nextcloud_password_client/utils/data_access_layer.dart';
import 'package:nextcloud_password_client/utils/http_utils.dart';
import 'package:nextcloud_password_client/utils/key_chain.dart';
import 'package:nextcloud_password_client/utils/theme_utils.dart';
import 'package:nextcloud_password_client/utils/view_state.dart';
import 'package:nextcloud_password_client/view_models/config_view_model.dart';
import 'package:nextcloud_password_client/view_models/credentials_view_model.dart';
import 'package:nextcloud_password_client/view_models/folder_list_view_model.dart';
import 'package:nextcloud_password_client/view_models/password_list_view_model.dart';
import 'package:nextcloud_password_client/view_models/server_config_view_model.dart';
import 'package:nextcloud_password_client/view_models/tag_list_view_model.dart';

//import 'package:flutter_sodium/flutter_sodium.dart';

void main(List<String> args) async {
  runZonedGuarded(() async {
    bool initError = await initializeApp();

    runApp(await myApp(initError)); // starting point of app

    doWhenWindowReady(() {
      appWindow.minSize = const Size(500, 500);
      appWindow.size = const Size(1500, 1000);
      appWindow.alignment = Alignment.center;
      appWindow.show();
    });
  }, (error, stackTrace) async {
    Logger.logError(error, stackTrace);
  });
}

Future<bool> initializeApp() async {
  try {
    Sodium.init();
    await Hive.initFlutter(npcFolder);
  } catch (e) {
    return true;
  }

  Hive.registerAdapter(ConfigModelAdapter());
  Hive.registerAdapter(FolderModelAdapter());
  Hive.registerAdapter(PasswordModelAdapter());
  Hive.registerAdapter(CredentialsModelAdapter());
  Hive.registerAdapter(KeyChainAdapter());
  Hive.registerAdapter(ThemesAdapter());
  Hive.registerAdapter(ViewStateAdapter());
  Hive.registerAdapter(ServerConfigModelAdapter());
  Hive.registerAdapter(PasswordShareModelAdapter());
  Hive.registerAdapter(TagModelAdapter());
  Hive.registerAdapter(SecurityAdapter());
  Hive.registerAdapter(UserModelAdapter());
  Hive.registerAdapter(NodesAdapter());

  await DataAccessLayer().openBoxes();

  return false;
}

Future<StatelessWidget> myApp(bool initError) async {
  ViewState viewState = ViewState();

  final parser = PasswordRouteParser();

  if (!initError) {
    ConfigViewModel configViewModel = ConfigViewModel();
    UserListViewModel userListViewModel = UserListViewModel();
    ServerConfigViewModel serverConfigViewModel = ServerConfigViewModel();
    FolderListViewModel folderListViewModel = FolderListViewModel();
    TagListViewModel tagListViewModel = TagListViewModel();
    PasswordListViewModel passwordListViewModel = PasswordListViewModel();
    CredentialsViewModel credentialsViewModel = CredentialsViewModel();

    await Future.wait([
      configViewModel.initialize(),
      serverConfigViewModel.initialize(),
      viewState.initialize(),
      folderListViewModel.initialize(),
      passwordListViewModel.initialize(),
      credentialsViewModel.initialize(),
      tagListViewModel.initialize(),
      userListViewModel.initialize()
    ]);

    viewState.isLoggedIn =
        viewState.isURLReachable = configViewModel.loginSucceeded;

    final delegate = PasswordRouterDelegate(viewState);

    HttpUtils().setLoginData(
        configViewModel.serverURL,
        credentialsViewModel.userName,
        credentialsViewModel.password,
        configViewModel.session);

    configViewModel.loginSucceeded
        ? delegate.setInitialRoutePath(passwordScreenPageConfig)
        : delegate.setInitialRoutePath(loginPageConfig);

    return MultiProvider(
        providers: [
          ChangeNotifierProvider.value(
            value: configViewModel,
          ),
          ChangeNotifierProvider.value(
            value: serverConfigViewModel,
          ),
          ChangeNotifierProvider.value(
            value: viewState,
          ),
          ChangeNotifierProvider.value(
            value: folderListViewModel,
          ),
          ChangeNotifierProvider.value(
            value: passwordListViewModel,
          ),
          ChangeNotifierProvider.value(
            value: credentialsViewModel,
          ),
          ChangeNotifierProvider.value(
            value: tagListViewModel,
          ),
          ChangeNotifierProvider.value(
            value: userListViewModel,
          ),
        ],
        builder: (context, _) => LocaleManager(
              child: MaterialApp.router(
                theme: ThemeUtils.getTheme(
                    context.watch<ConfigViewModel>().selectedTheme,
                    serverConfigModel: context.watch<ServerConfigViewModel>()),
                title: appTitle,
                debugShowCheckedModeBanner: false,
                locale: LocaleSwitcher.localeBestMatch,
                localizationsDelegates: AppLocalizations.localizationsDelegates,
                supportedLocales: AppLocalizations.supportedLocales,
                routerDelegate: delegate,
                routeInformationParser: parser,
              ),
            ));
  } else {
    viewState.initError = initError;

    final delegate = PasswordRouterDelegate(viewState);

    delegate.setInitialRoutePath(missingDependenciesPageConfig);

    return MultiProvider(
        providers: [
          ChangeNotifierProvider.value(
            value: viewState,
          ),
        ],
        builder: (context, _) => LocaleManager(
              child: MaterialApp.router(
                title: appTitle,
                debugShowCheckedModeBanner: false,
                locale: LocaleSwitcher.localeBestMatch,
                localizationsDelegates: AppLocalizations.localizationsDelegates,
                supportedLocales: AppLocalizations.supportedLocales,
                routerDelegate: delegate,
                routeInformationParser: parser,
              ),
            ));
  }
}
