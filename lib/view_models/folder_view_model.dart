// Project imports:
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:nextcloud_password_client/models/folder_model.dart';
import 'package:nextcloud_password_client/provider/nextcloud_folder_provider.dart';
import 'package:nextcloud_password_client/utils/data_access_layer.dart';

class FolderViewModel extends FolderModel {

  DataAccessLayer dataAccessLayer = DataAccessLayer();
  FolderModel _folderModel = FolderModel();
  List<FolderViewModel> revisionViewModels = [];
  FolderModel get folderModel => _folderModel;

  @override
  List<dynamic> get props => super.props;
  
  FolderViewModel({DataAccessLayer? dataAccessLayer})
      : dataAccessLayer = dataAccessLayer ?? DataAccessLayer();
  
  FolderViewModel.fromModel(FolderModel model) {
  
    _folderModel = FolderModel();
    refreshModel(fromModel: model, toModel: this);
    refreshModel(fromModel: this, toModel: _folderModel);
  
  }

  FolderViewModel.fromMap(Map<String, dynamic> folder) {
  
    _folderModel = FolderModel();
    _folderModel.id = id = folder['id'] ?? id;
    _folderModel.label = label = folder['label'] ?? label;
    _folderModel.parent = parent = folder['parent'] ?? parent;
  
    if (folder['revisions'] != null) {
  
      revisionViewModels =
          getRevisionsFromAPI(List.castFrom(folder['revisions']));
      _folderModel.revisions = getRevisionModelFromViewModels();
  
    }
  
    _folderModel.cseType = cseType = folder['cseType'] ?? cseType;
    _folderModel.cseKey = cseKey = folder['cseKey'] ?? cseKey;
    _folderModel.sseType = sseType = folder['sseType'] ?? sseType;
    _folderModel.client = client = folder['client'] ?? client;
    _folderModel.hidden = hidden = folder['hidden'] ?? hidden;
    _folderModel.trashed = trashed = folder['trashed'] ?? trashed;
    _folderModel.favorite = favorite = folder['favorite'] ?? favorite;
    _folderModel.created = created = folder['created'] ?? created;
    _folderModel.updated = updated = folder['updated'] ?? updated;
    _folderModel.edited = edited = folder['edited'] ?? edited;
  
  }

  void refreshModel(
      {required FolderModel fromModel, 
      required FolderModel toModel}) {
    
    toModel.id = fromModel.id;
    toModel.label = fromModel.label;
    toModel.parent = fromModel.parent;
    toModel.revision = fromModel.revision;
    toModel.cseType = fromModel.cseType;
    toModel.cseKey = fromModel.cseKey;
    toModel.sseType = fromModel.sseType;
    toModel.client = fromModel.client;
    toModel.hidden = fromModel.hidden;
    toModel.trashed = fromModel.trashed;
    toModel.favorite = fromModel.favorite;
    toModel.created = fromModel.created;
    toModel.updated = fromModel.updated;
    toModel.edited = fromModel.edited;
    
    notifyListeners();
  
  }

  void refreshViewModel(FolderModel folderModel) {
  
    refreshModel(
      fromModel: folderModel, 
      toModel: this
    );
    
    revisionViewModels = getRevisionViewModelsFromModels(_folderModel);
  
  }

  void refreshDataModel(FolderViewModel folderViewModel) {
    
    refreshModel(
      fromModel: folderViewModel, 
      toModel: _folderModel
    );
    
    _folderModel.revisions = getRevisionModelFromViewModels();
  
  }

  @override
  Future<String> setFavorite(
    BuildContext context, 
    bool newFavorite) async {
    
    favorite = newFavorite;
    _folderModel.favorite = favorite;
    dataAccessLayer.saveFolder(_folderModel);
    
    notifyListeners();
    
    FolderViewModel remoteModel =  await NextcloudFolderProvider.setFolderToFavorite(getBodyForFavoriteUpdate());
    
    if (remoteModel.id.isNotEmpty) {
    
      FolderViewModel revision = FolderViewModel.fromModel(this);
      revision.id = remoteModel.id;
      revision.favorite = !revision.favorite;
      revisionViewModels.add(revision);
      revisions.add(revision._folderModel);
      notifyListeners();
    
      return "";
    
    } else {
    
      return AppLocalizations.of(context)!.folderRemoteUpdateError + AppLocalizations.of(context)!.localChangesOverwriteHint;
    
    }
  
  }

  getBodyForFavoriteUpdate() {
    
    return jsonEncode({
      'id': id,
      'label': label,
      'cseType': cseType,
      'cseKey': cseKey,
      'favorite': favorite
    });
  
  }

  List<FolderModel> getRevisionModelFromViewModels() {
    
    List<FolderModel> models = [];
    
    for (FolderViewModel revision in revisionViewModels) {
    
      models.add(revision._folderModel);
    
    }
    
    return models;
  
  }

  List<FolderViewModel> getRevisionViewModelsFromModels(
      FolderModel folderModel) {
  
    List<FolderViewModel> models = [];
  
    for (FolderModel revision in folderModel.revisions) {
  
      models.add(FolderViewModel.fromModel(revision));
  
    }
  
    return models;
  
  }

  List<FolderViewModel> getRevisionsFromAPI(
      List<Map<String, dynamic>> revisions) {
  
    List<FolderViewModel> models = [];
  
    for (Map<String, dynamic> revision in revisions) {
  
      models.add(FolderViewModel.fromMap(revision));
  
    }
  
    return models;
  
  }

  Future<bool> updateFolder() async{
  
    dataAccessLayer.saveFolder(this);
  
    return await NextcloudFolderProvider.updateFolder(getBodyForUpdate());
  
  }

  String getBodyForUpdate() {
  
    return jsonEncode({
        'id': id,
        'label': label,
        'parent': parent,
        'cseType': cseType,
        'cseKey': cseKey,
        'favorite': favorite,
        'hidden': hidden
      });
  
  }

  Future<bool> createFolder() async{
    
    FolderViewModel remoteFolder = await NextcloudFolderProvider.createFolder(getBodyForCreation());
  
    if(remoteFolder.id.isNotEmpty){
  
        id = remoteFolder.id;
  
    }
  
    return remoteFolder.id.isNotEmpty;
  
  }

  String getBodyForCreation() {
  
    return jsonEncode({
        'label': label,
        'parent': parent,
        'cseType': cseType,
        'cseKey': cseKey
      });
  
  }

}
