// Project imports:
import 'package:nextcloud_password_client/interfaces/view_model_interface.dart';
import 'package:nextcloud_password_client/models/credentials_model.dart';
import 'package:nextcloud_password_client/utils/data_access_layer.dart';

class CredentialsViewModel extends CredentialsModel
    implements ViewModelInterface {

  CredentialsModel _credentialsModel = CredentialsModel();
  DataAccessLayer dataAccessLayer;
  CredentialsViewModel({DataAccessLayer? dataAccessLayer})
      : dataAccessLayer = dataAccessLayer ?? DataAccessLayer();

  @override
  Future<void> initialize() async {

    loadModel();
    refreshViewModels();
  
  }

  CredentialsModel get credentialsModel => _credentialsModel;
  
  @override
  void refreshDataModels() {
  
    refreshModel(
      fromModel: this, 
      toModel: _credentialsModel
    );
  
  }

  @override
  void refreshViewModels() {
    
    refreshModel(
      fromModel: _credentialsModel, 
      toModel: this
    );
  
  }

  refreshModel(
    {required CredentialsModel fromModel,
    required CredentialsModel toModel}) {
    
    toModel.userName = fromModel.userName;
    toModel.password = fromModel.password;
    toModel.masterPassword = fromModel.masterPassword;
    toModel.clientPassword = fromModel.clientPassword;
  
    notifyListeners();

  }

  @override
  void persistModel([
    bool firstBool = false, 
    bool secondBool = false]) {

    refreshDataModels();
    
    if (!firstBool) {
    
      _credentialsModel.userName = '';
      _credentialsModel.password = '';
    
    }
    
    if (!secondBool) {
    
      _credentialsModel.masterPassword = '';
    
    }
    
    dataAccessLayer.persistCredentials(_credentialsModel);
  
  }

  @override
  void loadModel() {
  
    _credentialsModel = dataAccessLayer.loadCredentials()!;
  
  }

  @override
  void handleLogout() {
  
    _credentialsModel = CredentialsModel();
    refreshViewModels();
  
  }

}
