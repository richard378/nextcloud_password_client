// Project imports:
import 'package:nextcloud_password_client/models/server_config_model.dart';
import 'package:nextcloud_password_client/utils/data_access_layer.dart';

class ServerConfigViewModel extends ServerConfigModel {

  ServerConfigModel _serverConfigModel = ServerConfigModel();
  DataAccessLayer dataAccessLayer;
  ServerConfigModel get serverConfigModel => _serverConfigModel;
  
  set serverConfigModel(ServerConfigModel serverConfigModel) {
  
    _serverConfigModel = serverConfigModel;
    
    refreshModel(
      fromModel: _serverConfigModel, 
      toModel: this
    );
    
    notifyListeners();
  
  }

  Future<void> initialize() async {
    
    loadServerConfig();
  
    refreshModel(
      fromModel: _serverConfigModel, 
      toModel: this
    );
  
  }

  ServerConfigViewModel({DataAccessLayer? dataAccessLayer})
      : dataAccessLayer = dataAccessLayer ?? DataAccessLayer();

  void refreshModel(
    {required ServerConfigModel fromModel,
    required ServerConfigModel toModel}) {
  
    toModel.userPasswordGeneratorStrength =
        fromModel.userPasswordGeneratorStrength;
    toModel.userPasswordGeneratorNumbers =
        fromModel.userPasswordGeneratorNumbers;
    toModel.userPasswordGeneratorSpecial =
        fromModel.userPasswordGeneratorSpecial;
    toModel.usePasswordSecurityDuplicates =
        fromModel.usePasswordSecurityDuplicates;
    toModel.usePasswordSecurityAge = fromModel.usePasswordSecurityAge;
    toModel.userPasswordSecurityHash = fromModel.userPasswordSecurityHash;
    toModel.useMailSecurity = fromModel.useMailSecurity;
    toModel.userMailShares = fromModel.userMailShares;
    toModel.userNotificationSecurity = fromModel.userNotificationSecurity;
    toModel.userNotificationShares = fromModel.userNotificationShares;
    toModel.userNotificationErrors = fromModel.userNotificationErrors;
    toModel.userNotificationAdmin = fromModel.userNotificationAdmin;
    _serverConfigModel.userEncryptionSse = fromModel.userEncryptionSse;
    toModel.userEncryptionCse = fromModel.userEncryptionCse;
    toModel.userSharingEditable = fromModel.userSharingEditable;
    toModel.userSharingResharing = fromModel.userSharingResharing;
    toModel.userSessionLifetime = fromModel.userSessionLifetime;
    toModel.serverVersion = fromModel.serverVersion;
    toModel.serverAppVersion = fromModel.serverAppVersion;
    toModel.serverBaseUrl = fromModel.serverBaseUrl;
    toModel.serverBaseUrlWebdav = fromModel.serverBaseUrlWebdav;
    toModel.serverSharingEnabled = fromModel.serverSharingEnabled;
    toModel.serverSharingResharing = fromModel.serverSharingResharing;
    toModel.serverSharingAutocomplete = fromModel.serverSharingAutocomplete;
    toModel.serverSharingTypes = fromModel.serverSharingTypes;
    toModel.serverThemeColorPrimary = fromModel.serverThemeColorPrimary;
    toModel.serverThemeColorText = fromModel.serverThemeColorText;
    toModel.serverThemeColorBackground = fromModel.serverThemeColorBackground;
    toModel.serverThemeBackground = fromModel.serverThemeBackground;
    toModel.serverThemeLogo = fromModel.serverThemeLogo;
    toModel.serverThemeLabel = fromModel.serverThemeLabel;
    toModel.serverThemeAppIcon = fromModel.serverThemeAppIcon;
    toModel.serverThemeFolderIcon = fromModel.serverThemeFolderIcon;
    toModel.serverHandbookUrl = fromModel.serverHandbookUrl;
    toModel.serverPerformance = fromModel.serverPerformance;
   
    notifyListeners();
  
  }

  void persistConfig() async {
  
    refreshModel(
      fromModel: this, 
      toModel: _serverConfigModel
    );
  
    dataAccessLayer.persistServerConfig(_serverConfigModel);
  
  }

  void loadServerConfig() {

    _serverConfigModel = dataAccessLayer.loadServerConfig()!;
  
  }

  void handleLogout() {
  
    _serverConfigModel = ServerConfigModel();
  
    refreshModel(
      fromModel: _serverConfigModel, 
      toModel: this
    );
  
  }

  ServerConfigViewModel.fromMap(
    Map<String, dynamic> serverConfig,
    {DataAccessLayer? dataAccessLayer})
    : dataAccessLayer = dataAccessLayer ?? DataAccessLayer() {
    
    _serverConfigModel = ServerConfigModel();

    _serverConfigModel.userPasswordGeneratorStrength =
        userPasswordGeneratorStrength =
            serverConfig['user.password.generator.strength'] ??
                userPasswordGeneratorStrength;
    _serverConfigModel.userPasswordGeneratorNumbers =
        userPasswordGeneratorNumbers =
            serverConfig['user.password.generator.numbers'] ??
                userPasswordGeneratorNumbers;
    _serverConfigModel.userPasswordGeneratorSpecial =
        userPasswordGeneratorSpecial =
            serverConfig['user.password.generator.special'] ??
                userPasswordGeneratorSpecial;
    _serverConfigModel.usePasswordSecurityDuplicates =
        usePasswordSecurityDuplicates =
            serverConfig['user.password.security.duplicates'] ??
                usePasswordSecurityDuplicates;
    _serverConfigModel.usePasswordSecurityAge = usePasswordSecurityAge =
        serverConfig['user.password.security.age'] ?? usePasswordSecurityAge;
    _serverConfigModel.usePasswordSecurityAge = usePasswordSecurityAge =
        serverConfig['user.password.security.age'] ?? usePasswordSecurityAge;
    _serverConfigModel.userPasswordSecurityHash = userPasswordSecurityHash =
        serverConfig['user.password.security.hash'] ?? userPasswordSecurityHash;
    _serverConfigModel.useMailSecurity =
        useMailSecurity = serverConfig['user.mail.security'] ?? useMailSecurity;
    _serverConfigModel.userMailShares =
        userMailShares = serverConfig['user.mail.shares'] ?? userMailShares;
    _serverConfigModel.userNotificationSecurity = userNotificationSecurity =
        serverConfig['user.notification.security'] ?? userNotificationSecurity;
    _serverConfigModel.userNotificationShares = userNotificationShares =
        serverConfig['user.notification.shares'] ?? userNotificationShares;
    _serverConfigModel.userNotificationErrors = userNotificationErrors =
        serverConfig['user.notification.errors'] ?? userNotificationErrors;
    _serverConfigModel.userNotificationAdmin = userNotificationAdmin =
        serverConfig['user.notification.admin'] ?? userNotificationAdmin;
    _serverConfigModel.userEncryptionSse = userEncryptionSse =
        serverConfig['user.encryption.sse'] ?? userEncryptionSse;
    _serverConfigModel.userEncryptionCse = userEncryptionCse =
        serverConfig['user.encryption.cse'] ?? userEncryptionCse;
    _serverConfigModel.userSharingEditable = userSharingEditable =
        serverConfig['user.sharing.editable'] ?? userSharingEditable;
    _serverConfigModel.userSharingResharing = userSharingResharing =
        serverConfig['user.sharing.resharing'] ?? userSharingResharing;
    _serverConfigModel.userSessionLifetime = userSessionLifetime =
        serverConfig['user.session.lifetime'] ?? userSessionLifetime;
    _serverConfigModel.serverVersion =
        serverVersion = serverConfig['server.version'] ?? serverVersion;
    _serverConfigModel.serverAppVersion = serverAppVersion =
        serverConfig['server.app.version'] ?? serverAppVersion;
    _serverConfigModel.serverBaseUrl =
        serverBaseUrl = serverConfig['server.baseUrl'] ?? serverBaseUrl;
    _serverConfigModel.serverBaseUrlWebdav = serverBaseUrlWebdav =
        serverConfig['server.baseUrl.webdav'] ?? serverBaseUrlWebdav;
    _serverConfigModel.serverSharingEnabled = serverSharingEnabled =
        serverConfig['server.sharing.enabled'] ?? serverSharingEnabled;
    _serverConfigModel.serverSharingResharing = serverSharingResharing =
        serverConfig['server.sharing.resharing'] ?? serverSharingResharing;
    _serverConfigModel.serverSharingAutocomplete = serverSharingAutocomplete =
        serverConfig['server.sharing.autocomplete'] ??
            serverSharingAutocomplete;

    for (dynamic item in serverConfig['server.sharing.types']) {
      
      _serverConfigModel.serverSharingTypes.add(item);
   
    }

    _serverConfigModel.serverThemeColorPrimary = serverThemeColorPrimary =
        serverConfig['server.theme.color.primary'] ?? serverThemeColorPrimary;
    _serverConfigModel.serverThemeColorText = serverThemeColorText =
        serverConfig['server.theme.color.text'] ?? serverThemeColorText;
    _serverConfigModel.serverThemeColorBackground = serverThemeColorBackground =
        serverConfig['server.theme.color.background'] ??
            serverThemeColorBackground;
    _serverConfigModel.serverThemeBackground = serverThemeBackground =
        serverConfig['server.theme.background'] ?? serverThemeBackground;
    _serverConfigModel.serverThemeLogo =
        serverThemeLogo = serverConfig['server.theme.logo'] ?? serverThemeLogo;
    _serverConfigModel.serverThemeLabel = serverThemeLabel =
        serverConfig['server.theme.label'] ?? serverThemeLabel;
    _serverConfigModel.serverThemeAppIcon = serverThemeAppIcon =
        serverConfig['server.theme.app.icon'] ?? serverThemeAppIcon;
    _serverConfigModel.serverThemeFolderIcon = serverThemeFolderIcon =
        serverConfig['server.theme.folder.icon'] ?? serverThemeFolderIcon;
    _serverConfigModel.serverHandbookUrl = serverHandbookUrl =
        serverConfig['server.handbook.url'] ?? serverHandbookUrl;
    _serverConfigModel.serverPerformance = serverPerformance =
        serverConfig['server.performance'] ?? serverPerformance;
 
  }

}
