// Project imports:
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:nextcloud_password_client/models/tag_model.dart';
import 'package:nextcloud_password_client/provider/nextcloud_tag_provider.dart';

// Package imports:
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_styled_toast/flutter_styled_toast.dart';
import 'package:nextcloud_password_client/utils/data_access_layer.dart';
import 'package:nextcloud_password_client/utils/view_state.dart';
import 'package:nextcloud_password_client/view_models/tag_list_view_model.dart';
import 'package:provider/provider.dart';

class TagViewModel extends TagModel {
  TagModel _tagModel = TagModel();
  DataAccessLayer dataAccessLayer;
  List<TagViewModel> revisionViewModels = [];
  TagModel get tagModel => _tagModel;

  TagViewModel({DataAccessLayer? dataAccessLayer})
      : dataAccessLayer = dataAccessLayer ?? DataAccessLayer();

  @override
  List<dynamic> get props => super.props;

  TagViewModel.fromModel(TagModel model, {DataAccessLayer? dataAccessLayer})
      : dataAccessLayer = dataAccessLayer ?? DataAccessLayer() {
    _tagModel = TagModel();
    refreshModel(fromModel: model, toModel: this);
    refreshModel(fromModel: model, toModel: _tagModel);
  }

  TagViewModel.fromMap(Map<String, dynamic> tag,
      {DataAccessLayer? dataAccessLayer})
      : dataAccessLayer = dataAccessLayer ?? DataAccessLayer() {
    _tagModel = TagModel();
    _tagModel.id = id = tag['id'] ?? id;
    _tagModel.label = label = tag['label'] ?? label;
    _tagModel.revision = revision = tag['revision'] ?? revision;
    _tagModel.cseType = cseType = tag['cseType'] ?? cseType;
    _tagModel.cseKey = cseKey = tag['cseKey'] ?? cseKey;
    _tagModel.sseType = sseType = tag['sseType'] ?? sseType;
    _tagModel.client = client = tag['client'] ?? client;
    _tagModel.color = color = tag['color'] ?? color;
    _tagModel.hidden = hidden = tag['hidden'] ?? hidden;
    _tagModel.trashed = trashed = tag['trashed'] ?? trashed;
    _tagModel.favorite = favorite = tag['favorite'] ?? favorite;
    _tagModel.created = created = tag['created'] ?? created;
    _tagModel.updated = updated = tag['updated'] ?? updated;
    _tagModel.edited = edited = tag['edited'] ?? edited;

    if (tag['revisions'] != null) {
      revisionViewModels = getRevisionsFromAPI(List.castFrom(tag['revisions']));
      _tagModel.revisions = getRevisionModelFromViewModels();
    }
  }

  void refreshModel({required TagModel fromModel, required TagModel toModel}) {
    toModel.id = fromModel.id;
    toModel.label = fromModel.label;
    toModel.revision = fromModel.revision;
    toModel.cseType = fromModel.cseType;
    toModel.cseKey = fromModel.cseKey;
    toModel.sseType = fromModel.sseType;
    toModel.client = fromModel.client;
    toModel.color = fromModel.color;
    toModel.hidden = fromModel.hidden;
    toModel.trashed = fromModel.trashed;
    toModel.favorite = fromModel.favorite;
    toModel.created = fromModel.created;
    toModel.updated = fromModel.updated;
    toModel.edited = fromModel.edited;

    notifyListeners();
  }

  void refreshViewModel(TagModel tagModel) {
    refreshModel(fromModel: tagModel, toModel: this);

    revisionViewModels = getRevisionViewModelsFromModels(_tagModel);
    notifyListeners();
  }

  void refreshDataModel(TagViewModel tagViewModel) {
    refreshModel(fromModel: tagViewModel, toModel: _tagModel);

    _tagModel.revisions = getRevisionModelFromViewModels();
    notifyListeners();
  }

  List<TagViewModel> getRevisionsFromAPI(List<Map<String, dynamic>> revisions) {
    List<TagViewModel> models = [];

    for (Map<String, dynamic> revision in revisions) {
      models.add(TagViewModel.fromMap(revision));
    }

    return models;
  }

  List<TagModel> getRevisionModelFromViewModels() {
    List<TagModel> models = [];

    for (TagViewModel revision in revisionViewModels) {
      models.add(revision._tagModel);
    }

    return models;
  }

  List<TagViewModel> getRevisionViewModelsFromModels(TagModel tagModel) {
    List<TagViewModel> models = [];

    for (TagModel revision in tagModel.revisions) {
      models.add(TagViewModel.fromModel(revision));
    }

    return models;
  }

  Future<TagViewModel> createTag(BuildContext context) async {
    TagViewModel newTag =
        await NextcloudTagProvider.createTag(getBodyForTagCreation());

    if (newTag.id.isNotEmpty) {
      id = newTag.id;
      revision = newTag.revision;
      refreshDataModel(this);

      notifyListeners();
    } else {
      showToast(
          AppLocalizations.of(context)!.passwordRemoteUpdateError +
              AppLocalizations.of(context)!.localChangesOverwriteHint,
          duration: const Duration(seconds: 5),
          context: context);
    }

    context.read<ViewState>().inEditMode = false;
    dataAccessLayer.saveTag(_tagModel);

    return this;
  }

  String getBodyForTagCreation() {
    return jsonEncode(
        {'label': label, 'cseType': cseType, 'cseKey': cseKey, 'color': color});
  }

  delete() async {
    NextcloudTagProvider.deleteTag(jsonEncode({'id': id}));
  }

  update(BuildContext context) async {
    TagViewModel updatedTag =
        await NextcloudTagProvider.updateTag(getBodyForTagUpdate());
    handleNewRevision(context, updatedTag.id, id);
  }

  String getBodyForTagUpdate() {
    return jsonEncode({
      'id': id,
      'revision': revision,
      'label': label,
      'cseType': cseType,
      'cseKey': cseKey,
      'color': color,
      'favorite': favorite
    });
  }

  void handleNewRevision(
      BuildContext context, String newRevisionID, oldRevisionID) {
    TagViewModel oldTag = TagViewModel.fromModel(this);
    TagViewModel restoredTag = TagViewModel();

    for (TagViewModel tag in context.read<TagListViewModel>().tagViewModels) {
      if (tag.id = oldRevisionID) {
        tag.revision = oldRevisionID;
      }

      restoredTag._tagModel.revision = restoredTag.revision = newRevisionID;
    }

    refreshViewModel(restoredTag);
    revisionViewModels.add(oldTag);
    _tagModel.revisions.add(oldTag._tagModel);

    notifyListeners();
  }
}
