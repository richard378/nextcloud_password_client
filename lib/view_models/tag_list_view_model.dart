// Dart imports:
// ignore_for_file: use_build_context_synchronously

// Dart imports:

// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:

// Project imports:
import 'package:nextcloud_password_client/interfaces/view_model_interface.dart';
import 'package:nextcloud_password_client/models/tag_model.dart';
import 'package:nextcloud_password_client/utils/data_access_layer.dart';
import 'package:nextcloud_password_client/utils/key_chain.dart';
import 'package:nextcloud_password_client/view_models/tag_view_model.dart';

class TagListViewModel extends ChangeNotifier implements ViewModelInterface {
  List<TagViewModel> _tagViewModels = [];
  DataAccessLayer dataAccessLayer;
  TagListViewModel({DataAccessLayer? dataAccessLayer})
      : dataAccessLayer = dataAccessLayer ?? DataAccessLayer();

  List<TagViewModel> get tagViewModels => _tagViewModels;

  set tagViewModels(List<TagViewModel> tagViewModels) {
    _tagViewModels = tagViewModels;
    notifyListeners();
  }

  @override
  Future<void> initialize() async {
    loadModel();
  }

  @override
  void loadModel() {
    List<TagModel> models = dataAccessLayer.loadTags();

    for (TagModel tag in models) {
      tagViewModels.add(TagViewModel.fromModel(tag));
    }

    notifyListeners();
  }

  @override
  Future<void> persistModel(
      [bool firstBool = false, bool secondBool = false]) async {
    List<TagModel> models = [];

    for (TagViewModel tag in tagViewModels) {
      models.add(tag.tagModel);
    }

    dataAccessLayer.persistTags(models);
  }

  TagViewModel getTagByLabel(KeyChain keyChain, String newTagLabel) {
    TagViewModel model = TagViewModel();

    for (TagViewModel tag in _tagViewModels) {
      String tagLabel = keyChain.decrypt(tag.cseKey, tag.label);

      if (tagLabel == newTagLabel) {
        model = tag;

        break;
      }
    }

    return model;
  }

  TagViewModel getTagById(String tagID) {
    TagViewModel model = TagViewModel();

    for (TagViewModel tag in _tagViewModels) {
      if (tag.id == tagID) {
        model = tag;

        break;
      }
    }

    return model;
  }

  @override
  void handleLogout() {
    _tagViewModels.clear();
  }

  List<TagViewModel> getFavoriteTags() {
    List<TagViewModel> tagViewModels = [];

    for (TagViewModel tagViewModel in _tagViewModels) {
      if (tagViewModel.favorite) {
        tagViewModels.add(tagViewModel);
      }
    }

    return tagViewModels;
  }

  void addTag(TagViewModel tag) {
    _tagViewModels.add(tag);
    notifyListeners();
  }

  void deleteTag({TagViewModel? tag, String? tagId}) {
    TagViewModel tagViewModel = TagViewModel();

    if (tagId != null) {
      tagViewModel = getTagById(tagId);
    } else if (tag != null) {
      tagViewModel = tag;
    } else {
      return;
    }
    _tagViewModels.remove(tagViewModel);
    tagViewModel.delete();
    removeTag(tagViewModel);
    notifyListeners();
  }

  @override
  void refreshDataModels() {
    for (TagViewModel tagViewModel in _tagViewModels) {
      tagViewModel.refreshModel(
          fromModel: tagViewModel, toModel: tagViewModel.tagModel);
    }
  }

  @override
  void refreshViewModels() {
    for (TagViewModel tagViewModel in _tagViewModels) {
      tagViewModel.refreshModel(
          fromModel: tagViewModel.tagModel, toModel: tagViewModel);
    }
  }

  void removeTag(TagViewModel tagViewModel) {
    _tagViewModels.remove(tagViewModel);
  }

  void updateTag(BuildContext context, TagViewModel tag) async {
    tag.update(context);
    _tagViewModels.setAll(_tagViewModels.indexOf(tag), [tag]);
  }
}
