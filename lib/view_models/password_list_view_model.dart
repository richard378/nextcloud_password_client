// Dart imports:
import 'dart:convert';

// Flutter imports:
import 'package:flutter/material.dart';
import 'package:nextcloud_password_client/provider/nextcloud_password_provider.dart';
import 'package:nextcloud_password_client/utils/key_chain.dart';
import 'package:nextcloud_password_client/view_models/tag_view_model.dart';

// Package imports:
import 'package:pluto_grid_plus/pluto_grid_plus.dart';
import 'package:provider/provider.dart';

// Project imports:
import 'package:nextcloud_password_client/constants/api_constants.dart';
import 'package:nextcloud_password_client/enums/nodes.dart';
import 'package:nextcloud_password_client/enums/security.dart';
import 'package:nextcloud_password_client/interfaces/view_model_interface.dart';
import 'package:nextcloud_password_client/models/password_model.dart';
import 'package:nextcloud_password_client/utils/data_access_layer.dart';
import 'package:nextcloud_password_client/utils/view_state.dart';
import 'package:nextcloud_password_client/view_models/credentials_view_model.dart';
import 'package:nextcloud_password_client/view_models/password_share_view_model.dart';
import 'package:nextcloud_password_client/view_models/password_view_model.dart';
import 'package:nextcloud_password_client/widgets/password_grid/password_row.dart';

import 'config_view_model.dart';

class PasswordListViewModel extends ChangeNotifier
    implements ViewModelInterface {
  DataAccessLayer dataAccessLayer;
  late PlutoGridStateManager plutoStateManager;
  Map<String, List<PasswordViewModel>> _passwordViewModels = {};
  List<PasswordViewModel> _filteredPasswordViewModels = [];
  List<PasswordViewModel> _filteredPasswordsBeforeSearch = [];
  List<PlutoRow> _passwordRows = [];
  List<PlutoRow> _passwordRowsBeforeSearch = [];
  bool _isSearching = false;
  int numberOfSecurePasswords = 0;
  int numberOfWeakPasswords = 0;
  int numberOfBreachedPasswords = 0;
  int numberOfSharedPasswordsByYou = 0;
  int numberOfSharedPasswordsWithYou = 0;
  Map<String, int> numberOfTaggedPasswords = {};
  Map<String, int> numberOfFolderPasswords = {};

  PasswordListViewModel({DataAccessLayer? dataAccessLayer})
      : dataAccessLayer = dataAccessLayer ?? DataAccessLayer();

  List<PasswordViewModel> get filteredPasswordViewModels =>
      _filteredPasswordViewModels;

  Map<String, List<PasswordViewModel>> get passwordViewModels =>
      _passwordViewModels;

  set passwordViewModels(
      Map<String, List<PasswordViewModel>> passwordViewModels) {
    _passwordViewModels = passwordViewModels;
    getPasswordsCount();
    notifyListeners();
  }

  void setFavoritePasswords(BuildContext context) {
    plutoStateManager.removeRows(_passwordRows);
    _passwordRows = [];
    _filteredPasswordViewModels = [];

    for (var passwordViewModels in passwordViewModels.values) {
      for (PasswordViewModel passwordViewModel in passwordViewModels) {
        if (passwordViewModel.favorite) {
          _filteredPasswordViewModels.add(passwordViewModel);
        }
      }
    }

    _passwordRows.addAll(getPasswordRows(context, _filteredPasswordViewModels));
    resetPlutoGrid(context);
  }

  void setPasswordsByFolder(BuildContext context, String folderID) {
    plutoStateManager.removeRows(_passwordRows);
    _filteredPasswordViewModels = [];
    _passwordRows = [];

    if (folderID.isEmpty || folderID == apiRootFolder) {
      for (var passwordViewModels in passwordViewModels.values) {
        _filteredPasswordViewModels.addAll(passwordViewModels);
        _passwordRows.addAll(getPasswordRows(context, passwordViewModels));
      }
    } else {
      if (_passwordViewModels.containsKey(folderID)) {
        _filteredPasswordViewModels.addAll(_passwordViewModels[folderID]!);
        _passwordRows
            .addAll(getPasswordRows(context, _passwordViewModels[folderID]!));
      }
    }

    resetPlutoGrid(context);
  }

  void searchPasswords(BuildContext context, String searchString) {
    plutoStateManager.removeRows(_passwordRows);

    if (searchString.isEmpty) {
      _isSearching = false;
      _filteredPasswordViewModels = _filteredPasswordsBeforeSearch;
      _passwordRows = _passwordRowsBeforeSearch;
      _filteredPasswordsBeforeSearch.clear();
      _passwordRowsBeforeSearch.clear();
    } else {
      _isSearching = true;
      KeyChain keyChain = context.read<ConfigViewModel>().keyChain;

      if (_filteredPasswordsBeforeSearch.isEmpty) {
        _filteredPasswordsBeforeSearch = _filteredPasswordViewModels;
        _passwordRowsBeforeSearch = _passwordRows;
      }

      _passwordRows.clear();
      _filteredPasswordViewModels.clear();

      for (var passwordViewModels in passwordViewModels.values) {
        for (PasswordViewModel passwordViewModel in passwordViewModels) {
          String searchVariables =
              '${keyChain.decrypt(passwordViewModel.cseKey, passwordViewModel.label)}_${keyChain.decrypt(passwordViewModel.cseKey, passwordViewModel.username)}_${keyChain.decrypt(passwordViewModel.cseKey, passwordViewModel.url)}_${keyChain.decrypt(passwordViewModel.cseKey, passwordViewModel.customFields)}'
                  .toLowerCase();

          if (searchVariables.contains(searchString.toLowerCase()) &&
              !_filteredPasswordViewModels.contains(passwordViewModel)) {
            _filteredPasswordViewModels.add(passwordViewModel);
          }
        }
      }

      _passwordRows
          .addAll(getPasswordRows(context, _filteredPasswordViewModels));
    }

    resetPlutoGrid(context);
  }

  @override
  Future<void> initialize() async {
    loadModel();
  }

  @override
  void loadModel() {
    List<PasswordModel> models = dataAccessLayer.loadPasswords();

    for (PasswordModel password in models) {
      if (!_passwordViewModels.containsKey(password.folder)) {
        _passwordViewModels[password.folder] = [];
      }

      _passwordViewModels[password.folder]!
          .add(PasswordViewModel.fromModel(password));
    }

    notifyListeners();
  }

  @override
  Future<void> persistModel(
      [bool firstBool = false, bool secondBool = false]) async {
    List<PasswordModel> models = [];

    for (List<PasswordViewModel> viewModels in _passwordViewModels.values) {
      for (PasswordViewModel password in viewModels) {
        models.add(password.passwordModel);
      }
    }

    dataAccessLayer.persistPasswords(models);
  }

  void removeTag(TagViewModel tagViewMdoel) {
    for (List<PasswordViewModel> passwords in _passwordViewModels.values) {
      for (PasswordViewModel password in passwords) {
        if (password.tags.contains(tagViewMdoel.id)) {
          password.tags.remove(tagViewMdoel.id);
        }
      }
    }
    notifyListeners();
  }

  PasswordViewModel getPasswordById(String passwordID) {
    PasswordViewModel model = PasswordViewModel();

    if (passwordID.isEmpty) {
      if (_passwordViewModels.isNotEmpty) {
        model = _passwordViewModels.values.first.first;
      }
    } else {
      _passwordViewModels.forEach((folderID, passwordList) {
        for (var password in passwordList) {
          if (password.id == passwordID) {
            model = password;
          }
        }
      });
    }

    return model;
  }

  @override
  void handleLogout() {
    _passwordViewModels.clear();
  }

  void setPasswordsByTag(BuildContext context, String id) {
    plutoStateManager.removeRows(_passwordRows);
    _filteredPasswordViewModels = [];
    _passwordRows = [];

    for (var passwordViewModels in passwordViewModels.values) {
      for (PasswordViewModel passwordViewModel in passwordViewModels) {
        if (passwordViewModel.tags.contains(id)) {
          _filteredPasswordViewModels.add(passwordViewModel);
        }
      }
    }

    _passwordRows.addAll(getPasswordRows(context, _filteredPasswordViewModels));
    resetPlutoGrid(context);
  }

  void setPasswordsByShare(BuildContext context, bool sharedByYou) {
    plutoStateManager.removeRows(_passwordRows);
    _filteredPasswordViewModels = [];
    _passwordRows = [];

    for (var passwordViewModels in passwordViewModels.values) {
      for (PasswordViewModel passwordViewModel in passwordViewModels) {
        for (PasswordShareViewModel share
            in passwordViewModel.shareViewModels) {
          if (sharedByYou) {
            if (share.owner['id'] ==
                context.read<CredentialsViewModel>().userName) {
              if (!_filteredPasswordViewModels.contains(passwordViewModel)) {
                _filteredPasswordViewModels.add(passwordViewModel);
              }
            }
          } else {
            if (share.receiver['id'] ==
                context.read<CredentialsViewModel>().userName) {
              if (!_filteredPasswordViewModels.contains(passwordViewModel)) {
                _filteredPasswordViewModels.add(passwordViewModel);
              }
            }
          }
        }
      }
    }

    _passwordRows.addAll(getPasswordRows(context, _filteredPasswordViewModels));
    resetPlutoGrid(context);
  }

  void setSecurePasswords(BuildContext context, Security security) {
    plutoStateManager.removeRows(_passwordRows);
    _filteredPasswordViewModels = [];
    _passwordRows = [];

    for (var passwordViewModels in passwordViewModels.values) {
      for (PasswordViewModel passwordViewModel in passwordViewModels) {
        switch (security) {
          case Security.secure:
            if (passwordViewModel.status == 0) {
              _filteredPasswordViewModels.add(passwordViewModel);
            }

            break;

          case Security.weak:
            if (passwordViewModel.status == 1) {
              _filteredPasswordViewModels.add(passwordViewModel);
            }

            break;

          case Security.breached:
            if (passwordViewModel.status == 2) {
              _filteredPasswordViewModels.add(passwordViewModel);
            }

            break;
        }
      }
    }

    _passwordRows.addAll(getPasswordRows(context, _filteredPasswordViewModels));
    resetPlutoGrid(context);
  }

  void resetPlutoGrid(BuildContext context) {
    plutoStateManager.appendRows(_passwordRows);

    if (plutoStateManager.getSortedColumn != null) {
      switch (plutoStateManager.getSortedColumn!.sort) {
        case PlutoColumnSort.none:
          break;

        case PlutoColumnSort.ascending:
          plutoStateManager.sortAscending(plutoStateManager.getSortedColumn!);

          break;

        case PlutoColumnSort.descending:
          plutoStateManager.sortDescending(plutoStateManager.getSortedColumn!);

          break;
      }
    }

    plutoStateManager.notifyListeners();
  }

  void setRecentPasswords(BuildContext context) {
    plutoStateManager.removeRows(_passwordRows);
    _filteredPasswordViewModels = [];
    _passwordRows = [];

    List<PasswordViewModel> sortedPasswordViewModels = [];

    for (var passwordViewModels in passwordViewModels.values) {
      sortedPasswordViewModels.addAll(passwordViewModels);
    }

    sortedPasswordViewModels.sort();

    for (int i = 1; i <= 15; i++) {
      _filteredPasswordViewModels.add(sortedPasswordViewModels[i]);
    }

    _passwordRows.addAll(getPasswordRows(context, _filteredPasswordViewModels));
    resetPlutoGrid(context);
  }

  void setPasswordsInitially(BuildContext context) {
    if (!_isSearching) {
      ViewState viewState = context.read<ViewState>();

      switch (viewState.selectedNode) {
        case Nodes.all:
          setPasswordsByFolder(context, apiRootFolder);

          break;

        case Nodes.folder:
          setPasswordsByFolder(context, viewState.selectedFolder);

          break;

        case Nodes.recent:
          setRecentPasswords(context);

          break;

        case Nodes.favorite:
          if (viewState.selectedFolder.isNotEmpty) {
            setPasswordsByFolder(context, viewState.selectedFolder);
          } else if (viewState.selectedTag.isNotEmpty) {
            setPasswordsByTag(context, viewState.selectedTag);
          } else {
            setFavoritePasswords(context);
          }

          break;

        case Nodes.shared:
          setPasswordsByShare(context, viewState.sharedByYou);

          break;

        case Nodes.tag:
          setPasswordsByTag(context, viewState.selectedTag);

          break;

        case Nodes.security:
          setSecurePasswords(context, viewState.selectedSecurity);

          break;
      }
    }
  }

  PlutoRow? getRowByPassword(String selectedPassword) {
    PlutoRow? plutoRow;

    for (PlutoRow row in _passwordRows) {
      if (row.key.toString() == selectedPassword) {
        plutoRow = row;
      }
    }

    return plutoRow;
  }

  @override
  void refreshDataModels() {
    for (var passwordViewModels in passwordViewModels.values) {
      for (PasswordViewModel passwordViewModel in passwordViewModels) {
        passwordViewModel.refreshDataModel(passwordViewModel);
      }
    }
  }

  @override
  void refreshViewModels() {
    for (var passwordViewModels in passwordViewModels.values) {
      for (PasswordViewModel passwordViewModel in passwordViewModels) {
        passwordViewModel.refreshViewModel(passwordViewModel.passwordModel);
      }
    }
  }

  void reassignPasswordToFolder(
      BuildContext context, PasswordViewModel password) {
    for (List<PasswordViewModel> passwords in _passwordViewModels.values) {
      passwords.removeWhere((element) => element.id == password.id);
    }

    _passwordViewModels[password.folder]?.add(password);
    setPasswordsInitially(context);
  }

  void delete(BuildContext context,
      {String? passwordId, PasswordViewModel? password}) async {
    late PasswordViewModel passwordViewModel;

    if (passwordId != null) {
      passwordViewModel = getPasswordById(passwordId);
    } else if (password != null) {
      passwordViewModel = password;
    } else {
      return;
    }

    NextcloudPasswordProvider.deletePassword(
        jsonEncode({'id': passwordViewModel.id}));

    for (List<PasswordViewModel> pws in passwordViewModels.values) {
      pws.remove(passwordViewModel);
    }

    _filteredPasswordViewModels.remove(passwordViewModel);
    plutoStateManager.removeRows(_passwordRows);
    _passwordRows.clear();
    _passwordRows.addAll(getPasswordRows(context, _filteredPasswordViewModels));

    resetPlutoGrid(context);
    notifyListeners();
  }

  void getPasswordsCount() {
    numberOfSecurePasswords = 0;
    numberOfWeakPasswords = 0;
    numberOfBreachedPasswords = 0;
    numberOfSharedPasswordsByYou = 0;
    numberOfSharedPasswordsWithYou = 0;
    numberOfTaggedPasswords = {};
    numberOfFolderPasswords = {};
    int apiRootFolderCount = 0;

    for (var passwordViewModels in _passwordViewModels.values) {
      for (PasswordViewModel passwordViewModel in passwordViewModels) {
        apiRootFolderCount += 1;
        numberOfFolderPasswords.addAll({apiRootFolder: apiRootFolderCount});

        switch (passwordViewModel.status) {
          case 0:
            numberOfSecurePasswords += 1;

            break;

          case 1:
            numberOfWeakPasswords += 1;

            break;

          case 2:
            numberOfBreachedPasswords += 1;

            break;
        }

        if (passwordViewModel.shared) {
          numberOfSharedPasswordsByYou += 1;
        }

        if (passwordViewModel.shares.isNotEmpty) {
          numberOfSharedPasswordsWithYou += 1;
        }

        for (String tag in passwordViewModel.tags) {
          if (numberOfTaggedPasswords.containsKey(tag)) {
            int count = numberOfTaggedPasswords[tag]!;
            count += 1;
            numberOfTaggedPasswords.update(tag, (value) => count);
          } else {
            numberOfTaggedPasswords.addAll({tag: 1});
          }
        }

        if (numberOfFolderPasswords.containsKey(passwordViewModel.folder)) {
          int count = numberOfFolderPasswords[passwordViewModel.folder]!;
          count += 1;
          numberOfFolderPasswords.update(
              passwordViewModel.folder, (value) => count);
        } else {
          numberOfFolderPasswords.addAll({passwordViewModel.folder: 1});
        }
      }
    }
  }
}
