// Dart imports:
import 'dart:core';

// Package imports:
import 'package:hive_flutter/hive_flutter.dart';

// Project imports:
import 'package:nextcloud_password_client/models/base_model.dart';
import 'package:nextcloud_password_client/models/password_share_model.dart';

part 'password_model.g.dart';

@HiveType(typeId: 4)
class PasswordModel extends BaseModel {

  /// Username associated with the password
  @HiveField(13)
  String username = '';

  /// The actual password
  @HiveField(14)
  String password = '';

  ///Url of the website
  @HiveField(15)
  String url = '';

  /// Notes for the password. Can be formatted with Markdown
  @HiveField(16)
  String notes = '';

  /// Custom fields created by the user. (See custom fields)
  @HiveField(17)
  String customFields = '';

  /// Security status level of the password (0 = ok, 1 = user rules violated, 2 = breached)
  @HiveField(18)
  int status = 0;

  /// Specific code for the current security status (GOOD, OUTDATED, DUPLICATE, BREACHED)
  @HiveField(19)
  String statusCode = '';

  /// SHA1 hash of the password
  @HiveField(20)
  String hash = '';

  /// UUID of the current folder of the password
  @HiveField(21)
  String folder = '';

  /// UUID of the share if the password was shared by someone else with the user
  @HiveField(22)
  PasswordShareModel share = PasswordShareModel();

  /// True if the password is shared with other users
  @HiveField(23)
  bool shared = false;

  /// Specifies if the encrypted properties can be changed. Might be false for shared passwords
  @HiveField(24)
  bool editable = false;

  ///list of all revisions
  @HiveField(25)
  List<PasswordModel> revisions = [];

  ///list of all tags
  @HiveField(26)
  List<String> tags = [];

  ///list of all shares
  @HiveField(27)
  List<PasswordShareModel> shares = [];

  ///favIcon as base64 String
  @HiveField(28)
  String favIconString = '';

  @override
  List<Object?> get props => super.props
    ..addAll([
      username,
      password,
      url,
      notes,
      customFields,
      status,
      statusCode,
      hash,
      folder,
      share,
      shared,
      editable,
      revision,
      tags,
      shares,
      favIconString
    ]);

  PasswordModel();

}
