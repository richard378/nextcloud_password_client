// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'password_model.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class PasswordModelAdapter extends TypeAdapter<PasswordModel> {
  @override
  final int typeId = 4;

  @override
  PasswordModel read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return PasswordModel()
      ..username = fields[13] as String
      ..password = fields[14] as String
      ..url = fields[15] as String
      ..notes = fields[16] as String
      ..customFields = fields[17] as String
      ..status = fields[18] as int
      ..statusCode = fields[19] as String
      ..hash = fields[20] as String
      ..folder = fields[21] as String
      ..share = fields[22] as PasswordShareModel
      ..shared = fields[23] as bool
      ..editable = fields[24] as bool
      ..revisions = (fields[25] as List).cast<PasswordModel>()
      ..tags = (fields[26] as List).cast<String>()
      ..shares = (fields[27] as List).cast<PasswordShareModel>()
      ..favIconString = fields[28] as String
      ..id = fields[0] as String
      ..label = fields[1] as String
      ..revision = fields[2] as String
      ..cseType = fields[3] as String
      ..cseKey = fields[4] as String
      ..sseType = fields[5] as String
      ..client = fields[6] as String
      ..hidden = fields[7] as bool
      ..trashed = fields[8] as bool
      ..favorite = fields[9] as bool
      ..created = fields[10] as int
      ..updated = fields[11] as int
      ..edited = fields[12] as int;
  }

  @override
  void write(BinaryWriter writer, PasswordModel obj) {
    writer
      ..writeByte(29)
      ..writeByte(13)
      ..write(obj.username)
      ..writeByte(14)
      ..write(obj.password)
      ..writeByte(15)
      ..write(obj.url)
      ..writeByte(16)
      ..write(obj.notes)
      ..writeByte(17)
      ..write(obj.customFields)
      ..writeByte(18)
      ..write(obj.status)
      ..writeByte(19)
      ..write(obj.statusCode)
      ..writeByte(20)
      ..write(obj.hash)
      ..writeByte(21)
      ..write(obj.folder)
      ..writeByte(22)
      ..write(obj.share)
      ..writeByte(23)
      ..write(obj.shared)
      ..writeByte(24)
      ..write(obj.editable)
      ..writeByte(25)
      ..write(obj.revisions)
      ..writeByte(26)
      ..write(obj.tags)
      ..writeByte(27)
      ..write(obj.shares)
      ..writeByte(28)
      ..write(obj.favIconString)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.label)
      ..writeByte(2)
      ..write(obj.revision)
      ..writeByte(3)
      ..write(obj.cseType)
      ..writeByte(4)
      ..write(obj.cseKey)
      ..writeByte(5)
      ..write(obj.sseType)
      ..writeByte(6)
      ..write(obj.client)
      ..writeByte(7)
      ..write(obj.hidden)
      ..writeByte(8)
      ..write(obj.trashed)
      ..writeByte(9)
      ..write(obj.favorite)
      ..writeByte(10)
      ..write(obj.created)
      ..writeByte(11)
      ..write(obj.updated)
      ..writeByte(12)
      ..write(obj.edited);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is PasswordModelAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
