// Package imports:
import 'package:equatable/equatable.dart';
import 'package:hive_flutter/hive_flutter.dart';

// Project imports:
import 'package:nextcloud_password_client/interfaces/password_objects_interface.dart';

part 'base_model.g.dart';

@HiveType(typeId: 13)
class BaseModel extends PasswordObjectsInterface with EquatableMixin {
  /// The UUID of the object
  @HiveField(0)
  String id = '';

  /// User defined label of the object
  @HiveField(1)
  String label = '';

  /// UUID of the current revision
  @HiveField(2)
  String revision = '';
  
  /// Type of the used client side encryption
  @HiveField(3)
  String cseType = '';

  /// UUID of the key used for client side encryption
  @HiveField(4)
  String cseKey = '';

  /// Type of the used server side encryption
  @HiveField(5)
  String sseType = '';

  /// Name of the client which created this revision
  @HiveField(6)
  String client = '';

  /// Hides the folder in list / find actions
  @HiveField(7)
  bool hidden = false;

  /// True if the folder is in the trash
  @HiveField(8)
  bool trashed = false;

  /// True if the user has marked the folder as favorite
  @HiveField(9)
  bool favorite = false;

  /// Unix timestamp when the folder was created
  @HiveField(10)
  int created = 0;

  /// Unix timestamp when the folder was updated
  @HiveField(11)
  int updated = 0;

  /// Unix timestamp when the user last changed the folder name
  @HiveField(12)
  int edited = 0;


  @override
  List<Object?> get props => [
        id,
        label,
        revision,
        cseType,
        cseKey,
        sseType,
        client,
        hidden,
        trashed,
        favorite,
        created,
        updated
      ];
}
