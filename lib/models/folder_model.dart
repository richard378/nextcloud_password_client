// Package imports:
import 'package:hive_flutter/hive_flutter.dart';

// Project imports:
import 'package:nextcloud_password_client/models/base_model.dart';

part 'folder_model.g.dart';

@HiveType(typeId: 3)
class FolderModel extends BaseModel {
  
  /// UUID of the parent folder
  @HiveField(13)
  String parent = '';

  /// every passwords object gets revisions if changed
  @HiveField(14)
  List<FolderModel> revisions = [];

  @override
  List<Object?> get props => super.props..addAll([parent, revision]);
  
  FolderModel();

}
