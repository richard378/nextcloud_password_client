// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:equatable/equatable.dart';
import 'package:hive_flutter/hive_flutter.dart';

part 'server_config_model.g.dart';

@HiveType(typeId: 7)
class ServerConfigModel extends ChangeNotifier with EquatableMixin {

  ///  The strength of generated passwords, a value between 0 (weak) and 4 (strong)
  @HiveField(0)
  int userPasswordGeneratorStrength = 1;

  ///Whether or not generated passwords contain numbers
  @HiveField(1)
  bool userPasswordGeneratorNumbers = false;

  ///Whether or not generated passwords contain special characters
  @HiveField(2)
  bool userPasswordGeneratorSpecial = false;

  ///Whether or not the server should check passwords for duplicates
  @HiveField(3)
  bool usePasswordSecurityDuplicates = true;

  /// Marks passwords a weak if they surpass the specified date mark. 0 is off.
  @HiveField(4)
  int usePasswordSecurityAge = 0;

  ///How many characters of the has should be saved. Can be either 0, 20, 30 or 40. For e2e this is implemented by the client, count characters from the beginning of the string
  @HiveField(5)
  int userPasswordSecurityHash = 40;

  /// Whether or not the user receives mails about security issues
  @HiveField(6)
  bool useMailSecurity = true;

  ///Whether or not the user receives mails about new shared objects
  @HiveField(7)
  bool userMailShares = false;

  /// Whether or not the user receives notifications about security issues
  @HiveField(8)
  bool userNotificationSecurity = true;

  ///Whether or not the user receives notifications about new shared objects
  @HiveField(9)
  bool userNotificationShares = true;

  ///Whether or not the user receives notifications about background errors
  @HiveField(10)
  bool userNotificationErrors = true;

  ///Whether or not the user receives notifications about configuration issues. Only for admins
  @HiveField(11)
  bool userNotificationAdmin = true;

  ///The server side encryption type. 0 = None, 1 = SSEv1, 2 = SSEv1
  @HiveField(12)
  int userEncryptionSse = 1;

  ///The client side encryption type. 0 = None, 1 = CSEv1
  @HiveField(13)
  int userEncryptionCse = 0;

  ///Whether or not the editable option should be set by default for a new share. Not writeable if sharing disabled.
  @HiveField(14)
  bool userSharingEditable = true;

  ///Whether or not the resharing option should be set by default for a new share. Not writeable if sharing or resharing disabled.
  @HiveField(15)
  bool userSharingResharing = true;

  ///The session lifetime in seconds
  @HiveField(16)
  int userSessionLifetime = 600;

  ///The Nextcloud version of the server, e.g. "20"
  @HiveField(17)
  String serverVersion = '';

  ///The app version of the server, e.g. "2020.9"
  @HiveField(18)
  String serverAppVersion = '';

  ///The base url of the server
  @HiveField(19)
  String serverBaseUrl = '';

  ///The base url of the WebDav service used for file storage
  @HiveField(20)
  String serverBaseUrlWebdav = '';

  ///Whether or not sharing is enabled globally
  @HiveField(21)
  bool serverSharingEnabled = true;

  ///Whether or not resharing shared entities is enabled globally
  @HiveField(22)
  bool serverSharingResharing = true;

  ///Whether or not the auto-complete request works
  @HiveField(23)
  bool serverSharingAutocomplete = true;

  ///List of supported sharing types.
  @HiveField(24)
  List<String> serverSharingTypes = ["user"];

  ///The color of the current Nextcloud theme
  @HiveField(25)
  String serverThemeColorPrimary = '#745bca';

  ///The text color of the current Nextcloud theme
  @HiveField(26)
  String serverThemeColorText = '#ffffff';

  ///The background color of the current Nextcloud theme
  @HiveField(27)
  String serverThemeColorBackground = '#ffffff';

  ///The url to the current Nextcloud background image
  @HiveField(28)
  String serverThemeBackground = '';

  ///The url to the logo of the current Nextcloud theme
  @HiveField(29)
  String serverThemeLogo = '';

  ///The name of the Nextcloud instance
  @HiveField(30)
  String serverThemeLabel = "Nextcloud";

  ///The url to the current svg app icon
  @HiveField(31)
  String serverThemeAppIcon = '';

  ///The url to the current svg folder icon
  @HiveField(32)
  String serverThemeFolderIcon = '';

  ///The base url of the in-app user handbook
  @HiveField(33)
  String serverHandbookUrl = '';

  ///An integer value indicating the server performance preference. 0 is a slow server where requests should be avoided (max 1 simultaneous request), 5 is a fast server which can handle many requests (x * 3 simultaneous requests), 6 is for unlimited requests.
  @HiveField(34)
  int serverPerformance = 2;

  ServerConfigModel();

  @override
  List<Object?> get props => [
        userPasswordGeneratorStrength,
        userPasswordGeneratorNumbers,
        userPasswordGeneratorSpecial,
        usePasswordSecurityDuplicates,
        usePasswordSecurityAge,
        userPasswordSecurityHash,
        useMailSecurity,
        userMailShares,
        userNotificationSecurity,
        userNotificationShares,
        userNotificationErrors,
        userNotificationAdmin,
        userEncryptionSse,
        userEncryptionCse,
        userSharingEditable,
        userSharingResharing,
        userSessionLifetime,
        serverVersion,
        serverAppVersion,
        serverBaseUrl,
        serverBaseUrlWebdav,
        serverSharingEnabled,
        serverSharingResharing,
        serverSharingAutocomplete,
        serverSharingTypes,
        serverThemeColorPrimary,
        serverThemeColorText,
        serverThemeColorBackground,
        serverThemeBackground,
        serverThemeLogo,
        serverThemeLabel,
        serverThemeAppIcon,
        serverThemeFolderIcon,
        serverHandbookUrl,
        serverPerformance
      ];
}
