// Flutter imports:
// ignore_for_file: use_build_context_synchronously

// Flutter imports:

// Dart imports:
import 'dart:io';

// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:locale_switcher/locale_switcher.dart';

// Project imports:
import 'package:nextcloud_password_client/screens/window_border.dart';
import 'package:nextcloud_password_client/utils/http_utils.dart';
import 'package:nextcloud_password_client/widgets/single_elements/label.dart';

// Package imports:

// Project imports:

class MissingDependenciesScreen extends StatefulWidget {

  const MissingDependenciesScreen({super.key});

  @override
  State<StatefulWidget> createState() => _MissingDependenciesScreenState();
}

class _MissingDependenciesScreenState extends State<MissingDependenciesScreen> {

  @override
  Widget build(BuildContext context) {

    return Scaffold(

      appBar: getWindowBorder(context, false),
      body: 
      
      Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [

          LocaleSwitcher.menu(
              title: AppLocalizations.of(context)!.languageSelection),

          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [

              Label(AppLocalizations.of(context)!.initError),

              InkWell(
                child: 
                Text(
                  AppLocalizations.of(context)!.installationInstructionGeneral,
                  style: const TextStyle(color: Colors.blueAccent),
                ),

                onTap: () => HttpUtils().openUrl(
                    'https://gitlab.com/j0chn/nextcloud_password_client/-/wikis/Installation-Guide'),
              ),

              if (Platform.isWindows)
              
                Text(AppLocalizations.of(context)!
                    .installationInstructionWindows),
              
              if (Platform.isLinux)
              
                Text(
                    AppLocalizations.of(context)!.installationInstructionLinux),
              
              if (Platform.isMacOS)
              
                Text(
                    AppLocalizations.of(context)!.installationInstructionMacOS),
            
            ],

          ),

        ],

      ),

    );

  }
  
}
