// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:provider/provider.dart';

// Project imports:
import 'package:nextcloud_password_client/utils/key_chain.dart';
import 'package:nextcloud_password_client/view_models/config_view_model.dart';
import 'package:nextcloud_password_client/view_models/password_view_model.dart';
import 'package:nextcloud_password_client/widgets/dialogues/confirm_password_revision_dialouge.dart';
import 'package:nextcloud_password_client/widgets/dialogues/password_revision_dialogue.dart';
import 'package:nextcloud_password_client/widgets/formatted_date.dart';

Widget getPasswordRevisionTab(
    BuildContext context, 
    PasswordViewModel password) {

  KeyChain keyChain = context.read<ConfigViewModel>().keyChain;
  
  return ListView.builder(
  
    itemCount: password.revisionViewModels.length,
    itemBuilder: (BuildContext context, int index) {

      PasswordViewModel passwordRevisionMViewModel =
          password.revisionViewModels.elementAt(index);
      
      String date = formattedDate(
        passwordRevisionMViewModel.created.toString().toString()
      );

      return ListTile(

        onTap: () async {
        
          await PasswordRevisionDialouge.showPasswordRevisionDialogue(
              context, passwordRevisionMViewModel);
        
        },

        title: 
        
        Text(keyChain.decrypt(
          passwordRevisionMViewModel.cseKey,
          passwordRevisionMViewModel.label)
        ),
        
        subtitle: 
        Text(date),
        
        trailing: 
        IconButton(
          icon: const Icon(Icons.restore),
          onPressed: () => ConfirmPasswordrevisionDialouge
              .showConfirmPasswordrevisionDialouge(
                context, 
                passwordRevisionMViewModel.id
              ),
        ),

        leading: const Icon(Icons.eleven_mp),
  
      );

    },

  );

}
