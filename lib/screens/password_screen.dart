// Flutter imports:
// ignore_for_file: use_build_context_synchronously

// Flutter imports:
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

// Package imports:
import 'package:multi_split_view/multi_split_view.dart';
import 'package:nextcloud_password_client/controller/textfield_controller.dart';
import 'package:nextcloud_password_client/widgets/single_elements/general_text_input.dart';
import 'package:provider/provider.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

// Project imports:
import 'package:nextcloud_password_client/provider/nextcloud_provider.dart';
import 'package:nextcloud_password_client/screens/password_detail_screen.dart';
import 'package:nextcloud_password_client/screens/password_folder_screen.dart';
import 'package:nextcloud_password_client/screens/password_grid_screen.dart';
import 'package:nextcloud_password_client/screens/window_border.dart';
import 'package:nextcloud_password_client/utils/key_chain.dart';
import 'package:nextcloud_password_client/utils/view_state.dart';
import 'package:nextcloud_password_client/view_models/config_view_model.dart';
import 'package:nextcloud_password_client/view_models/credentials_view_model.dart';
import 'package:nextcloud_password_client/view_models/password_list_view_model.dart';
import 'package:nextcloud_password_client/view_models/password_view_model.dart';
import 'package:nextcloud_password_client/widgets/dialogues/client_password_dialogue.dart';

class PasswordScreen extends StatefulWidget {
  const PasswordScreen({super.key});

  @override
  State<StatefulWidget> createState() => _PasswordScreenState();
}

class _PasswordScreenState extends State<PasswordScreen> {

  MultiSplitViewController? folderSplitController;
  List<LogicalKeyboardKey> shortcut = [];
  MultiSplitViewController? passwordSplitController;
  
  @override
  void initState() {
  
    RawKeyboard.instance.addListener(_handleKeyDown);
  
    ViewState viewState = context.read<ViewState>();
  
    super.initState();
  
    WidgetsBinding.instance.addPostFrameCallback((_) async {
  
      if (context.read<CredentialsViewModel>().clientPassword.isNotEmpty) {
  
        if (!viewState.clientAuthenticated) {
  
          await showClientPasswordDialog(context);
  
        }
  
      }
  
      if (viewState.relogin) {
  
        await relogin(context);
  
      }
  
      NextcloudProvider.retrieveObjects(
          context, context.read<ConfigViewModel>().$refreshRateInSeconds);
    });

  }

  @override
  Widget build(BuildContext context) {

    if (folderSplitController == null) {
    
      passwordSplitController = MultiSplitViewController(areas: [
        Area(flex: context.read<ViewState>().passwordSplitWeights[0], min:0.2,  builder: (context, area) {
           return passwordGridScreen(context);
        }),
        Area(flex: context.read<ViewState>().passwordSplitWeights[1], min:0.2,builder: (context, area) {
            return passwordDetailScreen(context);
          })
      ]);

    MultiSplitView passwordSplitView = MultiSplitView(
      onDividerDragEnd: (int i) => _savePasswordSplitWeights(context),
      axis: Axis.vertical,
      controller: passwordSplitController,
    );

      folderSplitController = MultiSplitViewController(areas: [
        Area(flex: context.read<ViewState>().folderSplitWeights[0],  min:0.1, builder: (context, area) {
            return passwordFolderScreen(context);
          }),
        Area(flex: context.read<ViewState>().folderSplitWeights[1],min:0.5, builder: (context, area) {
            return passwordSplitView;
        })
      ]);
    }

    MultiSplitView folderSplitView = MultiSplitView(
      controller: folderSplitController,
      onDividerDragEnd: ( int i ) => _saveFolderSplitWeights(context),
      axis: Axis.horizontal,
    );

    Column content = Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: 
        
        <Widget>[
          SizedBox(
              height: 50,
              width: 200,
              child: getGeneralTextInput(
                TextFieldController.searchController,
                label: AppLocalizations.of(context)!.searchPassword,
                onChanged: (value) {
                  context
                      .read<PasswordListViewModel>()
                      .searchPasswords(context, value);
                },
              
              )
              ),

          Expanded(
            child:
            
            MultiSplitViewTheme(
                data: MultiSplitViewThemeData(
                    dividerThickness: 1,
                    dividerPainter:
                        DividerPainters.background(color: Colors.white)
                      ),
                child: folderSplitView),
          )

        ]

        );

    return context.watch<ViewState>().clientAuthenticated ||
            context.read<CredentialsViewModel>().clientPassword.isEmpty
        ? Scaffold(appBar: getWindowBorder(
          context, true), 
          body: content
        )
        : Scaffold(
            appBar: getWindowBorder(context, true),
            body: const SizedBox.shrink());
  }

  void _handleKeyDown(RawKeyEvent value) {

    if (value is RawKeyDownEvent) {
    
      final k = value.logicalKey;
      shortcut.add(k);
    
    }

    if (value is RawKeyUpEvent) {
    
      final k = value.logicalKey;
      shortcut.remove(k);
    
    }
    
    if ((shortcut.contains(LogicalKeyboardKey.controlLeft) ||
        shortcut.contains(LogicalKeyboardKey.controlRight)) &&
        shortcut.contains(LogicalKeyboardKey.keyC)) {

      String passwordID = context.read<ViewState>().selectedPassword;
      KeyChain keyChain = context.read<ConfigViewModel>().keyChain;

      PasswordViewModel password =
          context.read<PasswordListViewModel>().getPasswordById(passwordID);

      Clipboard.setData(ClipboardData(
          text: keyChain.decrypt(
            password.cseKey, 
            password.password)
          )
        );
    
    }

    if ((shortcut.contains(LogicalKeyboardKey.controlLeft) ||
            shortcut.contains(LogicalKeyboardKey.controlRight)) &&
        shortcut.contains(LogicalKeyboardKey.keyB)) {

      String passwordID = context.read<ViewState>().selectedPassword;
      KeyChain keyChain = context.read<ConfigViewModel>().keyChain;
      
      PasswordViewModel password =
          context.read<PasswordListViewModel>().getPasswordById(passwordID);
      
      Clipboard.setData(ClipboardData(
          text: keyChain.decrypt(
            password.cseKey, 
            password.username
          )
        )
      );
    
    }
  }


  Future<void> relogin(BuildContext context) async {

    await NextcloudProvider.handleRelogin(context);
  
  }

  Future<void> showClientPasswordDialog(BuildContext context) async {

    await ClientPasswordDialogue.showClientPasswordDialogue(context);
  
  }

  void _savePasswordSplitWeights(BuildContext context) {
  
    context.read<ViewState>().passwordSplitWeights = [
      passwordSplitController!.areas[0].flex!.toDouble(),
      passwordSplitController!.areas[1].flex!.toDouble()
    ];
  
  }

  void _saveFolderSplitWeights(BuildContext context) {
  
    context.read<ViewState>().folderSplitWeights = [
      folderSplitController!.areas[0].flex!.toDouble(),
      folderSplitController!.areas[1].flex!.toDouble()
    ];
  
  }

}