// Flutter imports:
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

// Package imports:
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:locale_switcher/locale_switcher.dart';
import 'package:nextcloud_password_client/widgets/dropdowns/theme_selection_dropdown%20.dart';
import 'package:provider/provider.dart';

// Project imports:
import 'package:nextcloud_password_client/constants/settings_list_items.dart';
import 'package:nextcloud_password_client/controller/textfield_controller.dart';
import 'package:nextcloud_password_client/provider/nextcloud_provider.dart';
import 'package:nextcloud_password_client/utils/view_state.dart';
import 'package:nextcloud_password_client/view_models/config_view_model.dart';

Widget settingsDetailScreen(
    BuildContext context, 
    SettingsItems selectedSetting) {

  switch (selectedSetting) {
    
    case SettingsItems.login:
    
      return TextButton(
        onPressed: () {
          context.read<ViewState>().handleLogout(context);
        },
        child: Text(AppLocalizations.of(context)!.logout),
      );

    case SettingsItems.system:
      
      return Container(
        alignment: Alignment.center,
        child: Expanded(
          child: 
          
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: [

              LocaleSwitcher.menu(
                  title: AppLocalizations.of(context)!.languageSelection),
              
              const Divider(height: 2, color: Colors.black),
              
              Text(AppLocalizations.of(context)!.syncRate),
              
              TextField(
                controller: TextFieldController.refreshRateController,
                keyboardType: TextInputType.number,
                showCursor: true,
                inputFormatters: [FilteringTextInputFormatter.digitsOnly],
              ),
              
              TextButton(
                  onPressed: () {
                    context.read<ConfigViewModel>().refreshRateInSeconds =
                        int.parse(
                            TextFieldController.refreshRateController.text);
                    NextcloudProvider.retrieveObjects(
                        context,
                        int.parse(
                            TextFieldController.refreshRateController.text));
                  },

                  child: 
                  Text(AppLocalizations.of(context)!.save))
            ],

          ),

        ),

      );

    case SettingsItems.themes:
  
      return const ThemeSelectionDropDown();

    case SettingsItems.about:
  
      return Text(AppLocalizations.of(context)!.about);
  
  }

}
