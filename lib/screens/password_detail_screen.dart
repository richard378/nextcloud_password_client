// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';

// Project imports:
import 'package:nextcloud_password_client/screens/password_detail_details_tab.dart';
import 'package:nextcloud_password_client/screens/password_detail_notes_tab.dart';
import 'package:nextcloud_password_client/screens/password_detail_revision_tab.dart';
import 'package:nextcloud_password_client/screens/password_detail_share_tab.dart';
import 'package:nextcloud_password_client/utils/view_state.dart';
import 'package:nextcloud_password_client/view_models/password_list_view_model.dart';
import 'package:nextcloud_password_client/view_models/password_view_model.dart';
import 'package:nextcloud_password_client/widgets/password_details/general_password_information_row.dart';

Widget passwordDetailScreen(BuildContext context) {

  PasswordViewModel password = context
      .watch<PasswordListViewModel>()
      .getPasswordById(context.watch<ViewState>().selectedPassword);

  return context.watch<PasswordListViewModel>().passwordViewModels.isEmpty ||
          context.watch<ViewState>().hideData
      ? const CircularProgressIndicator()
      : DefaultTabController(
          length: 4, // length of tabs
          initialIndex: 0,
          child: 
          
          Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisSize: MainAxisSize.max,
            children: 
            
            <Widget>[
              generalPasswordInformationRow(context, password),
            
              SizedBox(
                height: 30,
                child: 
                
                TabBar(
                  labelColor: Colors.green,
                  unselectedLabelColor: Colors.black,
                  tabs: [
                   
                    Tab(text: AppLocalizations.of(context)!.detailsColumn),
                   
                    Tab(text: AppLocalizations.of(context)!.notesColumn),
                   
                    Tab(text: AppLocalizations.of(context)!.share),
                   
                    Tab(text: AppLocalizations.of(context)!.revisionColumn),
                  ],

                ),

              ),

              Expanded(
                child: 
                
                Container(
                  
                  decoration: const
                   BoxDecoration(

                      border: 
                      Border(
                          top: BorderSide(color: Colors.grey, width: 0.5))),
                  
                  child: 
                  
                  TabBarView(
                    children: 
                    
                    <Widget>[
                    
                      Container(child: getPasswordDetailTab(context, password)),
                    
                      Container(
                        child: getPasswordNotesTab(context, password),
                      ),
                    
                      Container(
                        child: getPasswordShareTab(context, password),
                      ),
                    
                      Container(
                        child: getPasswordRevisionTab(context, password),
                      ),

                    ],

                  ),

                ),

              )

            ],

          ),

        );
        
}
