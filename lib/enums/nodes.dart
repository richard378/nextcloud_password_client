// Package imports:
import 'package:hive_flutter/hive_flutter.dart';

part 'nodes.g.dart';

@HiveType(typeId: 12)
enum Nodes {
  @HiveField(0)
  all,
  @HiveField(1)
  folder,
  @HiveField(2)
  recent,
  @HiveField(3)
  shared,
  @HiveField(4)
  security,
  @HiveField(5)
  tag,
  @HiveField(6)
  favorite
}
