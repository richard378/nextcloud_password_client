// Package imports:
import 'package:hive_flutter/hive_flutter.dart';

part 'themes.g.dart';

@HiveType(typeId: 2)
enum Themes {
  @HiveField(0)
  dark,
  @HiveField(1)
  system,
  @HiveField(2)
  light,
  @HiveField(3)
  nextcloud,
  @HiveField(4)
  custom
}
