// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'themes.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class ThemesAdapter extends TypeAdapter<Themes> {
  @override
  final int typeId = 2;

  @override
  Themes read(BinaryReader reader) {
    switch (reader.readByte()) {
      case 0:
        return Themes.dark;
      case 1:
        return Themes.system;
      case 2:
        return Themes.light;
      case 3:
        return Themes.nextcloud;
      case 4:
        return Themes.custom;
      default:
        return Themes.dark;
    }
  }

  @override
  void write(BinaryWriter writer, Themes obj) {
    switch (obj) {
      case Themes.dark:
        writer.writeByte(0);
        break;
      case Themes.system:
        writer.writeByte(1);
        break;
      case Themes.light:
        writer.writeByte(2);
        break;
      case Themes.nextcloud:
        writer.writeByte(3);
        break;
      case Themes.custom:
        writer.writeByte(4);
        break;
    }
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ThemesAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
