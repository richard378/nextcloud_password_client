// Dart imports:
import 'dart:async';

// Package imports:

// Project imports:
import 'package:nextcloud_password_client/constants/api_constants.dart';
import 'package:nextcloud_password_client/utils/http_utils.dart';

class NextcloudShareProvider {
  static Future<void> updateShare(String shareBody) async {
    HttpUtils().httpUpdate(apiShareUpdate, body: shareBody);
  }

  static Future<void> deleteShare(String shareBody) async {
    HttpUtils().httpDelete(apiShareDelete, body: shareBody);
  }
}
