// Dart imports:
import 'dart:async';
import 'dart:convert';

// Package imports:
import 'package:http/http.dart' as http;

// Project imports:
import 'package:nextcloud_password_client/constants/api_constants.dart';
import 'package:nextcloud_password_client/utils/http_utils.dart';
import 'package:nextcloud_password_client/view_models/tag_view_model.dart';

class NextcloudTagProvider {
  static Future<List<TagViewModel>> retrieveTags() async {
    http.Response response = await HttpUtils().httpPost(apiGetTagList,
        body: jsonEncode({'details': apiGetTagDetail}));

    var body = json.decode(response.body);

    List<TagViewModel> models = [];

    for (Map<String, dynamic> tag in body) {
      TagViewModel tagViewModel = TagViewModel.fromMap(tag);

      models.add(tagViewModel);
    }

    return models;
  }

  static Future<bool> deleteTag(String tagBody) async {
    http.Response response =
        await HttpUtils().httpDelete(apiPostTagDelete, body: tagBody);

    return response.statusCode == 200;
  }

  static Future<TagViewModel> createTag(String body) async {
    TagViewModel newTag = TagViewModel();

    http.Response tagCreationResponse =
        await HttpUtils().httpPost(apiPostTagCreate, body: body);
    if (tagCreationResponse.statusCode == 201) {
      var body = jsonDecode(tagCreationResponse.body);

      newTag = await showTag(body['id']);
    }
    return newTag;
  }

  static Future<TagViewModel> showTag(String tagId) async {
    TagViewModel newTag = TagViewModel();

    http.Response tagShowResponse = await HttpUtils()
        .httpPost(apiPostTagShow, body: jsonEncode({"id": tagId}));

    if (tagShowResponse.statusCode == 200) {
      newTag = TagViewModel.fromMap(jsonDecode(tagShowResponse.body));
    }

    return newTag;
  }

  static Future<TagViewModel> updateTag(String body) async {
    TagViewModel newTag = TagViewModel();
    http.Response tagUpdateResponse =
        await HttpUtils().httpUpdate(apiPatchTagUpdate, body: body);
    if (tagUpdateResponse.statusCode == 200) {
      newTag = TagViewModel.fromMap(jsonDecode(tagUpdateResponse.body));
    }

    return newTag;
  }
}
