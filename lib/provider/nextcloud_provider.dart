// Dart imports:
// ignore_for_file: use_build_context_synchronously

// Dart imports:
import 'dart:async';

// Flutter imports:
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nextcloud_password_client/provider/nextcloud_user_provider.dart';
import 'package:nextcloud_password_client/view_models/folder_view_model.dart';
import 'package:nextcloud_password_client/view_models/password_view_model.dart';
import 'package:nextcloud_password_client/view_models/user_list_view_model.dart';

// Package imports:
import 'package:provider/provider.dart';

// Project imports:
import 'package:nextcloud_password_client/constants/api_constants.dart';
import 'package:nextcloud_password_client/constants/exception_constants.dart';
import 'package:nextcloud_password_client/exceptions/credentials_exception.dart';
import 'package:nextcloud_password_client/exceptions/general_exception.dart';
import 'package:nextcloud_password_client/exceptions/master_password_exception.dart';
import 'package:nextcloud_password_client/provider/nextcloud_auth_provider.dart';
import 'package:nextcloud_password_client/provider/nextcloud_folder_provider.dart';
import 'package:nextcloud_password_client/provider/nextcloud_password_provider.dart';
import 'package:nextcloud_password_client/provider/nextcloud_server_config_provider.dart';
import 'package:nextcloud_password_client/provider/nextcloud_tag_provider.dart';
import 'package:nextcloud_password_client/utils/view_state.dart';
import 'package:nextcloud_password_client/view_models/config_view_model.dart';
import 'package:nextcloud_password_client/view_models/credentials_view_model.dart';
import 'package:nextcloud_password_client/view_models/folder_list_view_model.dart';
import 'package:nextcloud_password_client/view_models/password_list_view_model.dart';
import 'package:nextcloud_password_client/view_models/server_config_view_model.dart';
import 'package:nextcloud_password_client/view_models/tag_list_view_model.dart';
import 'package:nextcloud_password_client/widgets/dialogues/credential_dialogue.dart';
import 'package:nextcloud_password_client/widgets/dialogues/master_password_dialogue.dart';
import 'package:nextcloud_password_client/widgets/loading_overlay.dart';

class NextcloudProvider {

  static late String url;
  static late String userName;
  static late String password;
  static late String session;
  static Timer? timer;

  static Future<void> retrieveObjects(
      BuildContext context, int waitTime) async {

    if (timer != null) {
      timer!.cancel();
    }

    timer = Timer.periodic(Duration(seconds: waitTime), (_) async {
      
      Map<String,List<FolderViewModel>> folderViewModels = context.read<FolderListViewModel>().folderViewModels;
      Map<String, List<PasswordViewModel>> passwordViewModels = context.read<PasswordListViewModel>().passwordViewModels;
      
      if (((userName.isNotEmpty && password.isNotEmpty) ||
           (folderViewModels.isEmpty && passwordViewModels.isEmpty)) &&
          !context.read<ViewState>().inEditMode) {

        FolderListViewModel folderListViewModel =
            context.read<FolderListViewModel>();

        PasswordListViewModel passwordListViewModel =
            context.read<PasswordListViewModel>();

        ServerConfigViewModel serverConfigViewModel =
            context.read<ServerConfigViewModel>();

        TagListViewModel tagListViewModel = context.read<TagListViewModel>();
        
        UserListViewModel userListViewModel =
            context.read<UserListViewModel>();

        try {

          await Future.wait([
            
            receiveServerConfig(serverConfigViewModel),
            retrieveFolders(folderListViewModel),
            retrieveTags(tagListViewModel),
            retrievePasswords(passwordListViewModel, context),
            retrieveUsers(userListViewModel),
            
          ]);

          passwordListViewModel.setPasswordsInitially(context);

          if (context.read<ConfigViewModel>().useLocalCopy) {

            serverConfigViewModel.persistConfig();
            folderListViewModel.persistModel();
            passwordListViewModel.persistModel();
            tagListViewModel.persistModel();

          }

        } catch (error) {

          ConfigViewModel configViewModel = context.read<ConfigViewModel>();
          
          if (configViewModel.loginSucceeded) {
          
            configViewModel.loginSucceeded = false;

            await handleRelogin(context);
            await logIntoNextcloud(context, false);

            ViewState viewState = context.read<ViewState>();
          
            if (viewState.credentialInputError ||
          
                viewState.masterPasswordInputError) {
          
              return;
          
            }
          
          }
        
        }
      
      }
   
    });
  
  }

  static Future<void> retrievePasswords(
    PasswordListViewModel passwordListViewModel, 
    BuildContext context) async {

    passwordListViewModel.passwordViewModels =
        await NextcloudPasswordProvider.retrievePasswords(context);
  
  }

  static Future<void> retrieveUsers(
      UserListViewModel userListViewModel) async {

    userListViewModel.userViewModels =
        await NextcloudUserProvider.retrieveUsers();
  
  }

  static Future<void> retrieveFolders(
      FolderListViewModel folderListViewModel) async {
    folderListViewModel.folderViewModels =
        await NextcloudFolderProvider.retrieveFolders();
  }

  static Future<void> receiveServerConfig(
      ServerConfigViewModel serverConfigViewModel) async {

    serverConfigViewModel.serverConfigModel =
        await NextcloudServerConfigProvider.retrieveServerConfig();

  }

  static Future<void> retrieveTags(TagListViewModel tagListViewModel) async {

    tagListViewModel.tagViewModels = await NextcloudTagProvider.retrieveTags();
 
  }

  static Future<void> handleRelogin(BuildContext context) async {

    ConfigViewModel configViewModel = context.read<ConfigViewModel>();

    CredentialsViewModel credentialsViewModel =
        context.read<CredentialsViewModel>();

    ViewState viewState = context.read<ViewState>();

    url = apiProtocol + configViewModel.serverURL;
    userName = credentialsViewModel.userName;
    password = credentialsViewModel.password;
    session = configViewModel.session;
    bool ignoreMessage = viewState.ignoreMissingCredentials;
    
    while ((userName.isEmpty || password.isEmpty) && ignoreMessage == false) {

      await CredentialDialogue.showCredentialsDialogue(context);

      ignoreMessage = viewState.ignoreMissingCredentials;
      userName = credentialsViewModel.userName;
      password = credentialsViewModel.password;

    }

  }

  static Future<void> logIntoNextcloud(
      BuildContext context, 
      bool displayLoadingOverlay) async {

    ViewState viewState = context.read<ViewState>();

    if (displayLoadingOverlay) {
    
      LoadingOverlay.of(context).show();
    
    }
    
    while (true) {
    
      try {
    
        await NextcloudAuthProvider().handleLogin(context);

        handleSuccessfulLogin(context);

        if (displayLoadingOverlay) {

          LoadingOverlay.of(context).hide();

        }

        break;

      } catch (error) {

        if (error is GeneralException) {

          GeneralException exception = error;
          
          if (error.runtimeType == MasterPasswordException) {

            if (exception.errorCode == masterpasswordWrongErrorCode) {

              viewState.masterPasswordInputError = true;

            }

            if (displayLoadingOverlay) {

              LoadingOverlay.of(context).hide();

            }

            await MasterPasswordDialogue.showMasterPasswordDialogue(context);
            
            if (displayLoadingOverlay) {
            
              LoadingOverlay.of(context).show();
            
            }
            
            if (viewState.masterPasswordInputAnswer) {
            
              if (displayLoadingOverlay) {
            
                LoadingOverlay.of(context).hide();
            
              }
            
              return;
            
            }

          } else if (error.runtimeType == CredentialsException) {

            viewState.credentialInputError = true;
            
            if (displayLoadingOverlay) {
            
              LoadingOverlay.of(context).hide();
            
            }
            
            return;
          }

        }

      }
    
    }

  }

  static void handleSuccessfulLogin(BuildContext context) {

    ConfigViewModel configViewModel = context.read<ConfigViewModel>();
    
    ViewState viewState = context.read<ViewState>();
    
    configViewModel.loginSucceeded = true;
    viewState.credentialInputError = false;
    viewState.masterPasswordInputError = false;
    viewState.clientPasswordInputError = false;
    configViewModel.persistModel();
    
    context.read<CredentialsViewModel>().persistModel(
        configViewModel.saveCredentials, 
        configViewModel.saveMasterPassword
      );

    viewState.isLoggedIn = true;
    viewState.clientAuthenticated = true;
  }
  
}
