// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

// Project imports:
import 'package:nextcloud_password_client/controller/textfield_controller.dart';
import 'package:nextcloud_password_client/widgets/single_elements/password_input.dart';

Widget clientPassword(BuildContext context) {
  return Container(
    margin: const EdgeInsets.symmetric(vertical: 10),
    alignment: Alignment.centerLeft,
    child: PasswordText(TextFieldController.clientPasswordFieldController,
        labelText: AppLocalizations.of(context)!.clientPassword),
  );
}
