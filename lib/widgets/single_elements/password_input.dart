// Flutter imports:
// ignore_for_file: library_private_types_in_public_api

// Flutter imports:
import 'package:flutter/material.dart';

// Project imports:
import 'package:nextcloud_password_client/widgets/single_elements/general_text_input.dart';

class PasswordText extends GeneralTextInput {
  const PasswordText(super.textEditingController,
      {super.key, super.labelText, super.errorText, super.onChanged});

  @override
  _PasswordTextFieldState createState() => _PasswordTextFieldState();
}

class _PasswordTextFieldState extends GeneralTextInputState {
  bool _obscureText = true;

  @override
  Widget build(BuildContext context) {
    return PasswordTextField(
      widget.textEditingController,
      _obscureText,
      PasswordInputDeco(
        IconButton(
          onPressed: () {
            _obscureText = !_obscureText;
            setState(
              () {},
            );
          },
          icon: Icon(_obscureText ? Icons.visibility : Icons.visibility_off),
        ),
        true,
        widget.errorText,
        widget.labelText,
      ),
      onChanged: widget.onChanged,
    );
  }
}

class PasswordInputDeco extends GeneralInputDeco {
  const PasswordInputDeco(suffixIcon, isDense, errorText, labelText)
      : super(
            isDense: isDense,
            suffixIcon: suffixIcon,
            labelText: labelText,
            errorText: errorText);
}

class PasswordTextField extends GeneralTextField {
  const PasswordTextField(super.controller, obscureText, super.decoration,
      {super.key, super.onChanged})
      : super(obscureText: obscureText);
}
