// Flutter imports:
import 'package:flutter/material.dart';

class Label extends Text {
  const Label(super.text, {super.style, super.key});
}
