// Flutter imports:
import 'package:flutter/material.dart';

Widget getGeneralTextInput(TextEditingController textController,
    {String? label,
    String? prefix,
    String? error,
    bool? obscure,
    ValueChanged<String>? onChanged}) {
  return TextField(
    controller: textController,
    showCursor: true,
    decoration: getInputDecoration(label: label, prefix: prefix, error: error),
    onChanged: onChanged,
  );
}

InputDecoration getInputDecoration({
  String? label,
  String? prefix,
  String? error,
  Widget? suffixIcon,
  bool? isDense,
}) {
  return InputDecoration(
    labelText: label,
    prefixText: prefix,
    errorText: error,
    border: const UnderlineInputBorder(),
    isDense: isDense,
    suffixIcon: suffixIcon,
  );
}

class GeneralTextInput extends StatefulWidget {
  final TextEditingController textEditingController;
  final String? labelText;
  final String? prefixText;
  final String? errorText;
  final ValueChanged<String>? onChanged;
  const GeneralTextInput(this.textEditingController,
      {super.key,
      this.labelText,
      this.prefixText,
      this.errorText,
      this.onChanged});

  @override
  GeneralTextInputState createState() => GeneralTextInputState();
}

class GeneralTextInputState extends State<GeneralTextInput> {
  @override
  Widget build(BuildContext context) {
    return GeneralTextField(
      widget.textEditingController,
      GeneralInputDeco(
          labelText: widget.labelText,
          prefixText: widget.prefixText,
          errorText: widget.errorText),
      onChanged: widget.onChanged,
    );
  }
}

class GeneralInputDeco extends InputDecoration {
  const GeneralInputDeco(
      {super.isDense = false,
      super.suffixIcon,
      super.labelText = '',
      super.prefixText = '',
      super.errorText})
      : super(
          border: const UnderlineInputBorder(),
        );
}

class GeneralTextField extends TextField {
  const GeneralTextField(
      TextEditingController controller, InputDecoration decoration,
      {super.obscureText = false, super.onChanged, super.key})
      : super(
            obscuringCharacter: '*',
            showCursor: true,
            decoration: decoration,
            controller: controller);
}
