// Flutter imports:
import 'package:flutter/material.dart';
import 'package:nextcloud_password_client/utils/view_state.dart';
import 'package:nextcloud_password_client/view_models/password_list_view_model.dart';
import 'package:nextcloud_password_client/view_models/password_view_model.dart';
import 'package:provider/provider.dart';

class PasswordLabel extends StatefulWidget {
  const PasswordLabel(String text, {super.key});

  @override
  PasswordLabelState createState() => PasswordLabelState();
}

class PasswordLabelState extends State<PasswordLabel> {
  @override
  Widget build(BuildContext context) {
    PasswordViewModel pw = context
        .read<PasswordListViewModel>()
        .getPasswordById(context.read<ViewState>().selectedPassword);

    return PasswordLabel(pw.label.toString());
  }
}
