// Flutter imports:
// ignore_for_file: library_private_types_in_public_api, use_build_context_synchronously

// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';

// Project imports:
import 'package:nextcloud_password_client/constants/api_constants.dart';
import 'package:nextcloud_password_client/controller/textfield_controller.dart';
import 'package:nextcloud_password_client/utils/view_state.dart';
import 'package:nextcloud_password_client/view_models/config_view_model.dart';
import 'package:nextcloud_password_client/widgets/single_elements/general_text_input.dart';

Widget serverConnection(BuildContext context) {
  ConnectionCheckButton checkButton = const ConnectionCheckButton();

  return Row(
    mainAxisAlignment: MainAxisAlignment.start,
    crossAxisAlignment: CrossAxisAlignment.center,
    children: [
      Expanded(
        child: getGeneralTextInput(TextFieldController.urlTextFieldController,
            onChanged: ((value) {
          context.read<ViewState>().isURLReachable = false;
          checkButton.resetState();
        }), label: AppLocalizations.of(context)!.enterURL, prefix: apiProtocol),
      ),
      Container(
        height: 50,
        width: 50,
        margin: const EdgeInsets.only(left: 10),
        child: checkButton,
      ),
    ],
  );
}

class ConnectionCheckButton extends StatefulWidget {
  static bool _loading = false;
  static _ConnectionCheckButtonState state = _ConnectionCheckButtonState();
  const ConnectionCheckButton({super.key});

  @override
  // ignore: no_logic_in_create_state
  _ConnectionCheckButtonState createState() => state;
  resetState() => state.resetState();
}

class _ConnectionCheckButtonState extends State<ConnectionCheckButton> {
  String _toolTip = '';
  Icon _icon = const Icon(Icons.offline_bolt);

  @override
  Widget build(BuildContext context) {
    _toolTip = AppLocalizations.of(context)!.checkURLReachability;

    return ConnectionCheckButton._loading
        ? const CircularProgressIndicator()
        : FloatingActionButton(
            heroTag: 'serverConnectionButton',
            onPressed: () async => _checkConnection(),
            tooltip: _toolTip,
            child: _icon,
          );
  }

  resetState() {
    _toolTip = '';
    _icon = const Icon(Icons.offline_bolt);
    setState(() {});
  }

  _checkConnection() async {
/*    final directory = await getApplicationDocumentsDirectory();
    final path = directory.path + '/' + npcFolder + '/webview';

    final webview = await WebviewWindow.create(
      configuration: CreateConfiguration(
        windowHeight: 880,
        userDataFolderWindows: path,
        windowWidth: 720,
        title: "Nextcloud Login",
      ),
    );
    webview
      ..setApplicationNameForUserAgent(" Nextcloud_Password_Client")
      ..launch(apiProtocol +
          TextFieldController.urlTextFieldController.text +
          '/index.php/login/v2/flow?OCS-APIREQUEST=true')
      ..addOnUrlRequestCallback((url) {
        print(url);
      });
*/

    ConnectionCheckButton._loading = true;
    ConfigViewModel configViewModel = context.read<ConfigViewModel>();
    ViewState viewState = context.read<ViewState>();

    setState(() {});

    configViewModel.serverURL =
        apiProtocol + TextFieldController.urlTextFieldController.text;

    bool result =
        await viewState.checkURLreachability(configViewModel.serverURL);

    ConnectionCheckButton._loading = false;

    if (result) {
      _toolTip = AppLocalizations.of(context)!.serverReached;
      _icon = const Icon(Icons.bolt, color: Colors.green);
    } else {
      _toolTip = AppLocalizations.of(context)!.serverNotReached;
      _icon = const Icon(Icons.bolt, color: Colors.red);
    }

    setState(() {});
  }
}
