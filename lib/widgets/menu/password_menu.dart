// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

// Project imports:
import 'package:nextcloud_password_client/constants/menu_constants.dart';
import 'package:nextcloud_password_client/widgets/menu/selection_handler.dart';

Widget getPasswordMenu(BuildContext context) {
  return PopupMenuButton<String>(
    tooltip: AppLocalizations.of(context)!.passwordMenu,
    onSelected: (value) => handleMenuItemSelection(context, value),
    itemBuilder: (context) => <PopupMenuEntry<String>>[
      PopupMenuItem<String>(
        value: createPassword,
        child: ListTile(
          leading: const Icon(Icons.add),
          title: Text(AppLocalizations.of(context)!.createPassword),
        ),
      ),
      PopupMenuItem<String>(
        value: editPassword,
        child: ListTile(
          leading: const Icon(Icons.edit),
          title: Text(AppLocalizations.of(context)!.editPassword),
        ),
      ),
      PopupMenuItem<String>(
        value: deletePassword,
        child: ListTile(
          leading: const Icon(Icons.delete),
          title: Text(AppLocalizations.of(context)!.deletePassword),
        ),
      ),
    ],
    child: Text(AppLocalizations.of(context)!.password),
  );
}
