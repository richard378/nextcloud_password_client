// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

// Project imports:
import 'package:nextcloud_password_client/constants/menu_constants.dart';
import 'package:nextcloud_password_client/widgets/menu/selection_handler.dart';

Widget getTagMenu(BuildContext context) {
  return PopupMenuButton<String>(
    tooltip: AppLocalizations.of(context)!.tagMenu,
    onSelected: (value) => handleMenuItemSelection(context, value),
    itemBuilder: (context) => <PopupMenuEntry<String>>[
      PopupMenuItem<String>(
        value: createTag,
        child: ListTile(
          leading: const Icon(Icons.add),
          title: Text(AppLocalizations.of(context)!.createTag),
        ),
      ),
      PopupMenuItem<String>(
        value: editTag,
        child: ListTile(
          leading: const Icon(Icons.edit),
          title: Text(AppLocalizations.of(context)!.editTag),
        ),
      ),
      PopupMenuItem<String>(
        value: deleteTag,
        child: ListTile(
          leading: const Icon(Icons.delete),
          title: Text(AppLocalizations.of(context)!.deleteTag),
        ),
      ),
    ],
    child: Text(AppLocalizations.of(context)!.tag),
  );
}
