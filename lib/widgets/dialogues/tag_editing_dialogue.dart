// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:nextcloud_password_client/constants/widget_constants.dart';
import 'package:nextcloud_password_client/view_models/tag_list_view_model.dart';
import 'package:nextcloud_password_client/view_models/tag_view_model.dart';
import 'package:nextcloud_password_client/widgets/single_elements/general_text_input.dart';
import 'package:provider/provider.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';

// Project imports:
import 'package:nextcloud_password_client/controller/textfield_controller.dart';
import 'package:nextcloud_password_client/utils/key_chain.dart';
import 'package:nextcloud_password_client/utils/view_state.dart';
import 'package:nextcloud_password_client/view_models/config_view_model.dart';
import 'package:nextcloud_password_client/widgets/single_elements/label.dart';

class TagEditingDialogue {
  static Future<void> showTagEditingDialogue(
      BuildContext context, String id) async {
    context.read<ViewState>().inEditMode = true;

    TagViewModel tag = context.read<TagListViewModel>().getTagById(id);

    KeyChain keyChain = context.read<ConfigViewModel>().keyChain;

    TextFieldController.tagTextController.text =
        keyChain.decrypt(tag.cseKey, tag.label);

    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        // create some values
        return AlertDialog(
          scrollable: true,
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Label(AppLocalizations.of(context)!.nameColumn),
                        SizedBox(
                          width: 400,
                          height: 70,
                          child: GeneralTextInput(
                              TextFieldController.tagTextController),
                        ),
                      ],
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        ColorPicker(
                            pickerColor: Color(int.parse(keyChain
                                .decrypt(tag.cseKey, tag.color)
                                .replaceFirst(colorHashTag, colorPrefix))),
                            onColorChanged: (value) => tag.color =
                                keyChain.encrypt(
                                    colorToHex(value,
                                        enableAlpha: false,
                                        includeHashSign: true),
                                    keyChain.type,
                                    keyChain.current))
                      ],
                    )
                  ],
                ),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text(AppLocalizations.of(context)!.cancel),
              onPressed: () {
                _closePopUp(context);
              },
            ),
            TextButton(
              child: Text(AppLocalizations.of(context)!.save),
              onPressed: () {
                _updateTag(context, tag);
              },
            )
          ],
        );
      },
    );
  }
}

void _updateTag(BuildContext context, TagViewModel tag) {
  KeyChain keyChain = context.read<ConfigViewModel>().keyChain;

  tag.label = keyChain.encrypt(TextFieldController.tagTextController.text,
      keyChain.type, keyChain.current);

  TextFieldController.tagTextController.clear();

  tag.refreshModel(fromModel: tag, toModel: tag.tagModel);

  TagListViewModel tagListViewModel = context.read<TagListViewModel>();

  tagListViewModel.updateTag(context, tag);

  _closePopUp(context);
}

void _closePopUp(BuildContext context) {
  context.read<ViewState>().inEditMode = false;

  TextFieldController.tagTextController.clear();

  Navigator.of(context).pop();
}
