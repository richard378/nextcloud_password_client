// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:nextcloud_password_client/utils/view_state.dart';
import 'package:nextcloud_password_client/view_models/password_list_view_model.dart';
import 'package:nextcloud_password_client/view_models/password_view_model.dart';

// Project imports:
import 'package:nextcloud_password_client/widgets/single_elements/label.dart';
import 'package:provider/provider.dart';

class ConfirmPasswordrevisionDialouge {
  static Future<void> showConfirmPasswordrevisionDialouge(
      BuildContext context, String revisionId) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Label(
            AppLocalizations.of(context)!.confirmRevisionTitle,
            style: const TextStyle(fontWeight: FontWeight.w700),
          ),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Label(
                  AppLocalizations.of(context)!.confirmRevisionText,
                ),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text(AppLocalizations.of(context)!.cancel),
              onPressed: () {
                _cancel(context);
              },
            ),
            TextButton(
              child: Text(AppLocalizations.of(context)!.restore),
              onPressed: () {
                _setPasswordRevision(context, revisionId);
              },
            )
          ],
        );
      },
    );
  }
}

void _setPasswordRevision(BuildContext context, String revisionID) {
  PasswordViewModel passwordViewModel = context
      .read<PasswordListViewModel>()
      .getPasswordById(context.read<ViewState>().selectedPassword);

  passwordViewModel.restorePassword(context, revisionID);
  Navigator.of(context).pop();
}

void _cancel(BuildContext context) {
  Navigator.of(context).pop();
}
