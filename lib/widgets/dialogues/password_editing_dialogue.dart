// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:nextcloud_password_client/widgets/dropdowns/folder_selection_dropdown.dart';
import 'package:provider/provider.dart';
import 'package:simple_markdown_editor/simple_markdown_editor.dart';

// Project imports:
import 'package:nextcloud_password_client/controller/textfield_controller.dart';
import 'package:nextcloud_password_client/utils/key_chain.dart';
import 'package:nextcloud_password_client/utils/view_state.dart';
import 'package:nextcloud_password_client/view_models/config_view_model.dart';
import 'package:nextcloud_password_client/view_models/password_list_view_model.dart';
import 'package:nextcloud_password_client/view_models/password_view_model.dart';
import 'package:nextcloud_password_client/widgets/single_elements/general_text_input.dart';
import 'package:nextcloud_password_client/widgets/single_elements/label.dart';
import 'package:nextcloud_password_client/widgets/single_elements/password_input.dart';

class PasswordEditingDialogue {
  static Future<void> showPasswordEditDialogue(BuildContext context) async {
    context.read<ViewState>().inEditMode = true;

    KeyChain keyChain = context.read<ConfigViewModel>().keyChain;

    PasswordViewModel passwordViewModel = context
        .read<PasswordListViewModel>()
        .getPasswordById(context.read<ViewState>().selectedPassword);

    String originalFolderID = passwordViewModel.folder;

    TextFieldController.passwordEditingFieldController.text =
        keyChain.decrypt(passwordViewModel.cseKey, passwordViewModel.password);

    TextFieldController.userNameEditingFieldController.text =
        keyChain.decrypt(passwordViewModel.cseKey, passwordViewModel.username);

    TextFieldController.urlEditingTextFieldController.text =
        keyChain.decrypt(passwordViewModel.cseKey, passwordViewModel.url);

    TextFieldController.passwordNameEditingFieldController.text =
        keyChain.decrypt(passwordViewModel.cseKey, passwordViewModel.label);

    TextFieldController.notesEditingFieldController.text =
        keyChain.decrypt(passwordViewModel.cseKey, passwordViewModel.notes);

    FolderSelectionDropDown folderSelectionDropDown = FolderSelectionDropDown();

    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          scrollable: true,
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Label(AppLocalizations.of(context)!.passwordColumn),
                        SizedBox(
                          width: 400,
                          height: 70,
                          child: PasswordText(TextFieldController
                              .passwordEditingFieldController),
                        ),
                        const SizedBox(
                          width: 40,
                          height: 70,
                        ),
                        Label(AppLocalizations.of(context)!.usernameColumn),
                        SizedBox(
                          width: 400,
                          height: 70,
                          child: GeneralTextInput(TextFieldController
                              .userNameEditingFieldController),
                        ),
                      ],
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Label(AppLocalizations.of(context)!.nameColumn),
                        SizedBox(
                          width: 400,
                          height: 70,
                          child: GeneralTextInput(TextFieldController
                              .passwordNameEditingFieldController),
                        ),
                        Label(AppLocalizations.of(context)!.urlColumn),
                        SizedBox(
                          width: 400,
                          height: 70,
                          child: GeneralTextInput(TextFieldController
                              .urlEditingTextFieldController),
                        ),
                        Label(AppLocalizations.of(context)!.folder),
                        folderSelectionDropDown,
                      ],
                    ),
                    Row(
                      children: [
                        SafeArea(
                          child: SizedBox(
                            width: 800,
                            height: 400,
                            child: Container(
                              height: 300,
                              decoration: BoxDecoration(
                                border: Border.all(color: Colors.black),
                                borderRadius: BorderRadius.circular(20),
                              ),
                              child: MarkdownFormField(
                                controller: TextFieldController
                                    .notesEditingFieldController,
                                enableToolBar: true,
                                emojiConvert: true,
                                autoCloseAfterSelectEmoji: false,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text(AppLocalizations.of(context)!.cancel),
              onPressed: () {
                _closePopUp(context);
              },
            ),
            TextButton(
              child: Text(AppLocalizations.of(context)!.save),
              onPressed: () {
                passwordViewModel.folder = folderSelectionDropDown.folder.id;
                _save(context, passwordViewModel, originalFolderID);
              },
            )
          ],
        );
      },
    );
  }
}

void _closePopUp(BuildContext context) {
  TextFieldController.passwordEditingFieldController.clear();
  TextFieldController.userNameEditingFieldController.clear();
  TextFieldController.urlEditingTextFieldController.clear();
  TextFieldController.passwordNameEditingFieldController.clear();
  TextFieldController.notesEditingFieldController.clear();
  context.read<ViewState>().inEditMode = false;
  Navigator.of(context).pop();
}

void _save(BuildContext context, PasswordViewModel passwordViewModel,
    String originalFolderID) {
  PasswordViewModel passwordRevision =
      PasswordViewModel.fromModel(passwordViewModel);

  passwordRevision.folder = originalFolderID;

  KeyChain keyChain = context.read<ConfigViewModel>().keyChain;

  passwordViewModel.label = keyChain.encrypt(
      TextFieldController.passwordNameEditingFieldController.text,
      passwordViewModel.cseType,
      passwordViewModel.cseKey);

  passwordViewModel.password = keyChain.encrypt(
      TextFieldController.passwordEditingFieldController.text,
      passwordViewModel.cseType,
      passwordViewModel.cseKey);

  passwordViewModel.username = keyChain.encrypt(
      TextFieldController.userNameEditingFieldController.text,
      passwordViewModel.cseType,
      passwordViewModel.cseKey);

  passwordViewModel.url = keyChain.encrypt(
      TextFieldController.urlEditingTextFieldController.text,
      passwordViewModel.cseType,
      passwordViewModel.cseKey);

  passwordViewModel.notes = keyChain.encrypt(
      TextFieldController.notesEditingFieldController.text,
      passwordViewModel.cseType,
      passwordViewModel.cseKey);

  passwordViewModel.revisionViewModels.add(passwordRevision);
  passwordViewModel.refreshDataModel(passwordViewModel);
  passwordViewModel.updatePassword(context);

  _closePopUp(context);
}
