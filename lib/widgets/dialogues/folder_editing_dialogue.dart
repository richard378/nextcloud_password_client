// Flutter imports:
// ignore_for_file: use_build_context_synchronously

// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:nextcloud_password_client/controller/textfield_controller.dart';
import 'package:nextcloud_password_client/utils/key_chain.dart';
import 'package:nextcloud_password_client/view_models/config_view_model.dart';
import 'package:nextcloud_password_client/view_models/folder_list_view_model.dart';
import 'package:nextcloud_password_client/view_models/folder_view_model.dart';
import 'package:nextcloud_password_client/widgets/dropdowns/folder_selection_dropdown.dart';
import 'package:nextcloud_password_client/widgets/single_elements/general_text_input.dart';

// Project imports:
import 'package:nextcloud_password_client/widgets/single_elements/label.dart';
import 'package:provider/provider.dart';

class FolderEditingDialogue {
  static Future<void> showFolderEditingDialogue(
      BuildContext context, String id) async {
    KeyChain keyChain = context.read<ConfigViewModel>().keyChain;

    FolderListViewModel folderListViewModel =
        context.read<FolderListViewModel>();

    FolderViewModel folderViewModel = folderListViewModel.getFolderById(id);

    TextFieldController.folderNameController.text =
        keyChain.decrypt(folderViewModel.cseKey, folderViewModel.label);

    var folderSelectionDropdown = FolderSelectionDropDown();

    folderSelectionDropdown.folder =
        folderListViewModel.getDirectParent(context, folderViewModel.parent);

    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Label(AppLocalizations.of(context)!.editFolder),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Column(children: <Widget>[
                  Row(
                    children: <Widget>[
                      Label(AppLocalizations.of(context)!.folderName),
                      SizedBox(
                          width: 400,
                          height: 70,
                          child: GeneralTextInput(
                              TextFieldController.folderNameController))
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Label(AppLocalizations.of(context)!.parentFolder),
                      folderSelectionDropdown,
                    ],
                  )
                ]),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text(AppLocalizations.of(context)!.cancel),
              onPressed: () {
                _closePopUp(context);
              },
            ),
            TextButton(
              child: Text(AppLocalizations.of(context)!.save),
              onPressed: () {
                _updateFolder(context, folderViewModel,
                    folderSelectionDropdown.folder.id);
              },
            ),
          ],
        );
      },
    );
  }
}

void _closePopUp(BuildContext context) {
  TextFieldController.folderNameController.clear();
  Navigator.of(context).pop();
}

void _updateFolder(
    BuildContext context, FolderViewModel folder, String parentFolder) async {
  KeyChain keyChain = context.read<ConfigViewModel>().keyChain;
  folder.label = keyChain.encrypt(TextFieldController.folderNameController.text,
      folder.cseType, folder.cseKey);
  folder.parent = parentFolder;
  context.read<FolderListViewModel>().updateFolder(context, folder);
  _closePopUp(context);
}
