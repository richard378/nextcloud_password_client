// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:nextcloud_password_client/view_models/tag_list_view_model.dart';
import 'package:nextcloud_password_client/view_models/tag_view_model.dart';
import 'package:nextcloud_password_client/widgets/single_elements/general_text_input.dart';
import 'package:provider/provider.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';

// Project imports:
import 'package:nextcloud_password_client/controller/textfield_controller.dart';
import 'package:nextcloud_password_client/utils/key_chain.dart';
import 'package:nextcloud_password_client/utils/view_state.dart';
import 'package:nextcloud_password_client/view_models/config_view_model.dart';
import 'package:nextcloud_password_client/widgets/single_elements/label.dart';

class TagCreationDialogue {
  static Future<void> showTagCreationDialogue(BuildContext context) async {
    context.read<ViewState>().inEditMode = false;

    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        // create some values
        Color pickerColor = const Color.fromARGB(0, 236, 48, 48);

        return AlertDialog(
          scrollable: true,
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Label(AppLocalizations.of(context)!.nameColumn),
                        SizedBox(
                          width: 400,
                          height: 70,
                          child: GeneralTextInput(
                              TextFieldController.tagTextController),
                        ),
                      ],
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        ColorPicker(
                            pickerColor: pickerColor,
                            onColorChanged: (value) => pickerColor = value)
                      ],
                    )
                  ],
                ),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text(AppLocalizations.of(context)!.cancel),
              onPressed: () {
                _cancel(context);
              },
            ),
            TextButton(
              child: Text(AppLocalizations.of(context)!.create),
              onPressed: () {
                _createTag(context, pickerColor);
              },
            )
          ],
        );
      },
    );
  }
}

void _createTag(BuildContext context, Color color) {
  KeyChain keyChain = context.read<ConfigViewModel>().keyChain;

  TagViewModel tag = TagViewModel();

  tag.label = keyChain.encrypt(TextFieldController.tagTextController.text,
      keyChain.type, keyChain.current);

  TextFieldController.tagTextController.clear();
  tag.cseKey = keyChain.current;
  tag.cseType = keyChain.type;
  String col = colorToHex(color, enableAlpha: false, includeHashSign: true);
  tag.color = keyChain.encrypt(col, keyChain.type, keyChain.current);
  tag.id = UniqueKey().hashCode.toString();
  tag.refreshModel(fromModel: tag, toModel: tag.tagModel);
  TagListViewModel tagListViewModel = context.read<TagListViewModel>();
  tagListViewModel.addTag(tag);
  tag.createTag(context).then((answerTag) {
    tagListViewModel.deleteTag();
    tagListViewModel.addTag(answerTag);
  });

  Navigator.of(context).pop();
}

void _cancel(BuildContext context) {
  Navigator.of(context).pop();
}
