// Flutter imports:
import 'package:flutter/material.dart';
import 'package:nextcloud_password_client/constants/api_constants.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

// Package imports:
import 'package:nextcloud_password_client/utils/key_chain.dart';
import 'package:nextcloud_password_client/utils/view_state.dart';
import 'package:nextcloud_password_client/view_models/folder_list_view_model.dart';
import 'package:nextcloud_password_client/view_models/folder_view_model.dart';
import 'package:provider/provider.dart';

// Project imports:
import 'package:nextcloud_password_client/view_models/config_view_model.dart';

// ignore: must_be_immutable
class FolderSelectionDropDown extends StatefulWidget {
  FolderSelectionDropDown({super.key});

  FolderViewModel folder = FolderViewModel();

  @override
  // ignore: library_private_types_in_public_api
  _FolderSelectionDropDownState createState() =>
      _FolderSelectionDropDownState();
}

class _FolderSelectionDropDownState extends State<FolderSelectionDropDown> {
  @override
  Widget build(BuildContext context) {
    widget.folder = widget.folder.id.isEmpty
        ? context
            .read<FolderListViewModel>()
            .getFolderById(context.read<ViewState>().selectedFolder)
        : widget.folder;

    if (widget.folder.id.isEmpty) {
      widget.folder.id = apiRootFolder;
      widget.folder.label = AppLocalizations.of(context)!.all;
    }

    return DropdownButton(
        value: widget.folder,
        items: createFolderItems(context),
        onChanged: (value) {
          setState(() {
            widget.folder = value as FolderViewModel;
          });
        });
  }
}

List<DropdownMenuItem<FolderViewModel>> createFolderItems(
    BuildContext context) {
  List<DropdownMenuItem<FolderViewModel>> dropdDownItems = [];

  KeyChain keyChain = context.read<ConfigViewModel>().keyChain;

  FolderViewModel rootFolder = FolderViewModel();

  rootFolder.id = apiRootFolder;
  rootFolder.label = AppLocalizations.of(context)!.all;
  dropdDownItems
      .add(DropdownMenuItem(value: rootFolder, child: Text(rootFolder.label)));

  for (List<FolderViewModel> folderViewModels
      in context.read<FolderListViewModel>().folderViewModels.values) {
    for (FolderViewModel folderViewModel in folderViewModels) {
      dropdDownItems.add(DropdownMenuItem(
        value: folderViewModel,
        child: Text(
            keyChain.decrypt(folderViewModel.cseKey, folderViewModel.label)),
      ));
    }
  }

  return dropdDownItems;
}
