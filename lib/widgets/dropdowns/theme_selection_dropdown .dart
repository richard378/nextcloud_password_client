import 'package:flutter/material.dart';

// Package imports:
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';

// Project imports:
import 'package:nextcloud_password_client/enums/themes.dart';
import 'package:nextcloud_password_client/view_models/config_view_model.dart';

// ignore: must_be_immutable
class ThemeSelectionDropDown extends StatefulWidget {
  const ThemeSelectionDropDown({super.key});

  @override
  // ignore: library_private_types_in_public_api
  _ThemeSelectionDropDownState createState() => _ThemeSelectionDropDownState();
}

class _ThemeSelectionDropDownState extends State<ThemeSelectionDropDown> {
  Themes selectedTheme = Themes.nextcloud;

  @override
  Widget build(BuildContext context) {
    selectedTheme = context.read<ConfigViewModel>().selectedTheme;

    return DropdownButton(
        value: selectedTheme,
        items: [
          DropdownMenuItem(
            value: Themes.dark,
            child: Text(AppLocalizations.of(context)!.dark),
          ),
          DropdownMenuItem(
            value: Themes.light,
            child: Text(AppLocalizations.of(context)!.light),
          ),
          const DropdownMenuItem(
            value: Themes.nextcloud,
            child: Text("Nextcloud"),
          ),

          /*DropdownMenuItem(
            child: Text("System"),
            value: Themes.system,
          ),*/
          DropdownMenuItem(
            value: Themes.custom,
            child: Text(AppLocalizations.of(context)!.custom),
          ),
        ],
        onChanged: (value) {
          setState(() {
            selectedTheme = value as Themes;
            context.read<ConfigViewModel>().selectedTheme = selectedTheme;
          });
        });
  }
}
