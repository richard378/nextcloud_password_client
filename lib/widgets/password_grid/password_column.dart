import 'package:flutter/material.dart';

// Package imports:
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:pluto_grid_plus/pluto_grid_plus.dart';
import 'package:provider/provider.dart';

// Project imports:
import 'package:nextcloud_password_client/constants/password_grid_header_constants.dart';
import 'package:nextcloud_password_client/utils/view_state.dart';
import 'package:nextcloud_password_client/view_models/password_list_view_model.dart';

List<PlutoColumn> getPasswordHeader(
    BuildContext context, bool obscurePassword) {
  return [
    PlutoColumn(
      title: AppLocalizations.of(context)!.nameColumn,
      field: label,
      type: PlutoColumnType.text(),
      renderer: (rendererContext) {
        List<String> values = rendererContext.cell.value.toString().split('~');

        return Row(
          mainAxisSize: MainAxisSize.max,
          children: [
            // Image.memory(const Base64Decoder().convert(values[0])),
            // const SizedBox(width: 10),
            Text(
              values[1],
              overflow: TextOverflow.fade,
            )
          ],
        );
      },
    ),
    PlutoColumn(
      title: AppLocalizations.of(context)!.usernameColumn,
      field: userName,
      type: PlutoColumnType.text(),
    ),
    PlutoColumn(
      title: AppLocalizations.of(context)!.passwordColumn,
      field: password,
      type: PlutoColumnType.text(),
      enableEditingMode: false,
      formatter: (value) => obscurePassword ? '*****' : value,
    ),
    PlutoColumn(
      title: AppLocalizations.of(context)!.urlColumn,
      field: url,
      type: PlutoColumnType.text(),
    ),
    PlutoColumn(
      title: AppLocalizations.of(context)!.sharedColumn,
      field: shared,
      type: PlutoColumnType.text(),
      renderer: (rendererContext) {
        bool value = rendererContext.cell.value;
        return value
            ? const Icon(Icons.check, color: Colors.green)
            : const Icon(Icons.highlight_off, color: Colors.red);
      },
    ),
    PlutoColumn(
        title: AppLocalizations.of(context)!.statusColumn,
        field: status,
        type: PlutoColumnType.text(),
        renderer: (rendererContext) {
          switch (rendererContext.cell.value) {
            case 0:
              return Icon(MdiIcons.shieldHalfFull, color: Colors.green);

            case 1:
              return Icon(MdiIcons.shieldHalfFull, color: Colors.amber);

            case 2:
              return Icon(MdiIcons.shieldHalfFull, color: Colors.red);

            default:
              return Icon(MdiIcons.shieldHalfFull);
          }
        }),
    PlutoColumn(
      title: AppLocalizations.of(context)!.passwordMenu,
      field: options,
      type: PlutoColumnType.text(),
      renderer: (rendererContext) {
        return PopupMenuButton<String>(
          onSelected: (value) {
            switch (value) {
              case 'delete':
                context.read<PasswordListViewModel>().delete(context,
                    passwordId: context.read<ViewState>().selectedPassword);
                break;
              default:
            }
          },
          itemBuilder: (context) => <PopupMenuEntry<String>>[
            PopupMenuItem<String>(
              value: 'delete',
              child: ListTile(
                title: Text(AppLocalizations.of(context)!.deletePassword),
              ),
            ),
          ],
        );
      },
    ),
  ];
}
