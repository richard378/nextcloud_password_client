// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:flutter_styled_toast/flutter_styled_toast.dart';
import 'package:provider/provider.dart';

// Project imports:
import 'package:nextcloud_password_client/constants/widget_constants.dart';
import 'package:nextcloud_password_client/utils/key_chain.dart';
import 'package:nextcloud_password_client/view_models/config_view_model.dart';
import 'package:nextcloud_password_client/view_models/password_view_model.dart';
import 'package:nextcloud_password_client/view_models/tag_list_view_model.dart';
import 'package:nextcloud_password_client/view_models/tag_view_model.dart';

Widget tagWidget(PasswordViewModel password, BuildContext context, String tag) {
  KeyChain keyChain = context.read<ConfigViewModel>().keyChain;

  TagViewModel tagViewModel = context.read<TagListViewModel>().getTagById(tag);

  String label = keyChain.decrypt(tagViewModel.cseKey, tagViewModel.label);

  String color = keyChain.decrypt(tagViewModel.cseKey, tagViewModel.color);

  return Container(
    decoration: BoxDecoration(
      color: Color(int.parse(
        color.replaceFirst(colorHashTag, colorPrefix),
      )),
      borderRadius: const BorderRadius.all(Radius.circular(5)),
    ),
    margin: const EdgeInsets.only(right: 2.5, left: 2.5),
    padding: const EdgeInsets.only(top: 0, bottom: 0),
    height: 25,
    child: Row(
      children: [
        TextButton(
          onPressed: () => showToast('not yet implemented', context: context),
          style: TextButton.styleFrom(
            foregroundColor: Colors.black,
            padding: const EdgeInsets.only(left: 5, right: 0),
          ),
          child: Text(
            label,
            style: const TextStyle(fontSize: 15),
          ),
        ),
        IconButton(
          alignment: Alignment.center,
          padding: const EdgeInsets.only(left: 0, top: 0, bottom: 0),
          iconSize: 20,
          splashRadius: 20,
          onPressed: () => password.deleteTag(context, tagViewModel),
          icon: const Icon(
            Icons.clear,
          ),
        )
      ],
    ),
  );
}
