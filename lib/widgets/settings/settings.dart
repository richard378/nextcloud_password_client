// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:multi_split_view/multi_split_view.dart';
import 'package:provider/provider.dart';

// Project imports:
import 'package:nextcloud_password_client/constants/settings_list_items.dart';
import 'package:nextcloud_password_client/screens/settings_detail_screen.dart';
import 'package:nextcloud_password_client/screens/settings_topic_list.dart';
import 'package:nextcloud_password_client/utils/view_state.dart';

Widget getSettings(BuildContext context) {
  SettingsItems selectedSetting =
      context.watch<ViewState>().selectedSettingsItem;

  MultiSplitViewController settingsSplitController =
      MultiSplitViewController(areas: [Area(size: 0.3), Area(size: 0.3)]);

  MultiSplitView settingsSplitView = MultiSplitView(
    initialAreas: [Area(min: 0.1, 
                        data: [
                              settingsTopicList(context),
                              settingsDetailScreen(context, selectedSetting)
                              ])],
    axis: Axis.horizontal,
    controller: settingsSplitController,
  );

  MultiSplitViewTheme settingsSplitTheme = MultiSplitViewTheme(
      data: MultiSplitViewThemeData(
          dividerThickness: 1,
          dividerPainter: DividerPainters.background(color: Colors.white)),
      child: settingsSplitView);

  return Column(
    children: [
      Container(
        height: 50,
        decoration: const BoxDecoration(
          border: Border(
            bottom: BorderSide(width: 1.0, color: Colors.white),
          ),
        ),
        child: Row(
          children: [
            const Expanded(
              child: Text("Title"),
            ),
            FloatingActionButton(
              backgroundColor: Colors.transparent,
              foregroundColor: Colors.white,
              child: const Icon(Icons.cancel),
              onPressed: () {
                context.read<ViewState>().showPasswordScreen();
              },
            ),
          ],
        ),
      ),
      Expanded(
        child: settingsSplitTheme,
      ),
    ],
  );
}
