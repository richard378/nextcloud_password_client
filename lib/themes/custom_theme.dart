// Flutter imports:
import 'package:flutter/material.dart';

class CustomTheme extends ThemeData {

  factory CustomTheme() {
  
    ThemeData theme = CustomTheme();
  
    return theme.copyWith(
        colorScheme: theme.colorScheme.copyWith(
            secondary: Colors.blueAccent, primary: Colors.blue)) as CustomTheme;
  }
  
}
