// Flutter imports:
import 'package:flutter/material.dart';

abstract class PasswordObjectsInterface extends ChangeNotifier {
  Future<void> setFavorite(BuildContext context, bool newFavorite) async {}
}
