// Project imports:
import 'package:nextcloud_password_client/constants/page_config_key_constants.dart';
import 'package:nextcloud_password_client/constants/routing_paths_constants.dart';
import 'package:nextcloud_password_client/enums/routing_path.dart';
import 'package:nextcloud_password_client/router/page_configuration.dart';

PageConfiguration loginPageConfig = PageConfiguration(
    key: pageKeyLogin,
    path: routingPathLogin,
    uiPage: RoutingPath.login,
    currentPageAction: null);

PageConfiguration settingsPageConfig = PageConfiguration(
    key: pageKeySettings,
    path: routingPathSettings,
    uiPage: RoutingPath.settings,
    currentPageAction: null);

PageConfiguration passwordScreenPageConfig = PageConfiguration(
    key: pageKeyPasswordScreen,
    path: routingPathPasswordScreen,
    uiPage: RoutingPath.passwordScreen,
    currentPageAction: null);
    
PageConfiguration missingDependenciesPageConfig = PageConfiguration(
    key: pageKeymissingDependenciesScreen,
    path: routingPathMissingDependenciesScreen,
    uiPage: RoutingPath.missingDependenciesScreen,
    currentPageAction: null);
