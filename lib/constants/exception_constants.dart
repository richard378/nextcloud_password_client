const credentialsdError = 'credentials is missing or wrong';
const credentialErrorCode = 1;
const masterPassworMissingdError = 'Masterpasswort is missing';
const masterpasswordMissingErrorCode = 2;
const masterPasswordWrongError = 'Masterpasswort is wrong';
const masterpasswordWrongErrorCode = 3;
const decryptionKeyNotFoundError = 'No key for decryption found!';
const decryptionKeyNotFoundErrorCode = 4;
