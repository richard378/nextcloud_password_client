const apiProtocol = 'https://';

const apiGetUsers = '/index.php/apps/passwords/api/1.0/share/partners';
const apiShareWithUser = '/index.php/apps/passwords/api/1.0/share/create';
const apiShareUpdate = '/index.php/apps/passwords/api/1.0/share/update';
const apiShareDelete = '/index.php/apps/passwords/api/1.0/share/delete';

const apiSessionRequest = '/index.php/apps/passwords/api/1.0/session/request';
const apiSessionOpen = '/index.php/apps/passwords/api/1.0/session/open';
const apiSessionKeepAlive =
    '/index.php/apps/passwords/api/1.0/session/keepalive';

const apiGetFolderList = '/index.php/apps/passwords/api/1.0/folder/list';
const apiGetFolderListDetail = 'model+revisions';
const apiPostFolderDelete = '/index.php/apps/passwords/api/1.0/folder/delete';
const apiPostFolderCreate = '/index.php/apps/passwords/api/1.0/folder/create';
const apiPatchFolderUpdate = '/index.php/apps/passwords/api/1.0/folder/update';
const apiRootFolder = '00000000-0000-0000-0000-000000000000';

const apiPostPasswordDelete =
    '/index.php/apps/passwords/api/1.0/password/delete';
const apiGetPasswordList = '/index.php/apps/passwords/api/1.0/password/list';
const apiGetPasswordShow = '/index.php/apps/passwords/api/1.0/password/show';
const apiGetPasswordDetail = 'model+revisions+tag-ids+shares';
const apiPatchPasswordUpdate =
    '/index.php/apps/passwords/api/1.0/password/update';
const apiPostPasswordCreate =
    '/index.php/apps/passwords/api/1.0/password/create';
const apiPatchPasswordRestore =
    '/index.php/apps/passwords/api/1.0/password/restore';
const apiGetFavIcon =
    '/index.php/apps/passwords/api/1.0/service/favicon/{domain}/32';

const apiGetTagList = '/index.php/apps/passwords/api/1.0/tag/list';
const apiPostTagCreate = '/index.php/apps/passwords/api/1.0/tag/create';
const apiPostTagShow = '/index.php/apps/passwords/api/1.0/tag/show';
const apiPatchTagUpdate = '/index.php/apps/passwords/api/1.0/tag/update';
const apiPostTagDelete = '/index.php/apps/passwords/api/1.0/tag/delete';
const apiGetTagDetail = 'model+revisions';

const apiGetServerSettingsList =
    '/index.php/apps/passwords/api/1.0/settings/list';
const apiChallenge = 'challenge';
const apiType = 'type';
const apiPWDv1r1 = 'PWDv1r1';
const apiSalts = 'salts';
const apiToken = 'token';
const apiSuccess = 'success';
