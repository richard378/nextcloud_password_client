// Flutter imports:
import 'package:flutter/cupertino.dart';

// Project imports:
import 'package:nextcloud_password_client/enums/page_state.dart';
import 'package:nextcloud_password_client/router/page_configuration.dart';

class PageAction {

  PageState state;
  PageConfiguration? page;
  List<PageConfiguration>? pages;
  Widget? widget;

  PageAction({
    this.state = PageState.none, 
    this.page, 
    this.pages, 
    this.widget
  });
}
