// Package imports:
import 'package:equatable/equatable.dart';

// Project imports:
import 'package:nextcloud_password_client/enums/routing_path.dart';
import 'package:nextcloud_password_client/router/page_action.dart';

class PageConfiguration with EquatableMixin {

  final String key;
  final String path;
  final RoutingPath uiPage;
  PageAction? currentPageAction;

  PageConfiguration({
      required this.key,
      required this.path,
      required this.uiPage,
      this.currentPageAction
      });

  @override
  List<Object?> get props => [
    key,
    path, 
    uiPage, 
    currentPageAction
  ];
  
}
