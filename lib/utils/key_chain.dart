// Dart imports:
import 'dart:convert';
import 'dart:typed_data';

// Package imports:
import 'package:flutter_sodium/flutter_sodium.dart';
import 'package:hive_flutter/hive_flutter.dart';

// Project imports:
import 'package:nextcloud_password_client/constants/exception_constants.dart';
import 'package:nextcloud_password_client/exceptions/decrypt_exception.dart';

// Package imports:
//import 'package:flutter_sodium/flutter_sodium.dart';

part 'key_chain.g.dart';

@HiveType(typeId: 1)
class KeyChain {

  @HiveField(0)
  String _type;

  @HiveField(1)
  Map<String, dynamic> _keychain;

  @HiveField(2)
  String _current;

  String get type => _type;

  String get current => _current == '' ? '' : _current;

  KeyChain(this._type, this._keychain, this._current);

  KeyChain.none() : this('none', {}, '');

  KeyChain.fromMap(Map<String, dynamic> map)
      : _type = 'CSEv1r1',
        _keychain = map['keys'],
        _current = map['current'];

  String decrypt(String id, String encryptedValue) {

    if (encryptedValue.isEmpty || id.isEmpty) {
    
      return encryptedValue;
    
    }
    
    if (_keychain[id] == null) {
    
      throw DecryptException(decryptionKeyNotFoundError);
    
    }
    
    final vals = Sodium.hex2bin(encryptedValue);
    final nonce = vals.sublist(0, Sodium.cryptoSecretboxNoncebytes);
    final cipher = vals.sublist(Sodium.cryptoSecretboxNoncebytes);
    final decryptedValue = Sodium.cryptoSecretboxOpenEasy(
        cipher, nonce, Sodium.hex2bin(_keychain[id]));
    
    return utf8.decode(decryptedValue);
  }


  String encrypt(
    String decryptedValue, 
    String type, 
    String key) {

    if (type == 'none' || decryptedValue.isEmpty || current.isEmpty) {
      
      return decryptedValue;
    
    }

    final nonce = Sodium.randombytesBuf(Sodium.cryptoSecretboxNoncebytes);
    final vals = utf8.encoder.convert(decryptedValue);

    final value =
        Sodium.cryptoSecretboxEasy(vals, nonce, Sodium.hex2bin(_keychain[key]));

    final encryptedValue = nonce.toList();
    encryptedValue.addAll(value);

    return Sodium.bin2hex(Uint8List.fromList(encryptedValue));

  }

}
