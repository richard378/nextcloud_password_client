// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:equatable/equatable.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:provider/provider.dart';

// Project imports:
import 'package:nextcloud_password_client/constants/page_config_constants.dart';
import 'package:nextcloud_password_client/constants/settings_list_items.dart';
import 'package:nextcloud_password_client/enums/nodes.dart';
import 'package:nextcloud_password_client/enums/page_state.dart';
import 'package:nextcloud_password_client/enums/security.dart';
import 'package:nextcloud_password_client/router/page_action.dart';
import 'package:nextcloud_password_client/utils/data_access_layer.dart';
import 'package:nextcloud_password_client/view_models/config_view_model.dart';
import 'package:nextcloud_password_client/view_models/credentials_view_model.dart';
import 'package:nextcloud_password_client/view_models/folder_list_view_model.dart';
import 'package:nextcloud_password_client/view_models/password_list_view_model.dart';
import 'http_utils.dart';

part 'view_state.g.dart';

@HiveType(typeId: 6)
class ViewState extends ChangeNotifier with EquatableMixin {

  bool _isURLReachable = false;
  bool _masterPasswordInputAnswer = false;
  bool _isLoading = false;
  bool _isLoggedIn = false;
  bool _clientAuthenticated = false;
  PageAction _currentAction = PageAction(widget: null);
  bool _masterPasswordInputError = false;
  SettingsItems _selectedSettingsItem = SettingsItems.login;
  bool _clientPasswordInputError = false;
  bool _credentialInputError = false;

  @HiveField(0)
  Nodes _selectedNode = Nodes.all;
  
  @HiveField(1)
  String _selectedFolder = '';
  
  @HiveField(2)
  String _selectedTag = '';
  
  @HiveField(3)
  bool _sharedByYou = true;
  
  @HiveField(4)
  Security _selectedSecurity = Security.secure;
  
  @HiveField(5)
  String _selectedFavorite = '';
  
  @HiveField(6)
  String _selectedPassword = '';
  
  bool _ignoreMissingCredentials = false;
  bool _relogin = true;
  bool _obscurePassword = true;
  bool _credentialsProvided = false;
  
  @HiveField(7)
  List<double> folderSplitWeights = [0.3, 0.7];
  
  @HiveField(8)
  List<double> passwordSplitWeights = [0.7, 0.3];
  
  @HiveField(9)
  String sortedColumn = '';
  
  @HiveField(10)
  String columnSort = '';
  
  bool inEditMode = false;
  DataAccessLayer dataAccessLayer;
  bool _hideData = false;
  bool _initError = false;

  ViewState({DataAccessLayer? dataAccessLayer})
      : dataAccessLayer = dataAccessLayer ?? DataAccessLayer();

  SettingsItems get selectedSettingsItem => _selectedSettingsItem;
  bool get obscurePassword => _obscurePassword;
  PageAction get currentAction => _currentAction;
  bool get relogin => _relogin;
  bool get ignoreMissingCredentials => _ignoreMissingCredentials;
  bool get masterPasswordInputError => _masterPasswordInputError;
  bool get clientAuthenticated => _clientAuthenticated;
  bool get clientPasswordInputError => _clientPasswordInputError;
  bool get credentialInputError => _credentialInputError;
  bool get isLoading => _isLoading;
  bool get sharedByYou => _sharedByYou;
  bool get isLoggedIn => _isLoggedIn;
  bool get masterPasswordInputAnswer => _masterPasswordInputAnswer;
  String get selectedFolder => _selectedFolder;
  String get selectedPassword => _selectedPassword;
  Nodes get selectedNode => _selectedNode;
  String get selectedTag => _selectedTag;
  Security get selectedSecurity => _selectedSecurity;
  String get selectedFavorite => _selectedFavorite;
  bool get credentialsProvided => _credentialsProvided;
  bool get isURLReachable => _isURLReachable;
  bool get hideData => _hideData;
  bool get initError => _initError;

  set hideData(bool hideData) {

    _hideData = hideData;
    notifyListeners();
  
  }

  set selectedSettingsItem(SettingsItems selectedSettingsItem) {
  
    _selectedSettingsItem = selectedSettingsItem;
    notifyListeners();
  
  }

  set isURLReachable(bool isUrlReachableNew) {
  
    _isURLReachable = isUrlReachableNew;
    notifyListeners();
  
  }

  set credentialsProvided(bool credentialsProvided) {
  
    _credentialsProvided = credentialsProvided;
    notifyListeners();
  
  }

  set sharedByYou(bool sharedByYou) {
  
    _sharedByYou = sharedByYou;
    notifyListeners();
  
  }

  set selectedNode(Nodes node) {
  
    _selectedNode = node;
    notifyListeners();
  
  }

  set selectedTag(String tagId) {
  
    _selectedTag = tagId;
    notifyListeners();
  
  }

  set selectedSecurity(Security security) {
  
    _selectedSecurity = security;
    notifyListeners();
  
  }

  set selectedFavorite(String passwordID) {
  
    _selectedFavorite = passwordID;
    notifyListeners();
  
  }

  set selectedPassword(String passwordID) {
  
    _selectedPassword = passwordID;
    notifyListeners();
  
  }

  set relogin(bool relogin) {
  
    _relogin = relogin;
    notifyListeners();
  
  }

  set obscurePassword(bool obscure) {
  
    _obscurePassword = obscure;
    notifyListeners();
  
  }

  set ignoreMissingCredentials(bool ignore) {
  
    _ignoreMissingCredentials = ignore;
    notifyListeners();
  
  }

  set clientAuthenticated(bool authenticated) {
  
    _clientAuthenticated = authenticated;
    notifyListeners();
  
  }

  set selectedFolder(String folderID) {
  
    _selectedFolder = folderID;
    notifyListeners();
  
  }

  set masterPasswordInputError(bool inputError) {
  
    _masterPasswordInputError = inputError;
    notifyListeners();
  
  }

  set clientPasswordInputError(bool inputError) {
  
    _masterPasswordInputError = inputError;
    notifyListeners();
  
  }

  set credentialInputError(bool inputError) {
  
    _credentialInputError = inputError;
    notifyListeners();
  
  }

  set currentAction(PageAction action) {
  
    _currentAction = action;
    notifyListeners();
  
  }

  void resetCurrentAction() {
  
    _currentAction = PageAction(widget: const SizedBox());
  
  }

  Future<bool> checkURLreachability(String host) async {
  
    _isURLReachable = (await HttpUtils().checkConn(host))!;
    notifyListeners();
  
    return _isURLReachable;
  
  }

  set isLoading(bool isLoading) {
  
    _isLoading = isLoading;
    notifyListeners();
  
  }

  set masterPasswordInputAnswer(okCode) {
  
    _masterPasswordInputAnswer = okCode;
    notifyListeners();
  
  }

  set initError(bool initError) {
  
    _initError = initError;
    showErrorScreen();
  
  }

  set isLoggedIn(bool loggedIn) {
  
    _isLoggedIn = loggedIn;
  
    if (loggedIn) {
  
      showPasswordScreen();
  
    } else {
  
      showLoginScreen();
  
    }
  
  }

  void showLoginScreen() {
  
    _currentAction = PageAction(
      state: PageState.replaceAll,
      page: loginPageConfig,
      widget: const SizedBox(),
    );
  
    notifyListeners();
  
  }

  void showErrorScreen() {
  
    _currentAction = PageAction(
      state: PageState.replaceAll,
      page: missingDependenciesPageConfig,
      widget: const SizedBox(),
    );
  
    notifyListeners();
  
  }

  void showPasswordScreen() {
  
    _currentAction = PageAction(
      state: PageState.replaceAll,
      page: passwordScreenPageConfig,
      widget: const SizedBox(),
    );
  
    notifyListeners();
  
  }

  void persistViewState() {
  
    dataAccessLayer.persistViewState(this);
  
  }

  void showSettings() {
  
    _currentAction = PageAction(
      state: PageState.replaceAll,
      page: settingsPageConfig,
      widget: const SizedBox(),
    );
  
    notifyListeners();
  
  }

  Future<void> handleAppClosing() async {
  
    persistViewState();
    dataAccessLayer.closeBoxes();
  
  }

  Future<void> initialize() async {
  
    ViewState viewState = dataAccessLayer.loadViewState()!;
    _selectedNode = viewState.selectedNode;
    _selectedFolder = viewState.selectedFolder;
    _selectedTag = viewState.selectedTag;
    _sharedByYou = viewState.sharedByYou;
    _selectedSecurity = viewState.selectedSecurity;
    _selectedFavorite = viewState.selectedFavorite;
    _selectedPassword = viewState.selectedPassword;
    sortedColumn = viewState.sortedColumn;
    columnSort = viewState.columnSort;
    folderSplitWeights = viewState.folderSplitWeights;
    passwordSplitWeights = viewState.passwordSplitWeights;
  
  }

  void handleLogout(BuildContext context) {
  
    context.read<ConfigViewModel>().handleLogout();
    context.read<CredentialsViewModel>().handleLogout();
    context.read<FolderListViewModel>().handleLogout();
    context.read<PasswordListViewModel>().handleLogout();
    dataAccessLayer.handleLogout();
    resetViewState();
  
  }

  void resetViewState() {
  
    isURLReachable = false;
    _masterPasswordInputAnswer = false;
    _isLoading = false;
    _clientAuthenticated = false;
    _masterPasswordInputError = false;
    _selectedSettingsItem = SettingsItems.login;
    _clientPasswordInputError = false;
    _credentialInputError = false;
    _selectedFolder = '';
    _selectedPassword = '';
    _ignoreMissingCredentials = false;
    _relogin = true;
    _obscurePassword = true;
    folderSplitWeights = [0.3, 0.7];
    passwordSplitWeights = [0.7, 0.3];
    isLoggedIn = false;
  
  }

  @override
  List<Object?> get props => [
        isURLReachable,
        _masterPasswordInputAnswer,
        _isLoading,
        _isLoggedIn,
        _clientAuthenticated,
        _currentAction,
        _masterPasswordInputError,
        _selectedSettingsItem,
        _clientPasswordInputError,
        _credentialInputError,
        _selectedNode,
        _selectedFolder,
        _selectedTag,
        _sharedByYou,
        _selectedSecurity,
        _selectedFavorite,
        _selectedPassword,
        _ignoreMissingCredentials,
        _relogin,
        _obscurePassword,
        folderSplitWeights,
        passwordSplitWeights,
        sortedColumn,
        columnSort,
        inEditMode
      ];

}
