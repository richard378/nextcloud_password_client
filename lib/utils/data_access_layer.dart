// Dart imports:
import 'dart:convert';

// Package imports:
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:hive_flutter/hive_flutter.dart';

// Project imports:
import 'package:nextcloud_password_client/constants/persistence_constants.dart';
import 'package:nextcloud_password_client/models/config_model.dart';
import 'package:nextcloud_password_client/models/credentials_model.dart';
import 'package:nextcloud_password_client/models/folder_model.dart';
import 'package:nextcloud_password_client/models/password_model.dart';
import 'package:nextcloud_password_client/models/server_config_model.dart';
import 'package:nextcloud_password_client/models/tag_model.dart';
import 'package:nextcloud_password_client/models/user_model.dart';
import 'package:nextcloud_password_client/utils/view_state.dart';

class DataAccessLayer {
  late Box<ConfigModel> _configBox;
  late Box<ServerConfigModel> _serverConfigBox;
  late Box<FolderModel> _folderBox;
  late Box<PasswordModel> _passwordBox;
  late Box<CredentialsModel> _credentialBox;
  late Box<ViewState> _viewStateBox;
  late Box<TagModel> _tagBox;
  late Box<UserModel> _userBox;

  static final DataAccessLayer _dataAccessLayer = DataAccessLayer._internal();
  factory DataAccessLayer() => _dataAccessLayer;

  DataAccessLayer._internal();

  Future<void> openBoxes() async {
    const FlutterSecureStorage secureStorage = FlutterSecureStorage();

    if (!await secureStorage.containsKey(key: 'key') &&
        await secureStorage.read(key: 'key') == null) {
      await secureStorage.write(
          key: 'key', value: base64UrlEncode(Hive.generateSecureKey()));
    }

    var encryptionKey =
        base64Url.decode(await secureStorage.read(key: 'key') as String);

    _configBox = await Hive.openBox<ConfigModel>(persistenceConfigBox);

    _serverConfigBox =
        await Hive.openBox<ServerConfigModel>(persistenceServerConfigBox);

    _viewStateBox = await Hive.openBox<ViewState>(persistenceViewStateBox);

    _folderBox = await Hive.openBox<FolderModel>(persistenceFolderBox,
        encryptionCipher: HiveAesCipher(encryptionKey));

    _passwordBox = await Hive.openBox<PasswordModel>(persistencePasswordsBox,
        encryptionCipher: HiveAesCipher(encryptionKey));
    _tagBox = await Hive.openBox<TagModel>(persistenceTagsBox,
        encryptionCipher: HiveAesCipher(encryptionKey));

    _credentialBox =
        await Hive.openBox<CredentialsModel>(persistenceCredentialBox);

    _userBox = await Hive.openBox<UserModel>(persistenceUserBox,
        encryptionCipher: HiveAesCipher(encryptionKey));
  }

  ConfigModel? loadConfig() {
    return _configBox.containsKey(persistenceConfigKey)
        ? _configBox.get(persistenceConfigKey)
        : ConfigModel();
  }

  void persistConfig(ConfigModel configModel) {
    _configBox.put(persistenceConfigKey, configModel);
  }

  void persistServerConfig(ServerConfigModel serverConfigModel) {
    _serverConfigBox.put(persistenceServerConfigKey, serverConfigModel);
  }

  ServerConfigModel? loadServerConfig() {
    return _serverConfigBox.containsKey(persistenceServerConfigKey)
        ? _serverConfigBox.get(persistenceServerConfigKey)
        : ServerConfigModel();
  }

  ViewState? loadViewState() {
    ViewState? viewState;
    viewState = _viewStateBox.containsKey(persistenceViewStateKey)
        ? _viewStateBox.get(persistenceViewStateKey)
        : ViewState();

    return viewState;
  }

  void persistViewState(ViewState viewState) {
    _viewStateBox.put(persistenceViewStateKey, viewState);
  }

  void persistFolder(List<FolderModel> folderModels) {
    for (var folder in folderModels) {
      _folderBox.put(folder.id, folder);
    }
  }

  List<FolderModel> loadFolder() {
    return _folderBox.isEmpty ? [] : _folderBox.values.toList();
  }

  void saveFolder(FolderModel folder) {
    _folderBox.put(folder.id, folder);
  }

  void persistPasswords(List<PasswordModel> passwordModels) {
    for (var password in passwordModels) {
      _passwordBox.put(password.id, password);
    }
  }

  void savePassword(PasswordModel password) {
    _passwordBox.put(password.id, password);
  }

  List<PasswordModel> loadPasswords() {
    return _passwordBox.isEmpty ? [] : _passwordBox.values.toList();
  }

  void persistCredentials(CredentialsModel credentialsModel) {
    _credentialBox.put(persistenceCredentialKey, credentialsModel);
  }

  CredentialsModel? loadCredentials() {
    return _credentialBox.containsKey(persistenceCredentialKey)
        ? _credentialBox.get(persistenceCredentialKey)
        : CredentialsModel();
  }

  void persistTags(List<TagModel> tagModels) {
    for (var tag in tagModels) {
      _tagBox.put(tag.id, tag);
    }
  }

  List<TagModel> loadTags() {
    return _tagBox.isEmpty ? [] : _tagBox.values.toList();
  }

  void saveTag(TagModel tag) {
    _tagBox.put(tag.id, tag);
  }

  void closeBoxes() async {
    _viewStateBox.compact().then((value) => _viewStateBox.close());
    _folderBox.compact().then((value) => _folderBox.close());
    _passwordBox.compact().then((value) => _passwordBox.close());
    _credentialBox.compact().then((value) => _credentialBox.close());
    _serverConfigBox.compact().then((value) => _serverConfigBox.close());
    _configBox.compact().then((value) => _configBox.close());
  }

  void deleteConfig() {
    _configBox.clear();
  }

  void deleteCredentials() {
    _credentialBox.clear();
  }

  void deleteViewState() {
    _viewStateBox.clear();
  }

  void deletePasswords() {
    _passwordBox.clear();
  }

  void deleteFolder() {
    _folderBox.clear();
  }

  Future<void> handleLogout() async {
    deleteBoxes();
  }

  void deleteBoxes() {
    deleteConfig();
    deleteCredentials();
    deleteViewState();
    deletePasswords();
    deleteFolder();
  }

  void persistUsers(List<UserModel> users) {
    for (UserModel user in users) {
      _userBox.put(user.id, user);
    }
  }

  List<UserModel> loadUser() {
    return _userBox.isEmpty ? [] : _userBox.values.toList();
  }
}
