// Package imports:
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';

// Project imports:
import 'package:nextcloud_password_client/utils/data_access_layer.dart';
import 'package:nextcloud_password_client/view_models/folder_view_model.dart';
import 'folder_view_model_test.mocks.dart';

@GenerateMocks([DataAccessLayer])
void main() {
  MockDataAccessLayer dataAccessLayer = MockDataAccessLayer();
  FolderViewModel folderViewModelLocal = FolderViewModel();
  folderViewModelLocal.id = '7575ef5e-68ef-4bdb-bdb5-442accf73499';
  folderViewModelLocal.revision = '30e852bb-516f-497a-abfd-6971ab57a540';
  folderViewModelLocal.label = 'Work';
  folderViewModelLocal.parent = '00000000-0000-0000-0000-000000000000';
  folderViewModelLocal.edited = 1518640874;
  folderViewModelLocal.favorite = false;
  test('fromMap', () {
    FolderViewModel folderViewModelFromMap =
        FolderViewModel.fromMap(dataAccessLayer.samples['folders'][0]);
    expect(folderViewModelFromMap, folderViewModelLocal);
  });

  test('fromModel', () {
    FolderViewModel folderViewModelFromModel =
        FolderViewModel.fromModel(folderViewModelLocal);
    expect(folderViewModelFromModel, folderViewModelLocal);
  });

  test('refreshDataModel', () {
    folderViewModelLocal.client = 'TestClient';
    expect(folderViewModelLocal.folderModel.props == folderViewModelLocal.props,
        false);

    folderViewModelLocal.refreshModel(
        fromModel: folderViewModelLocal,
        toModel: folderViewModelLocal.folderModel);
    expect(folderViewModelLocal.folderModel.props, folderViewModelLocal.props);

    folderViewModelLocal.folderModel.trashed = true;
    expect(folderViewModelLocal.folderModel.props == folderViewModelLocal.props,
        false);

    folderViewModelLocal.refreshModel(
        fromModel: folderViewModelLocal.folderModel,
        toModel: folderViewModelLocal);
    expect(folderViewModelLocal.folderModel.props, folderViewModelLocal.props);
  });
}
