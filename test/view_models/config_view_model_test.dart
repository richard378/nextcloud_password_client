// Package imports:
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

// Project imports:
import 'package:nextcloud_password_client/enums/themes.dart';
import 'package:nextcloud_password_client/utils/data_access_layer.dart';
import 'package:nextcloud_password_client/view_models/config_view_model.dart';
import 'config_view_model_test.mocks.dart';

@GenerateMocks([DataAccessLayer])
void main() {
  MockDataAccessLayer dataAccessLayer = MockDataAccessLayer();
  test('temporary Id should be incremented', () {
    ConfigViewModel configViewModel =
        ConfigViewModel(dataAccessLayer: dataAccessLayer);
    expect(configViewModel.geTemporaryID(), 1);
    expect(configViewModel.geTemporaryID(), 2);
  });

  test('refresh rate attribute should be changed', () {
    ConfigViewModel configViewModel =
        ConfigViewModel(dataAccessLayer: dataAccessLayer);
    expect(configViewModel.$refreshRateInSeconds, 10);
    configViewModel.refreshRateInSeconds = 20;
    expect(configViewModel.$refreshRateInSeconds, 20);
  });

  test('save master password attribute should be changed', () {
    ConfigViewModel configViewModel =
        ConfigViewModel(dataAccessLayer: dataAccessLayer);
    expect(configViewModel.saveMasterPassword, false);
    configViewModel.saveMasterPassword = true;
    expect(configViewModel.saveMasterPassword, true);
  });
  test('use local copy attribute should be changed', () {
    ConfigViewModel configViewModel =
        ConfigViewModel(dataAccessLayer: dataAccessLayer);
    expect(configViewModel.useLocalCopy, false);
    configViewModel.useLocalCopy = true;
    expect(configViewModel.useLocalCopy, true);
  });
  test('save credentials attribute should be changed', () {
    ConfigViewModel configViewModel =
        ConfigViewModel(dataAccessLayer: dataAccessLayer);
    expect(configViewModel.saveCredentials, false);
    configViewModel.saveCredentials = true;
    expect(configViewModel.saveCredentials, true);
  });

  test('theme attribute should be changed', () {
    ConfigViewModel configViewModel =
        ConfigViewModel(dataAccessLayer: dataAccessLayer);
    expect(configViewModel.selectedTheme, Themes.nextcloud);
    configViewModel.selectedTheme = Themes.dark;
    expect(configViewModel.selectedTheme, Themes.dark);
  });
  test('refresh data model', () {
    ConfigViewModel configViewModel =
        ConfigViewModel(dataAccessLayer: dataAccessLayer);
    configViewModel.refreshRateInSeconds = 30;
    expect(configViewModel.props == configViewModel.configModel.props, false);
    configViewModel.refreshDataModels();
    expect(configViewModel.props, configViewModel.configModel.props);
  });

  test('refresh view model', () {
    ConfigViewModel configViewModel =
        ConfigViewModel(dataAccessLayer: dataAccessLayer);
    expect(configViewModel.props, configViewModel.configModel.props);
    configViewModel.configModel.loginSucceeded = true;
    expect(configViewModel.props == configViewModel.configModel.props, false);
    configViewModel.refreshViewModels();
    expect(configViewModel.props, configViewModel.configModel.props);
  });
  test('handle logout', () {
    ConfigViewModel configViewModel =
        ConfigViewModel(dataAccessLayer: dataAccessLayer);
    configViewModel.loginSucceeded = true;
    expect(configViewModel.props == configViewModel.configModel.props, false);
    configViewModel.handleLogout();
    expect(configViewModel.props, configViewModel.configModel.props);
  });
  test('persist config', () {
    ConfigViewModel configViewModel =
        ConfigViewModel(dataAccessLayer: dataAccessLayer);
    configViewModel.configModel.$refreshRateInSeconds = 30;
    expect(configViewModel.props == configViewModel.configModel.props, false);

    when(configViewModel.persistModel()).thenAnswer((_) {
      verify(() => dataAccessLayer.persistConfig(configViewModel.configModel))
          .called(1);
      expect(configViewModel.props, configViewModel.configModel.props);
    });
  });
  test('load config', () {
    clearInteractions(dataAccessLayer);
    ConfigViewModel configViewModel =
        ConfigViewModel(dataAccessLayer: dataAccessLayer);
    when(configViewModel.loadModel()).thenAnswer((_) {
      verify(() => dataAccessLayer.loadConfig()).called(1);
    });
  });

  test('initialize', () {
    clearInteractions(dataAccessLayer);
    ConfigViewModel configViewModel =
        ConfigViewModel(dataAccessLayer: dataAccessLayer);
    when(configViewModel.initialize()).thenAnswer((_) async {
      verify(() => dataAccessLayer.loadConfig()).called(1);
    });
  });
}
