// Flutter imports:
import 'package:flutter/foundation.dart';

// Package imports:
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

// Project imports:
import 'package:nextcloud_password_client/models/folder_model.dart';
import 'package:nextcloud_password_client/utils/data_access_layer.dart';
import 'package:nextcloud_password_client/view_models/folder_list_view_model.dart';
import 'package:nextcloud_password_client/view_models/folder_view_model.dart';
import 'folder_list_view_model_test.mocks.dart';

@GenerateMocks([DataAccessLayer])
void main() {
  MockDataAccessLayer dataAccessLayer = MockDataAccessLayer();
  Map<String, List<FolderViewModel>> folderViewModels = {};

  void addFolderViewModel(FolderViewModel folderViewModel) {
    if (!folderViewModels.containsKey(folderViewModel.parent)) {
      folderViewModels[folderViewModel.parent] = [];
    }
    folderViewModels[folderViewModel.parent]!
        .add(FolderViewModel.fromModel(folderViewModel));
  }

  FolderViewModel folderViewModel = FolderViewModel.fromMap({
    "id": "7575ef5e-68ef-4bdb-bdb5-442accf73499",
    "revision": "30e852bb-516f-497a-abfd-6971ab57a540",
    "label": "Work",
    "parent": "00000000-0000-0000-0000-000000000000",
    "edited": 1518640874,
    "favorite": false
  });
  addFolderViewModel(folderViewModel);

  folderViewModel = FolderViewModel.fromMap({
    "id": "6bfd35b0-3998-49e8-b9f3-b8aebda9f0d3",
    "revision": "c8305813-b059-4615-9407-01284b28f5bf",
    "label": "Private",
    "parent": "00000000-0000-0000-0000-000000000000",
    "edited": 1463431274,
    "favorite": false
  });
  addFolderViewModel(folderViewModel);

  folderViewModel = FolderViewModel.fromMap({
    "id": "77f4d132-7be3-4592-9b6b-0268380fd803",
    "revision": "33c10fce-7dad-4f6b-b6d8-e22d61fda07f",
    "label": "Development",
    "parent": "7575ef5e-68ef-4bdb-bdb5-442accf73499",
    "edited": 1552769158,
    "favorite": true
  });
  addFolderViewModel(folderViewModel);

  folderViewModel = FolderViewModel.fromMap({
    "id": "593b0f5b-c257-4c15-b2fe-386253524832",
    "revision": "1fbc3da8-042c-4581-9352-e6e4c8da024a",
    "label": "Hobbies",
    "parent": "6bfd35b0-3998-49e8-b9f3-b8aebda9f0d3",
    "edited": 1510432874,
    "favorite": false
  });
  addFolderViewModel(folderViewModel);

  folderViewModel = FolderViewModel.fromMap({
    "id": "129fe6a7-84e1-4836-bb5f-8b0a0ce72928",
    "revision": "1b319f28-b412-477a-8295-c5f36a198a82",
    "label": "Smartphone",
    "parent": "6bfd35b0-3998-49e8-b9f3-b8aebda9f0d3",
    "edited": 1482439274,
    "favorite": false
  });
  addFolderViewModel(folderViewModel);

  folderViewModel = FolderViewModel.fromMap({
    "id": "973faede-9bbf-4fb2-a625-a34acbbe6e8b",
    "revision": "157bb6cf-9dac-4ee9-b0ff-4972894a59ce",
    "label": "Financial",
    "parent": "6bfd35b0-3998-49e8-b9f3-b8aebda9f0d3",
    "edited": 1554707201,
    "favorite": true
  });
  addFolderViewModel(folderViewModel);

  folderViewModel = FolderViewModel.fromMap({
    "id": "42bfa1d6-ce03-4f95-a127-e41ebcc6080e",
    "revision": "e6d48b65-5553-4ced-b688-a65b6914c61a",
    "label": "Hosting",
    "parent": "7575ef5e-68ef-4bdb-bdb5-442accf73499",
    "edited": 1458160874,
    "favorite": false
  });
  addFolderViewModel(folderViewModel);

  folderViewModel = FolderViewModel.fromMap({
    "id": "c39f11df-7833-4ef0-8a39-26d87e9443df",
    "revision": "b781f5e9-5e70-4dc5-ab30-13348c704678",
    "label": "News",
    "parent": "6bfd35b0-3998-49e8-b9f3-b8aebda9f0d3",
    "edited": 1512592874,
    "favorite": false
  });
  addFolderViewModel(folderViewModel);

  folderViewModel = FolderViewModel.fromMap({
    "id": "1a306c08-6a2a-4495-827c-84c2b3c81127",
    "revision": "525fa8b3-4e6c-4dda-ac84-346669514516",
    "label": "International",
    "parent": "c39f11df-7833-4ef0-8a39-26d87e9443df",
    "edited": 1458247274,
    "favorite": false
  });
  addFolderViewModel(folderViewModel);

  folderViewModel = FolderViewModel.fromMap({
    "id": "cd246f63-e36e-4bfe-b761-23ce17ccdd7a",
    "revision": "e0ac7d88-ddee-4ad6-ac52-378a0a5fe8d2",
    "label": "Messengers",
    "parent": "129fe6a7-84e1-4836-bb5f-8b0a0ce72928",
    "edited": 1458506474,
    "favorite": false
  });
  addFolderViewModel(folderViewModel);

  folderViewModel = FolderViewModel();
  folderViewModel.id = 'cb29519e-7d46-42b9-b822-26f9dacf68cb';
  folderViewModel.revision = '325dd5a7-03a0-4d13-bd9a-008f8acb88a4';
  folderViewModel.label = 'Customer Accounts';
  folderViewModel.parent = '6bfd35b0-3998-49e8-b9f3-b8aebda9f0d3';
  folderViewModel.edited = 1476132074;
  folderViewModel.favorite = false;
  folderViewModel.refreshModel(
      fromModel: folderViewModel, toModel: folderViewModel.folderModel);
  addFolderViewModel(folderViewModel);

  folderViewModel = FolderViewModel();
  folderViewModel.id = '9e80a648-7dca-45a3-afcb-7dfa04c2a475';
  folderViewModel.revision = 'c6443800-4bcb-47f2-9f40-19fa0c9ee6cf';
  folderViewModel.label = 'Mobility';
  folderViewModel.parent = '6bfd35b0-3998-49e8-b9f3-b8aebda9f0d3';
  folderViewModel.edited = 1457296874;
  folderViewModel.favorite = false;
  folderViewModel.refreshModel(
      fromModel: folderViewModel, toModel: folderViewModel.folderModel);
  addFolderViewModel(folderViewModel);

  folderViewModel = FolderViewModel();
  folderViewModel.id = '8101819f-a146-40c8-ba67-2381927eb95a';
  folderViewModel.revision = '26f0cabe-e71b-49a8-8e22-d7b9a903026c';
  folderViewModel.label = 'Car Sharing';
  folderViewModel.parent = '9e80a648-7dca-45a3-afcb-7dfa04c2a475';
  folderViewModel.edited = 1499805674;
  folderViewModel.favorite = false;
  folderViewModel.refreshModel(
      fromModel: folderViewModel, toModel: folderViewModel.folderModel);
  addFolderViewModel(folderViewModel);

  folderViewModel = FolderViewModel();
  folderViewModel.id = '53735697-0450-4bcc-961c-bb1877e99be1';
  folderViewModel.revision = 'e1f5c57e-1e14-4a73-986d-6ea0221b34b2';
  folderViewModel.label = 'Social Media';
  folderViewModel.parent = '6bfd35b0-3998-49e8-b9f3-b8aebda9f0d3';
  folderViewModel.edited = 1468528874;
  folderViewModel.favorite = false;
  folderViewModel.refreshModel(
      fromModel: folderViewModel, toModel: folderViewModel.folderModel);
  addFolderViewModel(folderViewModel);

  folderViewModel = FolderViewModel();
  folderViewModel.id = '4c00917b-8261-4cab-9db1-7ff007f748da';
  folderViewModel.revision = 'c1010eb4-55e7-4c82-a0e4-5459425ec500';
  folderViewModel.label = 'IT';
  folderViewModel.parent = 'c39f11df-7833-4ef0-8a39-26d87e9443df';
  folderViewModel.edited = 1494276074;
  folderViewModel.favorite = false;
  folderViewModel.refreshModel(
      fromModel: folderViewModel, toModel: folderViewModel.folderModel);
  addFolderViewModel(folderViewModel);

  folderViewModel = FolderViewModel();
  folderViewModel.id = 'fb8ab5f8-52ba-4c44-90a0-276a672e012d';
  folderViewModel.revision = 'cccc73f9-9202-4e3c-821b-d053d5440fbc';
  folderViewModel.label = 'Shopping';
  folderViewModel.parent = '6bfd35b0-3998-49e8-b9f3-b8aebda9f0d3';
  folderViewModel.edited = 1510951274;
  folderViewModel.favorite = true;
  folderViewModel.refreshModel(
      fromModel: folderViewModel, toModel: folderViewModel.folderModel);
  addFolderViewModel(folderViewModel);

  folderViewModel = FolderViewModel();
  folderViewModel.id = '2f07fbd5-e9ec-4c4a-9d1b-7590387bc084';
  folderViewModel.revision = '851601b6-1dc9-4df2-8383-20c9776fec07';
  folderViewModel.label = 'Entertainment';
  folderViewModel.parent = '6bfd35b0-3998-49e8-b9f3-b8aebda9f0d3';
  folderViewModel.edited = 1490647274;
  folderViewModel.favorite = false;
  folderViewModel.refreshModel(
      fromModel: folderViewModel, toModel: folderViewModel.folderModel);
  addFolderViewModel(folderViewModel);

  folderViewModel = FolderViewModel();
  folderViewModel.id = 'b2ab16e6-994c-47ac-8324-7b6c0f6f4553';
  folderViewModel.revision = '1a09ddb7-5783-4e48-8c94-f5ca7c38aa74';
  folderViewModel.label = 'IT-Admin';
  folderViewModel.parent = '7575ef5e-68ef-4bdb-bdb5-442accf73499';
  folderViewModel.edited = 1555422462;
  folderViewModel.favorite = false;
  folderViewModel.refreshModel(
      fromModel: folderViewModel, toModel: folderViewModel.folderModel);
  addFolderViewModel(folderViewModel);

  folderViewModel = FolderViewModel();
  folderViewModel.id = '44471e98-a3bb-4449-9d0d-5452295977eb';
  folderViewModel.revision = 'fc90394c-0409-4995-94c1-d08899488ffc';
  folderViewModel.label = 'International';
  folderViewModel.parent = '4c00917b-8261-4cab-9db1-7ff007f748da';
  folderViewModel.edited = 1555640585;
  folderViewModel.favorite = false;
  folderViewModel.refreshModel(
      fromModel: folderViewModel, toModel: folderViewModel.folderModel);
  addFolderViewModel(folderViewModel);

  test('loadModel', () {
    List<FolderModel> folders = dataAccessLayer.loadFolder();
    expect(folders, dataAccessLayer.loadFolder());
  });

  test('initialize', () {
    clearInteractions(dataAccessLayer);
    FolderListViewModel folderListViewModel =
        FolderListViewModel(dataAccessLayer: dataAccessLayer);
    when(folderListViewModel.initialize()).thenAnswer((_) async {
      expect(folderViewModels == folderListViewModel.folderViewModels, true);
      verify(() => dataAccessLayer.loadFolder()).called(1);
    });
  });
  test('setFolderViewModels', () {
    FolderListViewModel folderListViewModel =
        FolderListViewModel(dataAccessLayer: dataAccessLayer);
    folderListViewModel.folderViewModels =
        folderListViewModel.folderViewModels =
            Map<String, List<FolderViewModel>>.from(folderViewModels);

    expect(folderViewModels, folderListViewModel.folderViewModels);
  });

  test('persistModel', () {
    FolderListViewModel folderListViewModel =
        FolderListViewModel(dataAccessLayer: dataAccessLayer);
    folderListViewModel.folderViewModels =
        folderListViewModel.folderViewModels =
            Map<String, List<FolderViewModel>>.from(folderViewModels);

    when(folderListViewModel.persistModel()).thenAnswer((_) async {
      verify(() => dataAccessLayer
              .persistFolder([folderViewModels.values.first.first.folderModel]))
          .called(1);
    });
  });

  test('getFolderById', () {
    FolderListViewModel folderListViewModel =
        FolderListViewModel(dataAccessLayer: dataAccessLayer);
    folderListViewModel.folderViewModels =
        folderListViewModel.folderViewModels =
            Map<String, List<FolderViewModel>>.from(folderViewModels);

    FolderViewModel localFolder = folderViewModels.values.first.first;
    FolderViewModel folder = folderListViewModel.getFolderById(localFolder.id);
    expect(folder, localFolder);
  });

  test('getFavoriteFolder', () {
    FolderListViewModel folderListViewModel =
        FolderListViewModel(dataAccessLayer: dataAccessLayer);

    folderListViewModel.folderViewModels =
        folderListViewModel.folderViewModels =
            Map<String, List<FolderViewModel>>.from(folderViewModels);

    List<FolderViewModel> favorites = [];
    for (List<FolderViewModel> folderViewModels in folderViewModels.values) {
      for (FolderViewModel folderViewModel in folderViewModels) {
        if (folderViewModel.id == '77f4d132-7be3-4592-9b6b-0268380fd803' ||
            folderViewModel.id == '973faede-9bbf-4fb2-a625-a34acbbe6e8b' ||
            folderViewModel.id == 'fb8ab5f8-52ba-4c44-90a0-276a672e012d') {
          favorites.add(folderViewModel);
        }
      }
    }
    favorites.sort(((a, b) => a.id.compareTo(b.id)));
    List<FolderViewModel> viewModelsFromListView =
        folderListViewModel.getFavoriteFolder();
    viewModelsFromListView.sort(((a, b) => a.id.compareTo(b.id)));
    expect(listEquals(favorites, viewModelsFromListView), true);
  });

  test('getParentFolder', () {
    FolderListViewModel folderListViewModel =
        FolderListViewModel(dataAccessLayer: dataAccessLayer);

    folderListViewModel.folderViewModels =
        folderListViewModel.folderViewModels =
            Map<String, List<FolderViewModel>>.from(folderViewModels);

    List<String> parents = [];
    List<String> parentsFromListViewModel = [];
    for (List<FolderViewModel> folderViewModels in folderViewModels.values) {
      for (FolderViewModel folderViewModel in folderViewModels) {
        if (folderViewModel.id == 'b2ab16e6-994c-47ac-8324-7b6c0f6f4553' ||
            folderViewModel.id == '7575ef5e-68ef-4bdb-bdb5-442accf73499' ||
            folderViewModel.id == '00000000-0000-0000-0000-000000000000') {
          parents.add(folderViewModel.id);
        }
      }
    }
    parents.sort((a, b) => a.compareTo(b));
    parentsFromListViewModel = folderListViewModel
        .getParentFolders('b2ab16e6-994c-47ac-8324-7b6c0f6f4553');
    parentsFromListViewModel.sort((a, b) => a.compareTo(b));
    expect(listEquals(parents, parentsFromListViewModel), true);
  });

  test('logout', () {
    FolderListViewModel folderListViewModel =
        FolderListViewModel(dataAccessLayer: dataAccessLayer);

    folderListViewModel.folderViewModels =
        folderListViewModel.folderViewModels =
            Map<String, List<FolderViewModel>>.from(folderViewModels);

    folderListViewModel.handleLogout();
    expect(folderListViewModel.folderViewModels.isEmpty, true);
  });
  test('refreshDataModel', () {
    FolderListViewModel folderListViewModel =
        FolderListViewModel(dataAccessLayer: dataAccessLayer);

    folderListViewModel.folderViewModels =
        folderListViewModel.folderViewModels =
            Map<String, List<FolderViewModel>>.from(folderViewModels);

    List<FolderModel> localModels = [];
    List<FolderModel> listModels = [];

    for (List<FolderViewModel> folderViewModelsFor in folderViewModels.values) {
      for (FolderViewModel folderViewModel in folderViewModelsFor) {
        FolderViewModel newModel =
            FolderViewModel.fromModel(folderViewModel.folderModel);
        newModel.hidden = newModel.folderModel.hidden = true;
        newModel.client = newModel.folderModel.client = 'test';
        localModels.add(newModel.folderModel);
      }
    }

    for (List<FolderViewModel> folderViewModelsFor
        in folderListViewModel.folderViewModels.values) {
      for (FolderViewModel folderViewModel in folderViewModelsFor) {
        folderViewModel.client = 'test';
        folderViewModel.hidden = true;
        listModels.add(folderViewModel.folderModel);
      }
    }

    localModels.sort((a, b) => a.id.compareTo(b.id));
    listModels.sort((a, b) => a.id.compareTo(b.id));
    expect(listEquals(localModels, listModels), false);
    folderListViewModel.refreshDataModels();
    listModels.clear();
    for (List<FolderViewModel> folderViewModels
        in folderListViewModel.folderViewModels.values) {
      for (FolderViewModel folderViewModel in folderViewModels) {
        listModels.add(folderViewModel.folderModel);
      }
    }
    listModels.sort((a, b) => a.id.compareTo(b.id));
    expect(listEquals(localModels, listModels), true);
  });

  test('refreshViewModel', () {
    FolderListViewModel folderListViewModel =
        FolderListViewModel(dataAccessLayer: dataAccessLayer);

    folderListViewModel.folderViewModels =
        Map<String, List<FolderViewModel>>.from(folderViewModels);
    List<FolderViewModel> localModels = [];
    List<FolderViewModel> listModels = [];

    for (List<FolderViewModel> folderViewModelsFor in folderViewModels.values) {
      for (FolderViewModel folderViewModel in folderViewModelsFor) {
        FolderViewModel newModel =
            FolderViewModel.fromModel(folderViewModel.folderModel);
        newModel.hidden = newModel.folderModel.hidden = true;
        newModel.client = newModel.folderModel.client = 'test';
        localModels.add(newModel);
      }
    }

    for (List<FolderViewModel> folderViewModelsFor
        in folderListViewModel.folderViewModels.values) {
      for (FolderViewModel folderViewModel in folderViewModelsFor) {
        folderViewModel.folderModel.client = 'test';
        folderViewModel.folderModel.hidden = true;
        listModels.add(folderViewModel);
      }
    }

    localModels.sort((a, b) => a.id.compareTo(b.id));
    listModels.sort((a, b) => a.id.compareTo(b.id));
    expect(localModels == listModels, false);

    folderListViewModel.refreshViewModels();
    listModels.clear();
    for (List<FolderViewModel> folderViewModels
        in folderListViewModel.folderViewModels.values) {
      for (FolderViewModel folderViewModel in folderViewModels) {
        listModels.add(folderViewModel);
      }
    }
    listModels.sort((a, b) => a.id.compareTo(b.id));
    expect(listEquals(localModels, listModels), true);
  });
}
