// Package imports:
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';

// Project imports:
import 'package:nextcloud_password_client/utils/data_access_layer.dart';
import 'package:nextcloud_password_client/view_models/password_share_view_model.dart';
import 'password_share_view_model_test.mocks.dart';

@GenerateMocks([DataAccessLayer])
void main() {
  MockDataAccessLayer dataAccessLayer = MockDataAccessLayer();
  PasswordShareViewModel passwordShareViewModelLocal = PasswordShareViewModel();
  passwordShareViewModelLocal.id = 'fb8ab5f8-52ba-2321-90a0-276a672e012d';
  passwordShareViewModelLocal.expires = 20220222102354;
  passwordShareViewModelLocal.editable = true;
  passwordShareViewModelLocal.password = 'verySecurePassword';
  passwordShareViewModelLocal.shareable = true;
  passwordShareViewModelLocal.updatePending = false;
  passwordShareViewModelLocal.owner = 'testuser';
  passwordShareViewModelLocal.receiver = 'anotherTestUser';
  passwordShareViewModelLocal.created = 20220212102354;
  passwordShareViewModelLocal.updated = 20220122102354;

  test('fromMap', () {
    PasswordShareViewModel folderViewModelFromMap =
        PasswordShareViewModel.fromMap(dataAccessLayer.samples['shares'][0]);
    expect(folderViewModelFromMap, passwordShareViewModelLocal);
  });

  test('fromModel', () {
    PasswordShareViewModel folderViewModelFromModel =
        PasswordShareViewModel.fromModel(passwordShareViewModelLocal);
    expect(folderViewModelFromModel, passwordShareViewModelLocal);
  });

  test('refreshDataModel', () {
    passwordShareViewModelLocal.owner = 'TestClient';
    expect(
        passwordShareViewModelLocal.passwordShareModel.props ==
            passwordShareViewModelLocal.props,
        false);

    passwordShareViewModelLocal.refreshModel(
        fromModel: passwordShareViewModelLocal,
        toModel: passwordShareViewModelLocal.passwordShareModel);
    expect(passwordShareViewModelLocal.passwordShareModel.props,
        passwordShareViewModelLocal.props);
  });

  test('refreshViewModel', () {
    passwordShareViewModelLocal.passwordShareModel.updated = 123;
    expect(
        passwordShareViewModelLocal.passwordShareModel.props ==
            passwordShareViewModelLocal.props,
        false);

    passwordShareViewModelLocal.refreshModel(
        fromModel: passwordShareViewModelLocal.passwordShareModel,
        toModel: passwordShareViewModelLocal);
    expect(passwordShareViewModelLocal.passwordShareModel.props,
        passwordShareViewModelLocal.props);
  });
}
