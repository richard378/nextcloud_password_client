{
  "createLocalCopy": "zapisywać hasła lokalnie?",
  "@createLocalCopy": {
    "description": "Check if a local copy shall be created (as fallback in case the server is not reachable)."
  },
  "serverReached": "serwer jest osiągalny.",
  "@serverReached": {
    "description": "status message when the given URL could be reached."
  },
  "serverNotReached": "serwer jest nieosiągalny.",
  "@serverNotReached": {
    "description": "status message when the given URL could not be reached."
  },
  "checkURLReachability": "Sprawdź, czy serwer jest osiągalny.",
  "@checkURLReachability": {
    "description": "check wether the server can be reached or not."
  },
  "enterURL": "Podaj adres serwera!",
  "@enterURL": {
    "description": "prompt that the user hast to enter the server URL."
  },
  "userName": "nazwa użytkownika",
  "@userName": {
    "description": "name of the login user"
  },
  "enterUserName": "wpisz swoją nazwę użytkownika",
  "@enterUserName": {
    "description": "prompt to enter the username"
  },
  "checkLogin": "sprawdź, czy podane dane są poprawne",
  "@checkLogin": {
    "description": "check wether the given credentials are valid or not"
  },
  "password": "hasło",
  "@password": {
    "description": "password"
  },
  "enterPassword": "wpisz swoje hasło",
  "@enterPassword": {
    "description": "prompt to enter the password"
  },
  "tags": "tagi",
  "@tags": {
    "description": "tags of passwords"
  },
  "secure": "bezpieczne",
  "@secure": {
    "description": "security level 'secure' of passwords"
  },
  "folder": "Folder",
  "@folder": {
    "description": "folder"
  },
  "donationMenuItem": "Wspieram",
  "@donationMenuItem": {
    "description": "String for the menu item to display Donate"
  },
  "reportIssueMenuItem": "Zgłoś problem",
  "@reportIssueMenuItem": {
    "description": "String for the menu item to display Report an issue"
  },
  "aboutMenuItem": "Informacje",
  "@aboutMenuItem": {
    "description": "String for the menu item to display About"
  },
  "editFolder": "edytuj folder",
  "@editFolder": {
    "description": "menu entry for editing a folder"
  },
  "deleteFolder": "usuń folder",
  "@deleteFolder": {
    "description": "menu entry for deleting a folder"
  },
  "favorites": "ulubione",
  "@favorites": {
    "description": "favorites passwords"
  },
  "shared": "udostępnione",
  "@shared": {
    "description": "shared passwords"
  },
  "passwordMenu": "Opcje hasła",
  "@passwordMenu": {
    "description": "menu for password options"
  },
  "folderMenu": "Opcje folderu",
  "@folderMenu": {
    "description": "menu for folder options"
  },
  "addPassword": "dodaj hasło",
  "@addPassword": {
    "description": "menu entry for adding a password"
  },
  "editPassword": "edytuj hasło",
  "@editPassword": {
    "description": "menu entry for editing a password"
  },
  "deletePassword": "usuń hasło",
  "@deletePassword": {
    "description": "menu entry for deleting a password"
  },
  "addFolder": "dodaj folder",
  "@addFolder": {
    "description": "menu entry for adding a folder"
  },
  "continueText": "Dalej",
  "@continueText": {},
  "enterMasterPassword": "Wpisz swoje hasło główne",
  "@enterMasterPassword": {
    "description": "title for popup to enter the masterpassword"
  },
  "saveMasterPassword": "zapisać hasło główne?",
  "@saveMasterPassword": {
    "description": "label for the checkbox if the master password shall be saved"
  },
  "masterPasswordException": "Hasło główne nieprawidłowe",
  "@masterPasswordException": {
    "description": "the entered masterpassword is not valid"
  },
  "credentialInputException": "Podane dane są nieprawidłowe",
  "@credentialInputException": {
    "description": "the entered credentials are not valid"
  },
  "clientPassword": "hasło klienta",
  "@clientPassword": {
    "description": "password to secure the client itself"
  },
  "enterClientPassword": "Wpisz swoje hasło klienta",
  "@enterClientPassword": {
    "description": "title for popup to enter the client password"
  },
  "clientPasswordException": "Hasło klienta nieprawidłowe",
  "@clientPasswordException": {
    "description": "the entered client password is not valid"
  },
  "ignoreMissingCredentials": "Nie pokazywać więcej tej wiadomości?",
  "@ignoreMissingCredentials": {
    "description": "ignore missing credentials and work with local data if available"
  },
  "enterCredentials": "Wprowadź dane uwierzytelniające",
  "@enterCredentials": {
    "description": "title for popup to enter the credentials"
  },
  "nameColumn": "Nazwisko",
  "@nameColumn": {},
  "usernameColumn": "Użytkownik",
  "@usernameColumn": {
    "description": "User for the password"
  },
  "passwordColumn": "Hasło",
  "@passwordColumn": {
    "description": "the password itself"
  },
  "urlColumn": "URL",
  "@urlColumn": {
    "description": "Url to the website of the password"
  },
  "detailsColumn": "Szczegóły",
  "@detailsColumn": {
    "description": "Details to the password"
  },
  "notesColumn": "Notatki",
  "@notesColumn": {
    "description": "Notes to the password"
  },
  "sharedColumn": "Udostępnione",
  "@sharedColumn": {
    "description": "Is the password shared with other users"
  },
  "statusColumn": "Status",
  "@statusColumn": {
    "description": "The passwords security status (secure, unsecure...)"
  },
  "systemMenu": "System",
  "@systemMenu": {
    "description": "String for the menu bar to display system"
  },
  "systemMenuTooltip": "Menu tematów związanych z systemem",
  "@systemMenuTooltip": {
    "description": "Tooltip for menuButton for system related topics"
  },
  "settingsMenuItem": "Ustawienia",
  "@settingsMenuItem": {
    "description": "String for the menu item to display settings"
  },
  "helpMenu": "Pomoc",
  "@helpMenu": {},
  "onlineHelpMenuItem": "Pomoc online",
  "@onlineHelpMenuItem": {
    "description": "String for the menu item to display Online-Help"
  },
  "accept": "akceptuj",
  "@accept": {
    "description": "accept"
  },
  "logout": "wyloguj",
  "@logout": {
    "description": "logout from server"
  },
  "syncRate": "odstępy synchronizacji",
  "@syncRate": {
    "description": "time between object retrieval from server"
  },
  "save": "zapisz",
  "@save": {
    "description": "save"
  },
  "about": "informacje",
  "@about": {
    "description": "about the app"
  },
  "recent": "często używane",
  "@recent": {
    "description": "recently used passwords"
  },
  "all": "wszystkie",
  "@all": {
    "description": "all passwords"
  },
  "duplicate": "niebezpieczne (duplikat)",
  "@duplicate": {
    "description": "the password is unsecure because it already use elsewhere"
  },
  "sse1r1": "SSEv1r1 Proste szyfrowanie (Gen. 1)",
  "@sse1r1": {
    "description": "SSEv1r1 Simple encryption (Gen. 1)"
  },
  "ssev1r2": "SSEv1r2 Proste szyfrowanie (Gen. 2)",
  "@ssev1r2": {
    "description": "SSEv1r2 Simple encryption (Gen. 2)"
  },
  "ssev2": "SSEv2 Zaawansowane szyfrowanie (SSE V2)",
  "@ssev2": {
    "description": "SSEv2 Advanced encryption (SSE V2)"
  },
  "noEncryption": "Brak szyfrowania",
  "@noEncryption": {},
  "serverEncryption": "szyfrowanie serwera",
  "@serverEncryption": {
    "description": "server encryption"
  },
  "clientEncryption": "szyfrowanie klienta",
  "@clientEncryption": {
    "description": "client encryption"
  },
  "id": "ID",
  "@id": {
    "description": "ID"
  },
  "lastEdited": "ostatnio edytowano",
  "@lastEdited": {
    "description": "last edited"
  },
  "created": "utworzono",
  "@created": {
    "description": "created"
  },
  "statistics": "statystyki",
  "@statistics": {
    "description": "statistical password details"
  },
  "themes": "Motywy",
  "@themes": {
    "description": "themes"
  },
  "login": "Login",
  "@login": {
    "description": "login"
  },
  "general": "Ogólny",
  "@general": {
    "description": "general password details"
  },
  "share": "Udostępnij (tylko online)",
  "@share": {
    "description": "manage password shares"
  },
  "passwordRemoteUpdateError": "Nie udało się zaktualizować hasła na serwerze.",
  "@passwordRemoteUpdateError": {
    "description": "the password could not be updated by the api. Eventually the local changes will be overwritten when data is received from server in background process"
  },
  "initError": "Wystąpił błąd podczas inicjowania aplikacji!",
  "@initError": {
    "description": "There was an error initializing the app. Mostly because of missing dependencies."
  },
  "installationInstructionGeneral": "Postępuj zgodnie z instrukcją instalacji!",
  "@installationInstructionGeneral": {
    "description": "Follow installation instruction to get the app running"
  },
  "installationInstructionWindows": "Zainstaluj libsodium w C:\\Windows\\System32",
  "@installationInstructionWindows": {
    "description": "Install libsodium in C:\\Windows\\System32"
  },
  "installationInstructionLinux": "Zainstaluj libsodium, libsecret, libjsoncpp oraz asecret-service!",
  "@installationInstructionLinux": {
    "description": "Install libsodium, libsecret, libjsoncpp and a secret-service"
  },
  "installationInstructionMacOS": "Zainstaluj libsodium!",
  "@installationInstructionMacOS": {
    "description": "Install libsodium"
  },
  "helpMenuTooltip": "Uzyskaj pomoc i dalsze informacje",
  "@helpMenuTooltip": {
    "description": "Tooltip for menuButton for help and further information"
  },
  "saveCredentials": "zapisać dane lokalnie?",
  "@saveCredentials": {
    "description": "checkbox if the user wants to save the credentials on local hard drive"
  },
  "revisionColumn": "Wersje",
  "@revisionColumn": {
    "description": "Revisions to the password"
  },
  "security": "bezpieczeństwo",
  "@security": {
    "description": "security level of passwords"
  },
  "weak": "słabe",
  "@weak": {
    "description": "security level 'weak' of passwords"
  },
  "breached": "naruszone",
  "@breached": {
    "description": "security level 'breached' of passwords"
  },
  "sharedByYou": "udostępnione przez Ciebie",
  "@sharedByYou": {
    "description": "passwords you shared with someone"
  },
  "passwords": "hasła",
  "@passwords": {
    "description": "passwords"
  },
  "dark": "Ciemny",
  "@dark": {
    "description": "dark theme"
  },
  "addTag": "dodaj tag",
  "@addTag": {
    "description": "add tag to password"
  },
  "sharedWithYou": "udostępnione Tobie",
  "@sharedWithYou": {
    "description": "passwords someone shared with you"
  },
  "light": "Jasny",
  "@light": {
    "description": "light theme"
  },
  "custom": "Własny",
  "@custom": {
    "description": "custom theme"
  },
  "expired": "niebezpieczne (wygasłe)",
  "@expired": {
    "description": "the password is unsecure because it has not been changed in a long time"
  },
  "confirmRevisionTitle": "Przywrócić wersję?",
  "@confirmRevisionTitle": {
    "description": "Title for dialogue to confirm restoring password to chosen revision"
  },
  "confirmRevisionText": "Czy chcesz przywrócić tę wersję?",
  "@confirmRevisionText": {
    "description": "Main text for dialogue to confirm restoring password to chosen revision"
  },
  "cancel": "anuluj",
  "@cancel": {
    "description": "cancel"
  },
  "restore": "przywróć",
  "@restore": {
    "description": "button for dialouge to restore password to revision"
  },
  "create": "utwórz",
  "@create": {
    "description": "create"
  },
  "folderName": "nazwa folderu",
  "@folderName": {
    "description": "Name des Ordners"
  },
  "subfolder": "Podfolder",
  "@subfolder": {
    "description": "subfolder"
  },
  "folderRemoteUpdateError": "Nie udało się zdalnie zaktualizować folderu.",
  "@folderRemoteUpdateError": {
    "description": "the folder could not be updated by the api. Eventually the local changes will be overwritten when data is received from server in background process"
  },
  "createFolder": "utwórz folder",
  "@createFolder": {
    "description": "menu entry for creating a folder"
  },
  "languageSelection": "Wybierz język",
  "@languageSelection": {
    "description": "Title for language selection"
  },
  "searchPassword": "wyszukaj hasło",
  "@searchPassword": {
    "description": "Label for password search input field"
  },
  "deleteTag": "usuń tag",
  "@deleteTag": {},
  "searchUser": "wyszukaj użytkownika",
  "@searchUser": {
    "description": "Label for the user search field"
  },
  "passwordSharedWithYou": "udostępnił Ci to hasło",
  "@passwordSharedWithYou": {
    "description": "text for the password share information"
  },
  "createTag": "utwórz tag",
  "@createTag": {
    "description": "menu entry for creating a tag"
  },
  "parentFolder": "folder nadrzędny",
  "@parentFolder": {
    "description": "text for the selection of the parent folder"
  },
  "editTag": "edytuj tag",
  "@editTag": {
    "description": "menu entry for editing a tag"
  },
  "notYetImplemented": "jeszcze nie zaimplementowano.",
  "@notYetImplemented": {
    "description": "Notification if some logic is not yet implemented."
  },
  "folderNotDeletable": "Folderu głównego nie można usunąć.",
  "@folderNotDeletable": {
    "description": "Notification if the user wants to edit the root folde"
  },
  "tag": "tag",
  "@tag": {
    "description": "tag"
  },
  "createPassword": "utwórz hasło",
  "@createPassword": {
    "description": "menu entry for creating a password"
  },
  "tagMenu": "Opcje tagów",
  "@tagMenu": {
    "description": "menu for tag options"
  },
  "folderNotEditable": "Folderu głównego nie można edytować.",
  "@folderNotEditable": {
    "description": "Notification if the user wants to edit the root folde"
  },
  "folderRemoteDeletionError": "Nie udało się zdalnie usunąć folderu. ",
  "@folderRemoteDeletionError": {
    "description": "The folder could not be deleted by the API."
  },
  "rootFolderDeletionError": "Nie można usunąć folderu głównego.",
  "@rootFolderDeletionError": {
    "description": "The root folder can and may not be deleted!"
  },
  "localChangesOverwriteHint": "Zmiany są zapisywane lokalnie i po pewnym czasie mogą zostać ponownie nadpisane z serwera!",
  "@localChangesOverwriteHint": {
    "description": "the change could not be processed by the api. Eventually the local changes will be overwritten when data is received from server in background process"
  },
  "passwordShareAllowEditing": "Zezwól użytkownikowi odbierającemu na edycję hasła (aktualnie zabronione).",
  "@passwordShareAllowEditing": {
    "description": "The password is currenly not editable and will be, after the button is pressed"
  },
  "passwordShareDenyEditing": "Zabroń użytkownikowi odbierającemu edycji hasła (obecnie dozwolone).",
  "@passwordShareDenyEditing": {
    "description": "The password is currenly editable and won't be, after the button is pressed"
  },
  "passwordShareAllowSharing": "Zezwól użytkownikowi odbierającemu na udostępnianie hasła (aktualnie zabronione).",
  "@passwordShareAllowSharing": {
    "description": "The password is currenly not shareable and will be, after the button is pressed"
  },
  "passwordShareDenySharing": "Zabroń użytkownikowi odbierającemu udostępniania hasła (obecnie dozwolone).",
  "@passwordShareDenySharing": {
    "description": "The password is currenly shareable and won't be, after the button is pressed"
  },
  "passwordShareExpiration": "Ustaw datę ważności",
  "@passwordShareExpiration": {
    "description": "Set expiration date of a share"
  },
  "deletePasswordShare": "Usuń udostępnienie",
  "@deletePasswordShare": {
    "description": "Stop sharing the password"
  }
}
