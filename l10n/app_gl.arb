{
  "enterURL": "Introduza o enderezo do servidor!",
  "@enterURL": {
    "description": "prompt that the user hast to enter the server URL."
  },
  "serverNotReached": "non foi posíbel acceder ao servidor.",
  "@serverNotReached": {
    "description": "status message when the given URL could not be reached."
  },
  "createLocalCopy": "gardar os contrasinais localmente?",
  "@createLocalCopy": {
    "description": "Check if a local copy shall be created (as fallback in case the server is not reachable)."
  },
  "serverReached": "foi posíbel acceder ao servidor.",
  "@serverReached": {
    "description": "status message when the given URL could be reached."
  },
  "checkURLReachability": "Comprobar se o servidor é accesíbel.",
  "@checkURLReachability": {
    "description": "check wether the server can be reached or not."
  },
  "saveCredentials": "gardar as credenciais localmente?",
  "@saveCredentials": {
    "description": "checkbox if the user wants to save the credentials on local hard drive"
  },
  "continueText": "Continuar",
  "@continueText": {},
  "enterMasterPassword": "Introduza o seu contrasinal principal",
  "@enterMasterPassword": {
    "description": "title for popup to enter the masterpassword"
  },
  "saveMasterPassword": "gardar o contrasinal principal?",
  "@saveMasterPassword": {
    "description": "label for the checkbox if the master password shall be saved"
  },
  "password": "contrasinal",
  "@password": {
    "description": "password"
  },
  "masterPasswordException": "O contrasinal principal é incorrecto",
  "@masterPasswordException": {
    "description": "the entered masterpassword is not valid"
  },
  "enterUserName": "introduza o seu nome de usuario",
  "@enterUserName": {
    "description": "prompt to enter the username"
  },
  "userName": "nome de usuario",
  "@userName": {
    "description": "name of the login user"
  },
  "checkLogin": "comprobar se as credenciais son válidas",
  "@checkLogin": {
    "description": "check wether the given credentials are valid or not"
  },
  "enterPassword": "introduza o seu contrasinal",
  "@enterPassword": {
    "description": "prompt to enter the password"
  },
  "credentialInputException": "As credenciais introducidas son incorrectas",
  "@credentialInputException": {
    "description": "the entered credentials are not valid"
  },
  "enterClientPassword": "Introduza o seu contrasinal do cliente",
  "@enterClientPassword": {
    "description": "title for popup to enter the client password"
  },
  "clientPassword": "contrasinal do cliente",
  "@clientPassword": {
    "description": "password to secure the client itself"
  },
  "clientPasswordException": "O contrasinal do cliente é incorrecto",
  "@clientPasswordException": {
    "description": "the entered client password is not valid"
  },
  "ignoreMissingCredentials": "Non volver amosar esta mensaxe?",
  "@ignoreMissingCredentials": {
    "description": "ignore missing credentials and work with local data if available"
  },
  "enterCredentials": "Introduza as súas credenciais",
  "@enterCredentials": {
    "description": "title for popup to enter the credentials"
  },
  "nameColumn": "Nome",
  "@nameColumn": {},
  "usernameColumn": "Usuario",
  "@usernameColumn": {
    "description": "User for the password"
  },
  "passwordColumn": "Contrasinal",
  "@passwordColumn": {
    "description": "the password itself"
  },
  "urlColumn": "URL",
  "@urlColumn": {
    "description": "Url to the website of the password"
  },
  "detailsColumn": "Detalles",
  "@detailsColumn": {
    "description": "Details to the password"
  },
  "revisionColumn": "Revisións",
  "@revisionColumn": {
    "description": "Revisions to the password"
  },
  "sharedColumn": "Compartido",
  "@sharedColumn": {
    "description": "Is the password shared with other users"
  },
  "statusColumn": "Estado",
  "@statusColumn": {
    "description": "The passwords security status (secure, unsecure...)"
  },
  "systemMenu": "Sistema",
  "@systemMenu": {
    "description": "String for the menu bar to display system"
  },
  "settingsMenuItem": "Axustes",
  "@settingsMenuItem": {
    "description": "String for the menu item to display settings"
  },
  "helpMenu": "Axuda",
  "@helpMenu": {},
  "helpMenuTooltip": "Obteña axuda e máis información",
  "@helpMenuTooltip": {
    "description": "Tooltip for menuButton for help and further information"
  },
  "onlineHelpMenuItem": "Axuda en liña",
  "@onlineHelpMenuItem": {
    "description": "String for the menu item to display Online-Help"
  },
  "donationMenuItem": "Doar",
  "@donationMenuItem": {
    "description": "String for the menu item to display Donate"
  },
  "reportIssueMenuItem": "Informar dunha incidencia",
  "@reportIssueMenuItem": {
    "description": "String for the menu item to display Report an issue"
  },
  "aboutMenuItem": "Sobre",
  "@aboutMenuItem": {
    "description": "String for the menu item to display About"
  },
  "accept": "acepto",
  "@accept": {
    "description": "accept"
  },
  "logout": "saír",
  "@logout": {
    "description": "logout from server"
  },
  "syncRate": "intervalo de sincronización",
  "@syncRate": {
    "description": "time between object retrieval from server"
  },
  "recent": "usado recentemente",
  "@recent": {
    "description": "recently used passwords"
  },
  "favorites": "favoritos",
  "@favorites": {
    "description": "favorites passwords"
  },
  "shared": "compartido",
  "@shared": {
    "description": "shared passwords"
  },
  "tags": "etiquetas",
  "@tags": {
    "description": "tags of passwords"
  },
  "security": "seguranza",
  "@security": {
    "description": "security level of passwords"
  },
  "all": "todo",
  "@all": {
    "description": "all passwords"
  },
  "secure": "seguro",
  "@secure": {
    "description": "security level 'secure' of passwords"
  },
  "weak": "feble",
  "@weak": {
    "description": "security level 'weak' of passwords"
  },
  "breached": "vulnerado",
  "@breached": {
    "description": "security level 'breached' of passwords"
  },
  "sharedWithYou": "compartido con Vde.",
  "@sharedWithYou": {
    "description": "passwords someone shared with you"
  },
  "sharedByYou": "compartido por Vde.",
  "@sharedByYou": {
    "description": "passwords you shared with someone"
  },
  "passwords": "contrasinais",
  "@passwords": {
    "description": "passwords"
  },
  "folder": "cartafol",
  "@folder": {
    "description": "folder"
  },
  "subfolder": "subcartafol",
  "@subfolder": {
    "description": "subfolder"
  },
  "dark": "Escuro",
  "@dark": {
    "description": "dark theme"
  },
  "light": "Claro",
  "@light": {
    "description": "light theme"
  },
  "custom": "Personalizado",
  "@custom": {
    "description": "custom theme"
  },
  "addTag": "engadir etiqueta",
  "@addTag": {
    "description": "add tag to password"
  },
  "sse1r1": "Cifrado simple SSEv1r1 (Gen. 1)",
  "@sse1r1": {
    "description": "SSEv1r1 Simple encryption (Gen. 1)"
  },
  "ssev1r2": "Cifrado simple SSEv1r2 (Gen. 2)",
  "@ssev1r2": {
    "description": "SSEv1r2 Simple encryption (Gen. 2)"
  },
  "noEncryption": "Sen cifrado",
  "@noEncryption": {},
  "serverEncryption": "cifrado do servidor",
  "@serverEncryption": {
    "description": "server encryption"
  },
  "clientEncryption": "cifrado do cliente",
  "@clientEncryption": {
    "description": "client encryption"
  },
  "id": "ID",
  "@id": {
    "description": "ID"
  },
  "lastEdited": "última edición",
  "@lastEdited": {
    "description": "last edited"
  },
  "created": "creado",
  "@created": {
    "description": "created"
  },
  "login": "Acceso",
  "@login": {
    "description": "login"
  },
  "localChangesOverwriteHint": "Os cambios gárdanse localmente e é posíbel que nalgún momento se sobrescriban desde o servidor!",
  "@localChangesOverwriteHint": {
    "description": "the change could not be processed by the api. Eventually the local changes will be overwritten when data is received from server in background process"
  },
  "passwordRemoteUpdateError": "Non foi posíbel actualizar o contrasinal de xeito remoto.",
  "@passwordRemoteUpdateError": {
    "description": "the password could not be updated by the api."
  },
  "folderRemoteUpdateError": "Non foi posíbel actualizar o cartafol de xeito remoto.",
  "@folderRemoteUpdateError": {
    "description": "the folder could not be updated by the api."
  },
  "createPassword": "crear contrasinal",
  "@createPassword": {
    "description": "menu entry for creating a password"
  },
  "editPassword": "editar o contrasinal",
  "@editPassword": {
    "description": "menu entry for editing a password"
  },
  "createTag": "crear etiqueta",
  "@createTag": {
    "description": "menu entry for creating a tag"
  },
  "initError": "Produciuse un erro ao iniciar a aplicación!",
  "@initError": {
    "description": "There was an error initializing the app. Mostly because of missing dependencies."
  },
  "installationInstructionGeneral": "Siga as instrucións de instalación!",
  "@installationInstructionGeneral": {
    "description": "Follow installation instruction to get the app running"
  },
  "installationInstructionWindows": "Instale libsodium en C:\\Windows\\System32",
  "@installationInstructionWindows": {
    "description": "Install libsodium in C:\\Windows\\System32"
  },
  "installationInstructionLinux": "Instale libsodium, libsecret, libjsoncpp e asecret-service!",
  "@installationInstructionLinux": {
    "description": "Install libsodium, libsecret, libjsoncpp and a secret-service"
  },
  "installationInstructionMacOS": "Instale libsodium!",
  "@installationInstructionMacOS": {
    "description": "Install libsodium"
  },
  "confirmRevisionTitle": "Restaurar a versión?",
  "@confirmRevisionTitle": {
    "description": "Title for dialogue to confirm restoring password to chosen revision"
  },
  "confirmRevisionText": "Quere restaurar esta revisión?",
  "@confirmRevisionText": {
    "description": "Main text for dialogue to confirm restoring password to chosen revision"
  },
  "restore": "restaurar",
  "@restore": {
    "description": "button for dialouge to restore password to revision"
  },
  "cancel": "cancelar",
  "@cancel": {
    "description": "cancel"
  },
  "create": "crear",
  "@create": {
    "description": "create"
  },
  "folderName": "nome do cartafol",
  "@folderName": {
    "description": "name of the folder"
  },
  "languageSelection": "Escolla un idioma",
  "@languageSelection": {
    "description": "Title for language selection"
  },
  "searchPassword": "buscar contrasinal",
  "@searchPassword": {
    "description": "Label for password search input field"
  },
  "searchUser": "buscar usuario",
  "@searchUser": {
    "description": "Label for the user search field"
  },
  "passwordSharedWithYou": "compartiu este contrasinal con Vde.",
  "@passwordSharedWithYou": {
    "description": "text for the password share information"
  },
  "parentFolder": "cartafol principal",
  "@parentFolder": {
    "description": "text for the selection of the parent folder"
  },
  "folderNotEditable": "Non é posíbel editar o cartafol raíz.",
  "@folderNotEditable": {
    "description": "Notification if the user wants to edit the root folde"
  },
  "folderNotDeletable": "Non é posíbel eliminar o cartafol raíz.",
  "@folderNotDeletable": {
    "description": "Notification if the user wants to edit the root folde"
  },
  "notYetImplemented": "aínda non está implementado.",
  "@notYetImplemented": {
    "description": "Notification if some logic is not yet implemented."
  },
  "rootFolderDeletionError": "Non é posíbel eliminar o cartafol raíz.",
  "@rootFolderDeletionError": {
    "description": "The root folder can and may not be deleted!"
  },
  "notesColumn": "Notas",
  "@notesColumn": {
    "description": "Notes to the password"
  },
  "systemMenuTooltip": "Menú para temas relacionados co sistema",
  "@systemMenuTooltip": {
    "description": "Tooltip for menuButton for system related topics"
  },
  "save": "gardar",
  "@save": {
    "description": "save"
  },
  "about": "sobre",
  "@about": {
    "description": "about the app"
  },
  "expired": "inseguro (caducado)",
  "@expired": {
    "description": "the password is unsecure because it has not been changed in a long time"
  },
  "ssev2": "Cifrado avanzado SSEv2 (SSE V2)",
  "@ssev2": {
    "description": "SSEv2 Advanced encryption (SSE V2)"
  },
  "duplicate": "inseguro (duplicado)",
  "@duplicate": {
    "description": "the password is unsecure because it already use elsewhere"
  },
  "statistics": "estatísticas",
  "@statistics": {
    "description": "statistical password details"
  },
  "themes": "Temas",
  "@themes": {
    "description": "themes"
  },
  "general": "Xeral",
  "@general": {
    "description": "general password details"
  },
  "share": "Compartir",
  "@share": {
    "description": "manage password shares"
  },
  "passwordMenu": "Opcións do contrasinal",
  "@passwordMenu": {
    "description": "menu for password options"
  },
  "folderMenu": "Opcións do cartafol",
  "@folderMenu": {
    "description": "menu for folder options"
  },
  "tagMenu": "Opcións da etiqueta",
  "@tagMenu": {
    "description": "menu for tag options"
  },
  "deletePassword": "eliminar o contrasinal",
  "@deletePassword": {
    "description": "menu entry for deleting a password"
  },
  "createFolder": "crear cartafol",
  "@createFolder": {
    "description": "menu entry for creating a folder"
  },
  "deleteTag": "eliminar etiqueta",
  "@deleteTag": {
    "description": "menu entry for deleting a tag"
  },
  "editTag": "editar etiqueta",
  "@editTag": {
    "description": "menu entry for editing a tag"
  },
  "editFolder": "editar cartafol",
  "@editFolder": {
    "description": "menu entry for editing a folder"
  },
  "tag": "etiqueta",
  "@tag": {
    "description": "tag"
  },
  "deleteFolder": "eliminar cartafol",
  "@deleteFolder": {
    "description": "menu entry for deleting a folder"
  },
  "folderRemoteDeletionError": "Non foi posíbel eliminar o cartafol de forma remota. ",
  "@folderRemoteDeletionError": {
    "description": "The folder could not be deleted by the API."
  }
}
